
\chapter{Implémentation}
\label{cha:implementation}

La librairie suppose de coder des classes spécifiques à chaque problème. Ces classes doivent implémenter certaines interfaces de la librairie pour pouvoir ensuite être utilisées avec un des solveurs disponibles. Le but de notre projet est de pouvoir incorporer cette librairie dans un logiciel permettant de résoudre un problème à priori quelconque\footnote{On se limite néanmoins à des problèmes modélisés par des équations différentielles ordinaires d'ordre 1 ou 2 à une dimension.}, il faut donc une implémentation valable pour un problème arbitraire. Cette étape nous permettra aussi de mieux prendre en main la librairie et de mieux comprendre les problèmes et les contraintes liés à son utilisation. 

L'implémentation se déroule en deux étapes. La première consiste à essayer de mettre en place une classe permettant de représenter un problème générique. La seconde étape consiste à reprendre le module construit précédemment et à le simplifier. En effet, la seule différence entre les deux implémentations est la source des valeurs en entrée. Dans un cas il s'agit d'une table de variables, des données du problème et de ces derniers mis à jour au fur et à mesure de l'intégration, dans le deuxième cas il s'agit simplement de lire les valeurs présentes sur les pins d'entrée-sortie du bloc.

Nous introduisons donc la première méthode la plus généraliste, où il s'agirait plus d'un module indépendant qu'autre chose, puis nous étudierons le raffinement appliqué à ce module dans la section \ref{sec:deux-meth-dint} qui traite de l'intégration avec ModHelX.

\section{Librairies}
\label{sec:librairies}

Les librairies utilisées ainsi que leur documentations sont disponibles dans le dépôt du projet.

Le solveur utilisé fait partie de \emph{Apache Commons Maths} et est aussi disponible à l'adresse suivante : \texttt{http://commons.apache.org/proper/commons-math/}. L'évaluateur d'expressions mathématiques (MESP) utilisé est aussi disponible à l'adresse suivante : \texttt{http://sourceforge.net/projects/expression-tree/}. 

Pour plus d'informations, se référer à la javadoc correspondante.

\section{Représentation d'une équation différentielle}
\label{sec:repr-dune-equat}

Il faut établir un langage permettant de décrire un problème de type ODE. Ce problème et ses paramètres doivent pouvoir ensuite être récupérés par notre module afin de paramétrer le solveur et l'objet représentant une équation différentielle. Une grammaire spécifique est donc définie à l'aide d'ANTLR \cite{antlrSite}.

Ici un exemple pour le problème de la balle rebondissante :
\begin{lstlisting}
Ordre = 2;

VectEtat {
	(y);
}

CondIni {
	y = "10";
	der1::y:: = "0.";
}

Cstes {
	g = "9.81";
}

Derivees {
	der2::y:: = "-g";
}

Evenement {
	RESET_STATE as der1::y:: = "-0.8 * der1::y::"; when y;
}
\end{lstlisting}

Chaque bloc correspond à un type d'information. Les expressions mathématiques sont sous forme de \texttt{String} afin de permettre le traitement par la librairie \emph{MESP} \cite{mespSite}. La reconnaissance de \texttt{String} sans guillemets nécessite une grammaire contextuelle qui n'est pas gérée par ANTLR. On notera aussi l'utilisation de \texttt{::} au lieu de parenthèses car \emph{MESP} considère les lexèmes suivis de parenthèses comme des fonctions. 

Le bloc \texttt{Evenement} permet de renseigner l'action effectuée lorsqu'un événement est détecté ainsi que la condition de détection. Plus de détails sur ces deux aspects dans la section \ref{sec:classe-evenement}. 

Tout ceci est ensuite converti sous forme d'AST et contient donc toutes les informations nécessaires au bon déroulement de l'intégration. Pour une explication sur le fonctionnement détaillée du processus d'intégration, voir section \ref{sec:dero-de-lint}. 

\section{Diagramme de classe}
\label{sec:diagramme-de-classe}

\begin{figure}[p]
  \makebox[\textwidth]{
    \includegraphics[scale=0.7]{images/diag-classe.jpg}
  }
  \caption{Diagramme de classe}
  \label{fig:diag-classe}
\end{figure}

\subsection{Classe \texttt{Equation}}
\label{sec:classe-equation}

Dès le départ il était nécessaire de considérer l'action du solveur comme quelque chose d'indépendant qui tournerait dans un thread à part. Notre classe principale \texttt{Equation} hérite donc de \texttt{Thread} et contient toutes les informations relatives à notre problème. Elle se charge de faire l'interface avec l'utilisateur et se charge de lancer le solveur. Elle contient aussi la table des symboles ainsi que les différents objets nécessaires au bon fonctionnement du solveur.

\begin{description}
\item[First/SecondOrderDifferentialEquations] Classe de la librairie du solveur correspondant à un problème donné du 1er/2nd ordre
\item[VarMap] Table des symboles contenant le nom des variables utilisées par MESP \cite{mespSite} ainsi que leur valeurs associées.
\item[ClassicalRungeKuttaIntegrator] Il s'agit du solveur en lui même, configurable à l'aide d'objets supplémentaires permettant d'agir sur les événéments et/ou l'exécution pas-à-pas.
\item[FuncMap] Liste des fonctions disponibles pour MESP. Chargée avec la liste des fonctions par défaut\footnote{Liste disponible dans la documentation de MESP}. 
\item [Step] Module servant à gérer l'exécution pas-à-pas du solveur, voir \ref{sec:classe-step}.  
\item [Evenement] Module servant à gérer les événements discrets, voir \ref{sec:classe-evenement}
\end{description}

\subsection{Classe \texttt{Step}}
\label{sec:classe-step}

Cette classe implémente l'interface \texttt{StepHandler} de la librairie \emph{Apache Commons Math} \cite{apacheJavadoc}. Cette interface décrit le comportement du composant en charge de l'exécution pas à pas. Cela se traduit concrétement par l'appel à la méthode \texttt{handleStep} à chaque pas d'intégration. A l'aide d'un objet \texttt{StepInterpolator} on a ensuite accès au vecteur d'état et l'instant t correspondant au pas courant.

A l'aide de cet objet on peut ainsi contrôler les actions effectuées à chaque pas d'intégration, comme l'écriture des résultats dans un fichier de sortie. Il est donc logique d'implémenter notre jeu de sémaphores dans cette méthode \texttt{handleStep}, un acquire au début de la méthode et un release à la fin permet d'obtenir une exécution pas à pas qui n'est pas nativement disponible. Par défaut le comportement du solveur est en mode \enquote{As Soon As Possible}, sous-entendu qu'il s'exécutera le plus rapidement possible sans possibilité d'arrêt autrement qu'en manipulant directement le thread ou en ordonnant l'arrêt de l'intégration via un événement (voir sous-section \ref{sec:classe-evenement}). 

\subsection{Classe \texttt{Evenement}}
\label{sec:classe-evenement}

Cette classe implémente l'interface \texttt{EventHandler} de la librairie \emph{Apache Commons Math} \cite{apacheJavadoc}. Cette interface décrit le comportement du détecteur d'événement du solveur. Ce détecteur est bien entendu optionnel et attaché si besoin au solveur.

Toutes les informations ci-après sont illustrées dans la javadoc dans la section consacrée à \texttt{EventHandler} \cite{apacheJavadoc}. Le principe repose sur la détection de changement de signe d'une certaine fonction \texttt{g} fournie par l'utilisateur. Lorsqu'un événement est détecté la méthode \texttt{eventOccured} est appelée, cette dernière décide de l'une des quatre actions à effectuer : continuer ou arrêter l'intégration, effectuer un changement du vecteur d'état et recalculer le pas ou encore simplement recalculer le pas. S'il y a un changement dans le vecteur d'état qui est demandé alors la méthode \texttt{resetState} est appelée et cette dernière permet d'en modifier le contenu. 

La détection des événéments revient donc à détecter les zéros. La méthode de détection par défaut utilisée dans le projet est la méthode de Brent qui combine la méthode de dichotomie, la méthode de la sécante et l'interpolation quadratique inverse \cite{brentWiki}, mais d'autres sont possibles. Le détecteur nécessite aussi plusieurs paramètres de configuration tels que l'intervale maximal entre deux vérifications de la valeur de la fonction \texttt{g}, le seuil de convergence ou le nombre d'itération maximal lors de la recherche.

Dans le cadre du problème de la balle qui rebondit au sol, la valeur de la fonction \texttt{g} correspond simplement à la variable de position dans le vecteur d'état. L'utilisation d'un système basé sur la détection de passage à zéro amène en flexibilité et permet de modéliser à peu près tous les types d'événements physiques possibles. En revanche, ce système amène aussi des problèmes numériques qui seront discutés dans la section \ref{sec:detect-des-even}.

\subsection{Classe \texttt{EquationDiff}}
\label{sec:classe-equadiff}

C'est une classe abstraite qui représente une équation différentielle. Elle rassemble les opérations communes aux ODE d'ordre 1 et 2 telles que l'initialisation des constantes et des conditions initiales.

\subsection{Classe \texttt{EquationOrdre1/2}}
\label{sec:classe-equat}

Ces deux classes implémentent respectivement les interfaces \texttt{FirstOrderDifferentialEquations} et \texttt{SecondOrderDifferentialEquations}. Les objets sont ensuites fournis au solveur en tant que paramètre. Cette interface se caractérise par les méthodes \texttt{computeDerivatives} et \texttt{computeSecondDerivatives} qui sont en charge de calculer les différentes dérivées à chaque pas d'exécution.

Plusieurs tableaux de \texttt{double} sont passés en paramètre de ces méthodes. Deux pour l'ordre 1 et trois pour l'ordre deux. Dans les deux cas, les tableaux contenant le vecteur d'état et sa dérivée première sont présents. Dans le cas de l'ordre 2, le tableau contenant les dérivées secondes est aussi présent. Chaque tableau correspond à un ordre de dérivée et chaque élément du tableau correspond à une variable du vecteur d'état\footnote{On se limite ici à des problèmes à une dimension, seul l'indice 0 est donc utilisé}. 

\section{Déroulement de l'intégration}
\label{sec:dero-de-lint}

\begin{enumerate}
\item Initialisation de l'objet \texttt{Equation}
  \begin{enumerate}
  \item Construction de l'AST à partir du fichier source contenant une équation différentielle telle que décrite dans la section \ref{sec:repr-dune-equat}
  \item Parcours de l'arbre afin de récupérer les informations concernant les constantes et les conditions initiales
  \item Initialisation de la table des variables ainsi que de toutes les structures de données nécessaires au bon fonctionnement du solveur
  \item L'objet correspondant à l'équation est créé, une conversion vers le 1er ordre est éventuellement effectuée à l'aide de \texttt{FirstOrderConverter}
  \item Création du solveur en lui-même, on considère par défaut un solveur de type Runge-Kutta d'ordre 4
  \item On attache ensuite à ce solveur un \texttt{StepHandler} et un \texttt{EventHandler} (si événement il y a), eux-même configurés à l'aide de paramètres par défaut.
  \item Fin de l'initialisation
  \end{enumerate}
\item Démarrage de l'intégration lorsque la méthode \texttt{integrate} est appelée. Chaque pas est effectué lorsque les sémaphores sont débloqués à l'aide de la méthode \texttt{update}, les dérivées sont calculées à l'aide respectivement des méthodes \texttt{computeDerivatives} et \texttt{computeSecondDerivatives}
  \begin{enumerate}
  \item Evaluation éventuelle de la fonction \texttt{g} en fonction de paramètres propre au solveur
  \item Si un événement est détecté alors la fonction \texttt{eventOccured} est appelée et l'action choisie exécutée
  \item Sinon la méthode \texttt{handleStep} est appelée. Dans tous les cas, \texttt{eventOccured} est appelée avant \texttt{handleStep}. Pour plus de détails, se référer à la javadoc \cite{apacheJavadoc} de la méthode \texttt{eventOccured} de l'interface \texttt{EventHandler}
  \end{enumerate}
\end{enumerate}

\section{Problèmes et contraintes}
\label{sec:probl-et-contr}

\subsection{Détection des événements}
\label{sec:detect-des-even}

La méthode de détection des événéments qui se base sur la détection de zéros possède des défauts inhérents liés à la précision numérique. Par exemple lors du problème de la chute de balle, au bout d'un certain nombre de rebonds, les changements de signe ne sont plus détectés par l'algorithme de Brent et les rebonds ne sont donc plus comptabilisés, par conséquent la balle finit par chuter indéfiniment lorsque la valeur de la position devient trop petite.

Beaucoup de paramètres entrent en jeu dans cette problèmatique : le manque de précision des données, les paramètres du détecteur d'événement, l'algorithme utilisé, problème lors du reset du vecteur d'état, une étude plus approfondie sur le fonctionnement de la détection de changement de signe serait nécessaire afin de mieux saisir tous les enjeux de cette fonctionnalité de la librairie.

\subsection{Mise à jour de la table des variables}
\label{sec:mise-jour-de}

Une table contenant le nom des variables et leur valeurs associées doit être maintenue à jour en fonction des informations fournies par le solveur à chaque étape. Elle doit être mise à jour avec ces informations avant chaque évalutation d'une expression mathématique par MESP. Cela suppose donc une étape supplémentaire qui peut s'avérer non-négligeable à terme. Le problème est limité et légérement compensé par le fait que MESP est extrêmement rapide (benchmark effectué avec JEP\footnote{http://www.singularsys.com/jep/} et formula4j\footnote{http://www.formula4j.com/}).
