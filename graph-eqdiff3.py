import numpy as np
import matplotlib.pyplot as plt

try:
    f1 = open("resultats.txt", "r")
except:
    print("erreur ouverture fichier")
    sys.exit(1);

temps1 = []
valeur1 = []

for line in f1:
    if (line.count(" ") == 0):
        continue;
    t, y = line.split(" ")
    temps1.append(float(t))
    valeur1.append(float(y))

f1.close()

plt.figure(1)
plt.plot(temps1, valeur1, linewidth = 2)
plt.axis([0, 20, -1, 12])
plt.show()