grammar EquaDiff;

options {
	language = Java;
	output = AST;
	ASTLabelType = CommonTree;
}

@rulecatch {
   catch (RecognitionException e) {
    throw e;
   }
}

equation 
	:	ordre vecteuretat condini cste derivees evt?
	;

ordre
	: 	ORDRE^ AFFECT! INT SEP!;

vecteuretat	
	: 	VECTETAT^ LACC! LPAREN! (ID)+ RPAREN! SEP! RACC!
	;

condini	
	:	CONDINI^ LACC! affectation+ RACC!
	;
	
cste	
	:	CSTES^ LACC! affectation+ RACC!
	;

evt
	:	EVT^ LACC! exprevt RACC!
	;

exprevt
	:	ACTION_EVT^ WHEN! expr SEP!
	|	ACT_RST_ST^ AS! affectation+ WHEN! expr SEP!
	;

affectation	
	:	ID AFFECT^ STRING SEP!	
	|	der AFFECT^ STRING SEP!
	;

derivees
	: DERIV^ LACC! (affectation)+ RACC!
	;

der	
	:	DER1^ DOUBLE_U! ID DOUBLE_U!
	|	DER2^ DOUBLE_U! ID DOUBLE_U!
	;

expr	
	:	term (ADDOP^ term)*
	|	ADDOP^ expr
	;

term	:	factor (MULTOP^ factor)*;

factor	:	ID | FLOAT | INT | ( LPAREN! expr RPAREN! ) | der;

DER1 
	:	'der1'
	;

DER2
	:	'der2'
	;
	
ORDRE
	:	'Ordre'
	;

LPAREN
	: 	'('
	;

RPAREN
	: 	')'
	;
	
DOUBLE_U
	:	'::'
	;
	
COMMA
	: 	','
	;
	
DERIV 
	:	'Derivees'
	;

VECTETAT 
	:	'VectEtat'
	;
	
CSTES 
	:	'Cstes'
	;

CONDINI
	:	'CondIni'
	;
	
EVT
	:	'Evenement'
	;

WHEN
	:	'when'
	;

THEN
	:	'then'
	;
	
AS
	:	'as'
	;
	
ACTION_EVT
	:	'RESET_DER'
	|	'STOP'
	;

ACT_RST_ST
	:	'RESET_STATE'
	;

LACC 
	:	'{'
	;
	
RACC
	:	'}'
	;
	
AFFECT
	: 	'='
	;

SEP :	';';

ID  :	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

INT :	'0'..'9'+
    ;

FLOAT
    :   ('0'..'9')+ '.' ('0'..'9')* EXPONENT?
    |   '.' ('0'..'9')+ EXPONENT?
    |   ('0'..'9')+ EXPONENT
    ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) {$channel=HIDDEN;}
    ;

ADDOP	:	'+' | '-';

MULTOP	:	'*' | '/';

STRING
    :  '"' ( ESC_SEQ | ~('\\'|'"') )* '"'
    ;

CHAR:  '\'' ( ESC_SEQ | ~('\''|'\\') ) '\''
    ;

fragment
EXPONENT : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;

fragment
HEX_DIGIT : ('0'..'9'|'a'..'f'|'A'..'F') ;

fragment
ESC_SEQ
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    |   UNICODE_ESC
    |   OCTAL_ESC
    ;

fragment
OCTAL_ESC
    :   '\\' ('0'..'3') ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7')
    ;

fragment
UNICODE_ESC
    :   '\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
    ;
