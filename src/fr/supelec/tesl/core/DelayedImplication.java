/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.core;


/*@
 * A clock implication which puts a tick on the slave clock each time n ticks have been counted on a counting clock after a tick on the reference clock.
 */
public class DelayedImplication<M1 extends Comparable<? super M1>,
                        M2 extends Comparable<? super M2>,
                        S  extends Comparable<? super S>> extends ImplicationRelation {
	private final boolean immediate_;
	//@ Number of ticks remaining to count
	private int delay_;
	//@ Total number of ticks of the delay
	private final int n_;
	//@ Are we counting?
	private boolean counting_;
	
	/**
	 * Get slave by delaying reference (immediately or not) by n on counting
	 * @param reference clock to delay
	 * @param counting clock for counting the delay
	 * @param slave delayed clock
	 * @param n length of the delay
	 * @param immediate does the delay start immediately (if 'counting' tick at the same time as 'reference')
	 */
	public DelayedImplication(Clock<M1> reference, Clock<M2> counting, Clock<S> slave, int n, boolean immediate) {
		super(slave, reference, counting);
		this.n_ = n;
		this.delay_ = 0;
		this.counting_ = false;
		this.immediate_ = immediate;
	}
	
	/**
	 * Get slave by delaying reference (not immediately) by n on counting
	 * @param reference clock to delay
	 * @param counting clock for counting the delay
	 * @param slave delayed clock
	 * @param n length of the delay
	 */
	public DelayedImplication(Clock<M1> reference, Clock<M2> counting, Clock<S> slave, int n) {
		this(reference, counting, slave, n, false);
	}	
		
	/**
	 * Return the reference clock, for the DelayedFor constraint
	 */
	public Clock<?> getReferenceClock() {
		return getMasterClocks()[0];
	}
	
	/**
	 * Return the counter clock, for the DelayedFor constraint
	 */
	public Clock<?> getCountingClock() {
		return getMasterClocks()[1];
	}
	
	/**
	 * Determine whether a tick should be set as "now" on the slave clock
	 * Impose the slave clock to tick synchronously with the n-th tick of 
	 * the counter clock (master2 clock) following a tick of the reference 
	 * clock (master1 clock) 
	 */
	@Override
	public boolean induceTicks() {
		// Has this implication been applied
		boolean applied = false;
		boolean start = false;
		
		// If the masster clock has a tick now, start counting n_ ticks on the counting clock
		if (getReferenceClock().hasRealTickNow()) {
			start = true;
			this.counting_ = true;
			this.delay_ = this.n_;
		}
		// If we are counting and the counting clock has a tick now, decrement the remaining delay
		if (this.getCountingClock().hasRealTickNow() && this.counting_ && ( (! start) || this.immediate_ )) {
			this.delay_ --;
			applied = true;
		}
		// If we are counting and the remaining delay is 0, set a now tick on the slave clock 
		// This test is not done inside the previous test because n_ can be 0
		if (this.counting_ && (this.delay_ == 0)) {
			this.counting_ = false;
			getSlaveClock().newTick().setNow(true);
			applied = true;
		}
		return applied;
	}

	@Override
	public String toString() {
		return "DelayedImplication: by " + this.n_ + (this.immediate_ ? ", immediate, " : ", delayed, ")
				+ (this.counting_ ? "counting (" + this.delay_ + "). " : "not counting. ")
				+ super.toString();
	}

	private class DelayedImplicationBackup implements ImplicationBackupInterface {
		public int backup_delay_;
		
		@SuppressWarnings("synthetic-access")
		public DelayedImplicationBackup() {
			this.backup_delay_ = DelayedImplication.this.delay_;
		}

		@Override
		public void restore() {
			DelayedImplication.this.restore(this);
		}
	}
	@Override
	public ImplicationBackupInterface backup() {
		return new DelayedImplicationBackup();
	}

	@Override
	public void restore(ImplicationBackupInterface bck) {
		@SuppressWarnings("unchecked")
		DelayedImplicationBackup backup = (DelayedImplicationBackup)bck;
		this.delay_ = backup.backup_delay_;
	}
}
