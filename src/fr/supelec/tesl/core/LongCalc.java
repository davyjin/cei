/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.core;

/**
 * A tag calculus on integers which uses the regular int operators
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class LongCalc implements TagCalculus<Long> {
	@Override
	public Long add(Long a, Long b) {
		return do_add(a, b);
	}
	public static Long do_add(Long a, Long b) {
		return a.longValue() + b.longValue();
	}

	@Override
	public Long subtract(Long a, Long b) {
		return do_subtract(a, b);
	}
	public static Long do_subtract(Long a, Long b) {
		return a.longValue() - b.longValue();
	}

	@Override
	public Long multiply(Long a, Long b) {
		return do_multiply(a, b);
	}
	public static Long do_multiply(Long a, Long b) {
		return a.longValue() * b.longValue();
	}

	@Override
	public Long divide(Long a, Long b) {
		return do_divide(a, b);
	}
	public static Long do_divide(Long a, Long b) {
		return a.longValue() / b.longValue();
	}
}
