/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.core;

public class TickDeletion<T extends Comparable<T>> extends ClockOperation<T> {
	private T tag_;
	
	public TickDeletion(Clock<T> clock, T tag) {
		super(clock);
		this.tag_ = tag;
	}

	@Override
	public void perform() {
		Clock<T> clock = this.getClock();
		Tick<T> tick = clock.getTick(this.tag_);
		if (tick != null) {
			clock.removeTick(tick);
		}
	}

}
