/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.core;


/*@
 * A clock implication which puts a tick on the slave clock each time some duration has elapsed 
 * on a time measuring clock after a tick on the reference clock.
 */
public class TimeDelayedImplication<M1 extends Comparable<? super M1>,
                        M2 extends Comparable<? super M2>,
                        S  extends Comparable<? super S>> extends ImplicationRelation {
	//@ Do I have to post a tick on the counting clock?
	private boolean should_post_tick_;
	//@ Did I already created a tick on the slave clock?
	private boolean slave_tick_created_;
	//@ Duration of the delay
	private final M2 time_delay_;
	
	/**
	 * Get tick on slave by delaying ticks on reference by duration d on measuring
	 * @param reference clock to delay
	 * @param measuring clock for timing the delay
	 * @param slave delayed clock
	 * @param d duration of the delay
	 */
	public TimeDelayedImplication(Clock<M1> reference, Clock<M2> measuring, Clock<S> slave, M2 d) {
		super(slave, reference, measuring);
		this.time_delay_ = d;
		this.should_post_tick_ = false;
		this.slave_tick_created_ = false;
	}
			
	/**
	 * Return the reference clock, for the TimeDelayedBy constraint
	 */
	public Clock<?> getReferenceClock() {
		return getMasterClocks()[0];
	}
	
	/**
	 * Return the measuring clock, for the TimeDelayedBy constraint
	 */
	public Clock<?> getMeasuringClock() {
		return getMasterClocks()[1];
	}
	
	@Override
	public void startSolving() {
		super.startSolving();
		this.slave_tick_created_ = false;
		this.should_post_tick_ = false;
	}
	
	@Override
	public void endSolving() {
		if (this.should_post_tick_) {
			throw new Error("# Error: " + this + " could not find current tag on " + getMeasuringClock());
		}
	}
	
	/**
	 * Determine whether a tick should be set as "now" on the slave clock.
	 */
	@Override
	public boolean induceTicks() {
		boolean applied = false;
		ClockSet solver = getReferenceClock().getSolver();
		
		// If we do not already know that we should post a tick on the measuring clock,
		// check if there is a now tick on the master clock.
		if (! this.should_post_tick_) {
			Tick<? extends Comparable<?>> now = this.getReferenceClock().getNowTick();
			this.should_post_tick_ = !solver.isGhost(now);
		}

		@SuppressWarnings("unchecked")
		Clock<M2> measuringClock = (Clock<M2>) getMeasuringClock();
		// Get the current time on the counting clock
		Tag<M2> tag = measuringClock.getCurrentTag();
		// If we haven't already created a tick on the slave at this instant
		if (!this.slave_tick_created_) {
			Tick<M2> tick = measuringClock.getNowTick();
			if ((tick != null) && (tick.getUserData(this) != null)) {
				// If the measuring clock has a now tick that this relation has created
				// create the implied tick on the slave clock
				getSlaveClock().newTick().setNow(true);
				this.slave_tick_created_ = true;
				applied = true;
			}
		}
		// Check if we should and could post a tick on the measuring clock
		if ((this.should_post_tick_) && (tag != null) && (tag.getValue() != null)) {
			// If we know the current time on the measuring clock, compute the tag of the next implication
			M2 nexttag = measuringClock.tagSum((M2)(tag.getValue()), this.time_delay_);
			measuringClock.newTick(nexttag).setUserData(this, true);
			this.should_post_tick_ = false;
			applied = true;
		}
		return applied;
	}

	@Override
	public String toString() {
		return "TimeDelayedImplication: " + this.time_delay_ + ", " + super.toString();
	}

	private class DelayedImplicationBackup implements ImplicationBackupInterface {
		public boolean should_post_tick_;
		public boolean slave_tick_created_;
		
		@SuppressWarnings("synthetic-access")
		public DelayedImplicationBackup() {
			this.should_post_tick_ = TimeDelayedImplication.this.should_post_tick_;
			this.slave_tick_created_ = TimeDelayedImplication.this.slave_tick_created_;
		}

		@Override
		public void restore() {
			TimeDelayedImplication.this.restore(this);
		}
	}
	@Override
	public ImplicationBackupInterface backup() {
		return new DelayedImplicationBackup();
	}

	@Override
	public void restore(ImplicationBackupInterface bck) {
		@SuppressWarnings("unchecked")
		DelayedImplicationBackup backup = (DelayedImplicationBackup)bck;
		this.should_post_tick_ = backup.should_post_tick_;
		this.slave_tick_created_ = backup.slave_tick_created_;
	}
}
