/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.core;

/**
 * A tag calculus on integers which uses the regular int operators
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class IntegerCalc implements TagCalculus<Integer> {
	@Override
	public Integer add(Integer a, Integer b) {
		return do_add(a, b);
	}
	public static Integer do_add(Integer a, Integer b) {
		return a.intValue() + b.intValue();
	}

	@Override
	public Integer subtract(Integer a, Integer b) {
		return do_subtract(a, b);
	}
	public static Integer do_subtract(Integer a, Integer b) {
		return a.intValue() - b.intValue();
	}

	@Override
	public Integer multiply(Integer a, Integer b) {
		return do_multiply(a, b);
	}
	public static Integer do_multiply(Integer a, Integer b) {
		return a.intValue() * b.intValue();
	}

	@Override
	public Integer divide(Integer a, Integer b) {
		return do_divide(a, b);
	}
	public static Integer do_divide(Integer a, Integer b) {
		return a.intValue() / b.intValue();
	}
}
