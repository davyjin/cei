/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.core;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * A tag calculus on doubles which keeps only the 6 first digits after
 * the decimal point and rounds to the nearest value, or down if there
 * is no nearest value.
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class RoundDouble implements TagCalculus<Double> {
	private static final int num_digits_ = 6;
	private static final RoundingMode rounding_mode_= RoundingMode.HALF_DOWN;
		
	@Override
	public Double add(Double a, Double b) {
		return do_add(a, b);
	}
	public static Double do_add(Double a, Double b) {
		return BigDecimal.valueOf(a + b).setScale(num_digits_, rounding_mode_).doubleValue();
	}

	@Override
	public Double subtract(Double a, Double b) {
		return do_subtract(a, b);
	}
	public static Double do_subtract(Double a, Double b) {
		return BigDecimal.valueOf(a - b).setScale(num_digits_, rounding_mode_).doubleValue();
	}

	@Override
	public Double multiply(Double a, Double b) {
		return do_multiply(a, b);
	}
	public static Double do_multiply(Double a, Double b) {
		return BigDecimal.valueOf(a * b).setScale(num_digits_, rounding_mode_).doubleValue();
	}

	@Override
	public Double divide(Double a, Double b) {
		return do_divide(a, b);
	}
	public static Double do_divide(Double a, Double b) {
		return BigDecimal.valueOf(a / b).setScale(num_digits_, rounding_mode_).doubleValue();
	}
}
