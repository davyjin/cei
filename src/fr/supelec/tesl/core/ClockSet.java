/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.core;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.supelec.logging.Logger;
import fr.supelec.logging.SysErrHandler;
//import java.util.logging.Level;

//import fr.supelec.tesl.core.Logging;

/*@
 * A solver for clock relations
 */
public class ClockSet {
	//@ The name of the solver
	private final String name_;
	//@ The logger for this solver
	private final Logger logger_;
	//@ The clock of this solver
	private List<Clock<?>> clocks_;
	//@ The implication relations (event triggers)
	private List<ImplicationRelation> c_implications_;
	//@ The tag relations (simultaneity, time triggers)
	private List<TagRelation<?,?>> t_relations_;
	//@ The implication relations classified by master clocks
	private Map<Clock<?>, List<ImplicationRelation>> masters_;
	//@ The implication relations classified by slave clocks
	private Map<Clock<?>, List<ImplicationRelation>> slaves_;
	//@ The ticks which are ghosts used by the solving algorithm (not real ticks) and may disappear at the end 
	private List<Tick<? extends Comparable<?>>> floatingGhosts_;
	//@ The belonging of clocks to time islands (connected subgraphs of the tag relations graph)
	private Map<Clock<? extends Comparable<?>>, Set<Clock<? extends Comparable<?>>>> time_islands_;
	//@ The transitive closure of the tag relations, computed for each clock assuming its current time is now
	private Map<Clock<? extends Comparable<?>>, Map<Clock<? extends Comparable<?>>, Tag<? extends Comparable<?>>>> sync_tags_; 

	//@ Create a new ClokSet for solving relations between clocks
	public ClockSet(String name) {
		this.name_ = name;
		this.logger_ = Logger.getLogger(name_);
		this.clocks_ = new LinkedList<Clock<?>>();
		this.c_implications_ = new LinkedList<ImplicationRelation>();
		this.t_relations_ = new LinkedList<TagRelation<?,?>>();
		this.masters_ = new HashMap<Clock<?>, List<ImplicationRelation>>();
		this.slaves_ = new HashMap<Clock<?>, List<ImplicationRelation>>();
		this.floatingGhosts_ = new LinkedList<Tick<? extends Comparable<?>>>();
		this.time_islands_ = new HashMap<Clock<? extends Comparable<?>>, Set<Clock<? extends Comparable<?>>>>();
		this.logger_.addInformationKinds("error", "info", "fine", "finer");
		if (this.logger_.getHandlers().size() == 0) {
			this.logger_.addHandler(new SysErrHandler());
		}
	}

	//@ Create a new ClockSet with a default name
	public ClockSet() {
		this("TESL solver");
	}
	
	//@ Get the logger of this solver
	public Logger getLogger() {
		return this.logger_;
	}
	
	//@ Add a clock to this solver
	public <T extends Comparable<? super T>> Clock<T> addClock(Clock<T> c) {
		if ( !this.clocks_.contains(c)) {
			this.clocks_.add(c);
			c.setSolver(this);
		}
		return c;
	}

	//@ Add a clock relation to this solver
	public ImplicationRelation addImplicationRelation(ImplicationRelation r) {
		if ( this.c_implications_.contains(r) ) {
			return r;
		}
		this.c_implications_.add(r);
		r.setSolver(this);
		// Get the list of relations associated to the first master clock of the relation
		List<ImplicationRelation> l = this.masters_.get(r.getMasterClocks()[0]);
		if (l == null) {
			l = new LinkedList<ImplicationRelation>();
			for (Clock<?> clk : r.getMasterClocks()) {
				this.masters_.put(clk, l);
			}
		}
		l.add(r);
		l = this.slaves_.get(r.getSlaveClock());
		if (l == null) {
			l = new LinkedList<ImplicationRelation>();
			this.slaves_.put(r.getSlaveClock(), l);
		}
		l.add(r);
		return r;
	}

	//@ Add a relation between the tags of two clocks to this solver
	public TagRelation<?,?> addTagRelation(TagRelation<?,?> t) {
		if (this.t_relations_.contains(t)) {
			return t;
		}
		this.t_relations_.add(t);
		t.setSolver(this);
		return t;
	}

	//@ Get the clocks of this solver
	public Iterable<Clock<?>> getClocks() {
		return this.clocks_;
	}

	//@ Reset the clocks of this solver
	public void resetClocks() {
		for (Clock<?> c : this.clocks_) {
			c.resetClock();
		}
	}
	
	//@ Remove the now tick on every clock
	public void removeNowTicks() {
		for (Clock<?> c : this.clocks_) {
			c.removeNowTick();;
		}
	}
	
	//@ Get the clock relations of this solver
	public Iterable<ImplicationRelation> getImplicationRelations() {
		return this.c_implications_;
	}

	//@ Get the relation between clock tags of this solver
	public Iterable<TagRelation<?,?>> getTagRelations() {
		return this.t_relations_;
	}

	public ClockSetMark getMark() {
		ClockSetMark mark = new ClockSetMark();
		for (Clock<?> clk : this.clocks_) {
			mark.addClock(clk.backup());
		}
		for (ImplicationRelation imp : this.c_implications_) {
			mark.addImplication(imp.backup());
		}
		return mark; 
	}

	public void setMark(ClockSetMark mark) {
		for (ClockBackupInterface<?> bck : mark.clockBackups()) {
			bck.restore();
		}
		for (ImplicationBackupInterface bck : mark.implicationBackups()) {
			bck.restore();
		}
	}

	private void findTimeIslands() {
		this.time_islands_.clear();
		// Populate with one clock per island
		for (Clock<? extends Comparable<?>> clk : getClocks()) {
			Set<Clock<? extends Comparable<?>>> s = new HashSet<Clock<? extends Comparable<?>>>();
			s.add(clk);
			this.time_islands_.put(clk, s);
		}
		// Merge islands according to tag relations 
		for (TagRelation<? extends Comparable<?>, ? extends Comparable<?>> rel : this.t_relations_) {
			Clock<? extends Comparable<?>> c1 = rel.fromClock();
			Clock<? extends Comparable<?>> c2 = rel.toClock();
			Set<Clock<? extends Comparable<?>>>island1 = this.time_islands_.get(c1);
			Set<Clock<? extends Comparable<?>>>island2 = this.time_islands_.get(c2);
			if ((island1 == null) && (island2 == null)) {
				island1 = island2 = new HashSet<Clock<? extends Comparable<?>>>();
				island1.add(c1);
				island1.add(c2);
				this.time_islands_.put(c1,  island1);
				this.time_islands_.put(c2,  island1);
			} else if ((island1 != null) && (island2 != null)) {
				if (island1 != island2) {
					island1.addAll(island2);
					for (Clock<? extends Comparable<?>> c : island2) {
						this.time_islands_.remove(c);
						this.time_islands_.put(c, island1);
					}
				}
			} else if (island1 == null) {
				island2.add(c1);
				this.time_islands_.put(c1, island2);
			} else {
				island1.add(c2);
				this.time_islands_.put(c2, island1);
			}
		}
		this.logger_.log("fine",
			new Object() {
				public String toString() {
					StringBuffer buf = new StringBuffer("Time islands:"+Logger.EOL);
					for (Clock<? extends Comparable<?>> robinson : time_islands_.keySet()) {
						buf.append(Logger.TAB);
						buf.append("Island of ");
						buf.append(robinson.getName());
						buf.append(" = {");
						for (Clock<? extends Comparable<?>> friday : time_islands_.get(robinson)) {
							buf.append(friday.getName());
							buf.append(" ");
						}
						buf.append("}");
						buf.append(Logger.EOL);
					}
					return buf.toString();
				}
		    }
		);
	}

	public ClockSet solve() throws Exception {
		return this.solve((Tick<Unit>)null);
	}

	//@ Perform a solving phase: create ticks induced by relations and compute their tags at this instant
	public <T extends Comparable<T>>  ClockSet solve(Tick<T> tick) throws Exception {
		this.logger_.log("fine",
			new Object() {
				public String toString() {
					StringBuffer buf = new StringBuffer(Logger.EOL);
					for (ImplicationRelation r : c_implications_) {
						buf.append(Logger.TAB);
						buf.append(r.toString());
						buf.append(Logger.EOL);
					}
					return buf.toString();
				}
			}
		);
		
		if (tick != null) {
			tick.setNow(true);
		}
		this.sync_tags_ = new HashMap<Clock<? extends Comparable<?>>, Map<Clock<? extends Comparable<?>>, Tag<? extends Comparable<?>>>>();

		findTimeIslands();

		this.logger_.log("info", new ClockPrinter("START"));

		cleanUpClocks();

		this.logger_.log("finer", new ClockPrinter("After cleanup"));

		// Start solving clock constraints and tag relation
		// only if there is a 'now' tick on Clock clk
		//checkOnlyOneNow();
		//setNowTickOnClock(clock, tick);

		// First, sort the relation in topological order to optimize solving
		doTopoSort();
		// Tell each relation that a new solving phase starts
		for (ImplicationRelation r : this.c_implications_) {
			r.startSolving();
		}
		// While new ticks are created at the current instant, apply relations and compute tags
		boolean new_tick;
		int i = 0;
		do {
			this.logger_.log("fine", new ClockPrinter("*** ITERATION: " + (++i)));
			do {
				this.logger_.log("finer", new ClockPrinter("2 - Before solving ticks"));

				new_tick = solveTicks();

				this.logger_.log("finer", new ClockPrinter("3 - After solving ticks"));

				boolean tick_added = solveTags();
				new_tick = new_tick || tick_added;

				this.logger_.log("finer", new ClockPrinter("4 - After solving tags"));
			} while (new_tick);

			this.logger_.log("finer", new ClockPrinter("5 - Before merging ticks"));

			new_tick = mergeTicks();

			this.logger_.log("finer", new ClockPrinter("6 - After merging ticks"));

			updateTagRelationClosure();
			
			if ( ! new_tick) {
				new_tick = alignFloatingTime();
				this.logger_.log("finer", new ClockPrinter("7 - After aligning floating times"));
			}
		} while (new_tick);	
		// Tell each relation that thesolving phase has ended (useful for detecting inconsistencies)
		for (ImplicationRelation r : this.c_implications_) {
			r.endSolving();
		}

		this.logger_.log("info", new ClockPrinter("*** Result"));

		// check that 'now' tick it's the first tick on each clock
		for(Clock<?> clk: this.clocks_) {
			if (!checkFirstTick(clk)) { // if is not the first tick, throw exception
				this.logger_.log("error", "The 'now' tick is not the first one for clock ", clk.getName());
				throw new Exception("# ERROR: The 'now' tick is not the first one for clock "+ clk.getName());
			}
		}

		return this;
	}

	//@ Clean up clocks: remove duplicate ticks, taking care of not removing "now" ticks
	private void cleanUpClocks() {
		for (Clock<? extends Comparable<?>> clock : this.clocks_) {
			clock.cleanUp();
		}
	}
	
	//@ Check how many 'now' ticks are on the input clocks
	public boolean checkNoNow() {
		int count = 0;
		for (Clock<?> clk : this.clocks_) {
			for (Tick<?> t : clk.getTicks()) {
				if (t.isNow()) {
					count ++;
				}
			}
		}
		if (count != 0) { // there is, at least, one clock with a 'now' tick		
			return false;
		} else { // no clock has a 'now' tick
			return true;			
		}
	}	

	//@ Check first tick of a clock, if this is 'now'
	protected <T extends Comparable<? super T>> boolean checkFirstTick(Clock<T> clk) {		
		if(clk.getNowTick() != null) {
			if(clk.getNowTick().getTag() == null) {
				return true; // it's OK, because 'null' is the 'soon as possible' tag
			} else {
				if(clk.getNowTick().getTag().compareTo(clk.getTick(0).getTag()) > 0) {
					return false;
				}
			}
		}							
		return true;
	}

	//@ Tell whether r1 depends on r2
	public boolean dependsOn(ImplicationRelation r1, ImplicationRelation r2) {
		for (Clock<? extends Comparable<?>> clock : r1.getMasterClocks()) {
			List<ImplicationRelation> slaves = this.slaves_.get(clock);
			if (slaves != null) {
				for (ImplicationRelation r : slaves) {
					if (r == r2) {
						// If the slave of r2 is the master of r1, r1 depends on r2
						// This is because when applying r2, we may add ticks to its slave, which is the master of r1 
						return true;
					}
					if (r == r1) {
						this.logger_.log("error", "Relation ", r1, " depends on itself");
						throw new RuntimeException("Error: relation "+r1+" depends on itself");
					}
					if (dependsOn(r, r2)) {
						// r1 depend on r, so if r depends on r2, r1 depends on r2
						return true;
					}
				}
			}
		}
		return false;
	}

	//@ Sort clock relations in topological order (less dependent first)
	protected void doTopoSort() {
		Collections.sort(this.c_implications_, new Comparator<ImplicationRelation>() {
			@Override
			public int compare(ImplicationRelation arg0, ImplicationRelation arg1) {
				if (dependsOn(arg0, arg1)) {
					return 1;
				}
				if (dependsOn(arg1, arg0)) {
					return -1;
				}
				return 0;
			}			
		});
	}

	//@ Apply clock relations to find which clocks have a tick "now"
	protected boolean solveTicks() {
		boolean changed;
		boolean new_ticks = false;

		// Apply relations until no new tick is produced now
		do {
			changed = false;
			boolean has_now = false;
			for (Clock<? extends Comparable<?>> clock : this.clocks_) {
				if (clock.hasTickNow()) {
					has_now = true;
					break;
				}
			}
			if (! has_now) {
				// No now tick => nothing to do
				return false;
			}
			for (Clock<? extends Comparable<?>> clock : this.clocks_) {
				Tick<? extends Comparable<?>> floating = clock.getFloatingTick();
				Tick<? extends Comparable<?>> now = clock.getNowTick();
				// A floating tick should be "now" ASAP.
				// If there are no other now tick on the clock, make the floating tick "now"
				if ((now == null) && (floating != null)) {
					floating.setNow(true);
				}
			}

			for (ImplicationRelation r : this.c_implications_) {
				changed = changed || r.apply(); 
			}
			new_ticks = new_ticks || changed;
		} while (changed);

		return new_ticks;
	}

	//@ Use tag relations to compute the tags of the ticks. Return true if new ticks become "now" 
	protected boolean solveTags() throws Exception {
		boolean new_ticks = false;
		boolean tick_added = false;

		do {
			tick_added = false;
			for (Clock<? extends Comparable<?>> clock : this.clocks_) {
				this.logger_.log("fine", "Solving tags from clock " + clock.getName());
				if (clock.hasTickNow()) {
					Tag<? extends Comparable<?>> tag = clock.getNowTick().getTag(); 
					if (tag != null) {
						Map<Clock<? extends Comparable<?>>, Tag<? extends Comparable<?>>> closure = tagClosure(clock, tag);
						this.logger_.log("fine", closure);
						for (Clock<? extends Comparable<?>> oclock : this.clocks_) {
							if (oclock == clock) {
								continue;
							}
							Tag<? extends Comparable<?>> otag = closure.get(oclock);
							if (otag != null) {
								Tick<? extends Comparable<?>> now = getTickWithTag(oclock, otag);
								if (now != null) {
									// There is a tick with this tag on the clock
									if ( ! now.isNow() ) {
										// It is not 'now' yet, check if another tick is now
										Tick<? extends Comparable<?>> onow = oclock.getNowTick();
										if (onow != null) {
											if (onow.getTag() == null) {
												// If the other now tick is floating, remove it
												now.mergeUserData(onow);
												removeTick(oclock, onow);
											} else {
												// Weird: the tick with the 'now' tag is not "now",
												// but there is another tick which is "now" and has a tag
												Tag<? extends Comparable<?>> onowtag = onow.getTag();
												if (onowtag.equals(otag)) {
													// OK, it was just two ticks with the same tag, remove the duplicate one
													now.mergeUserData(onow);
													removeTick(oclock, onow);
												} else {
													// Inconsistent equations
													this.logger_.log("error", "Clock ", oclock.getName(), " has a now tick with tag ", onowtag.toString(), " but should be now at ", otag.toString());
													throw new Exception("Clock " + oclock.getName() + " has a now tick with tag " + onowtag.toString() + " but should be now at " + otag.toString());
												}
											}
										}
										// Set the tick with the 'now' tag to 'now'
										now.setNow(true);
										tick_added= true;
									}
								} else {
									// Clock does not have a tick with the 'now' tag
									Tick<? extends Comparable<?>> onow = oclock.getNowTick();
									if (onow != null) {
										// Clock has a "now" tick
										Tag<? extends Comparable<?>> onowtag = onow.getTag();
										if (onowtag == null) {
											// We have a floating tick: set its tag to the 'now' tag
											setTag(onow, otag);
										} else {
											// Weird: we have a now tick, but is has the wrong tag
											this.logger_.log("error", "Clock ", oclock.getName(), " has a now tick with tag ", onowtag.toString(), " but should be now at ", otag.toString(), Logger.EOL, this);
											throw new Exception("Clock " + oclock.getName() + " has a now tick with tag " + onowtag.toString() + " but should be now at " + otag.toString());
										}
									} else {
										// Look for a floating tick
										Tick<? extends Comparable<?>> floating = oclock.getFloatingTick();
										if (floating != null) {
											setTag(floating, otag);
											floating.setNow(true);
										}
									}
									for (Tick<? extends Comparable<?>> tick : oclock.getTicks()) {
										Tag<? extends Comparable<?>> ctag = tick.getTag();
										if (ctag == null) {
											continue;
										}
										if (compareTags(ctag, otag) >= 0) {
											break;
										}
										this.logger_.log("error", "Clock ", oclock.getName(), " has a tick in the past (", ctag.toString(), "). Now is at ", otag.toString(), Logger.EOL, this);
										throw new Exception("Clock " + oclock.getName() + " has a tick in the past (" + ctag.toString() + "). Now is at " + otag.toString());
									}
								}
							}
						}
					}
				}
			}
			new_ticks = new_ticks || tick_added;
		} while (tick_added) ;
		return new_ticks;
	}

	@SuppressWarnings("unchecked")
	private static <T extends Comparable<? super T>> void setTag(Tick<T> tick, Tag<? extends Comparable<?>> tag) {
		tick.setTag((Tag<T>)tag);
	}

	@SuppressWarnings("unchecked")
	private static <T extends Comparable<? super T>> void removeTick(Clock<T> clk, Tick<? extends Comparable<?>> tick) {
		clk.removeTick((Tick<T>)tick);
	}

	@SuppressWarnings("unchecked")
	private static <T extends Comparable<? super T>> Tick<T> getTickWithTag(Clock<T> clk, Tag<? extends Comparable<?>> tag) {
		return clk.getTick((Tag<T>)tag);
	}

	private Map<Clock<? extends Comparable<?>>, Tag<? extends Comparable<?>>>
	tagClosure(Clock<? extends Comparable<?>> clock, Tag<? extends Comparable<?>> tag) throws Exception {

		Map<Clock<? extends Comparable<?>>, Tag<? extends Comparable<?>>> closure = 
				new HashMap<Clock<? extends Comparable<?>>, Tag<? extends Comparable<?>>>();
		closure.put(clock, tag);

		// Compute tags on other clocks assuming clock "clock" has tag "tag" 
		boolean done = false;
		int turns = 0;
		while (! done) {
			if (turns > this.clocks_.size()) {
				// If we iterate more than the number of clocks, something is wrong with the tag relations
				this.logger_.log("error", "Too many iterations while computing tags ticks. Are the tag relations sound?");
				throw new Exception("Too many iterations while computing tags ticks. Are the tag relations sound?");
			}
			turns++;
			done = true;
			for (TagRelation<?, ?> rel : this.t_relations_) {
				Tag<?> from_tag = closure.get(rel.fromClock());
				Tag<?> to_tag = closure.get(rel.toClock());

				this.logger_.log("fine", "Processing tag relation ", rel, Logger.EOL,
						                 Logger.TAB, "From tag = ", from_tag, ", to tag = ", to_tag);

				if ((from_tag != null) && (to_tag == null)) {
					closure.put(rel.toClock(), processConversion(rel, from_tag));
					done = false;  // we updated a tag, so we must propagate through tag relations
				}
				if ((to_tag != null) && (from_tag == null)) {
					closure.put(rel.fromClock(), processRevConversion(rel, to_tag));
					done = false; // we updated a tag, so we must propagate through tag relations
				}
			}
		}
		return closure;

	}
	//@ Merge the ticks of a clock . Return true if new ticks become "now" 
	protected boolean mergeTicks() throws Exception {
		// List of clocks where a floating tick may be merged with a tagged tick
		List<Clock<? extends Comparable<?>>> merge_clocks = new LinkedList<Clock<? extends Comparable<?>>>();

		for (Clock<? extends Comparable<?>> clk : this.clocks_) {
			Tick<? extends Comparable<?>> floating = clk.getFloatingTick();
			if (floating != null && floating.isNow() && clk.hasTaggedTick()) {
				// If the floating tick is now and there is a tagged tick, we may merge them
				merge_clocks.add(clk);
			}
		}

		if (merge_clocks.size() == 0) {
			return false;
		}

		// Map: clock c1 => (the map: clock c2 => the tag that would be now on c2 if we merged the floating tick on clock c1)
		Map<Clock<? extends Comparable<?>>, Map<Clock<? extends Comparable<?>>, Tag<? extends Comparable<?>>>> sync_tags = 
				new HashMap<Clock<? extends Comparable<?>>, Map<Clock<? extends Comparable<?>>, Tag<? extends Comparable<?>>>>();

		for (Clock<? extends Comparable<?>> clk : merge_clocks) {
			// Map: c => tag of "now" on c assuming we merge the floating tick on clk
			sync_tags.put(clk, tagClosure(clk, clk.getFirstTaggedTick().getTag()));
		}

		// Find the time islands to which the merging clocks belong
		Set<Set<Clock<? extends Comparable<?>>>> islands = new HashSet<Set<Clock<? extends Comparable<?>>>>();
		for (Clock<? extends Comparable<?>> clk : merge_clocks) {
			islands.add(this.time_islands_.get(clk));
		}
		// In each time island, look for the merging clock that gives the smallest tags
		for (Set<Clock<? extends Comparable<?>>> island : islands) {
			this.logger_.log("fine", "# Processing new island");
			// Clocks of the island that have a tick to merge
			List<Clock<? extends Comparable<?>>> to_merge = new LinkedList<Clock<? extends Comparable<?>>>();  
			for (Clock<? extends Comparable<?>> clock : island) {
				if (merge_clocks.contains(clock)) {
					to_merge.add(clock);
				}
			}
			final int size = to_merge.size(); // should be at least 1
			// Build an array of tag for easy navigation along columns
			// matrix[i][j] = tag of now on clock j if we merge the floating tick on clock i
			@SuppressWarnings("unchecked")
			final Tag<? extends Comparable<?>>[][] matrix = new Tag[size][size];
			int i = 0;
			for (Clock<? extends Comparable<?>> clock : to_merge) {
				Map<Clock<? extends Comparable<?>>, Tag<? extends Comparable<?>>> current_tags = sync_tags.get(clock);
				this.logger_.log("fine", "sync_tags["+i+"]= ", current_tags);
				int j = 0;
				for (Clock<? extends Comparable<?>> oclock : to_merge) {
					matrix[i][j] = current_tags.get(oclock);
					this.logger_.log("finer", "sync_tags["+i+"]["+j+"] = ", matrix[i][j]);
					j++;
				}
				i++;
			}
			this.logger_.log("fine",
				new Object() {
					public String toString() {
						StringBuffer buf = new StringBuffer();
						buf.append("==== Merge/Tag array =======");
						buf.append(Logger.EOL);
						for (int a = 0; a < size; a++) {
							buf.append(Logger.TAB);
							for (int b = 0; b < size; b++) {
								buf.append(matrix[a][b]);
								buf.append("  ");
							}
							buf.append(Logger.EOL);
						}
						return buf.toString();
					}
				}
			);
			// Look across columns for a minimum line 
			int min = 0;
			for (int k = 0; k < size; k++) {
				int cmp = (int) Math.signum(compareTags(matrix[k][0], matrix[min][0]));
				// Check that tag order is the same on all other clocks
				for (int z = 1; z < size; z++) {
					if (Math.signum(compareTags(matrix[k][z], matrix[min][z])) != cmp) {
						this.logger_.log("error", "Non homogeneous tag relations between ", to_merge.get(k), " and ", to_merge.get(z));
						throw new Exception("# Error : non homogeneous tag relations between " + to_merge.get(k) + " and " + to_merge.get(z));
					}
				}
				if (cmp < 0) {
					min = k;
				}
			}
			// Merge on 'min', the clock that gives the smallest tags
			this.logger_.log("info", "Merging on ", to_merge.get(min));
			mergeNullTick(to_merge.get(min));
		}

		return true;
	}	

	@SuppressWarnings("unchecked")
	private static <U extends Comparable<? super U>, V extends Comparable<? super V>> Tag<V> processConversion(TagRelation<U, V> rel, Tag<?> tag) {
		return new Tag<V>(rel.directConversion((U) tag.getValue()));
	}

	@SuppressWarnings("unchecked")
	private static <U extends Comparable<? super U>, V extends Comparable<? super V>> Tag<U> processRevConversion(TagRelation<U, V> rel, Tag<?> tag) {
		return new Tag<U>(rel.reverseConversion((V) tag.getValue()));
	}

	@SuppressWarnings("unchecked")
	private static <T extends Comparable<? super T>, U extends Comparable<? super U>> int compareTags(Tag<T> t1, Tag<U> t2) {
		return t1.compareTo((Tag<T>) t2);
	}
	
	private <T extends Comparable<? super T>> void mergeNullTick(Clock<T> clock) {
		Tick<T> floating = clock.getFloatingTick();
		// This one is no longer a ghost
		this.floatingGhosts_.remove(floating);
		// Remove other floating ticks from their clock
		for (Tick<? extends Comparable<?>> tick : this.floatingGhosts_) {
			tick.remove();
		}
		// There are no longer ghosts in this clockset 
		this.floatingGhosts_.clear();
		// Remove other floating ticks on this clock (they may not be ghosts)
		clock.removeTick(clock.getFloatingTick());
		for (Tick<T> t : clock.getTicks()) {
			// Set the first tick on this clock to now
			if (t.getTag() != null) {
				t.setNow(true);
				return;
			}
		}
	}

	/*@ Update the closure of the tag relations according to known now ticks and their tag at the current instant. */
	private void updateTagRelationClosure() throws Exception {
		for (Clock<? extends Comparable<?>> clock : this.clocks_) {
			Tick<? extends Comparable<?>> now = clock.getNowTick();
			if (now == null) {
				continue;
			}
			if (now.getTag() == null) {
				continue;
			}
			// If we have a now tick with a tag, compute the closure of the tag relations for this tick
			this.sync_tags_.put(clock, tagClosure(clock, now.getTag()));
		}
	}
	
	/*@ Get the current tag on a clock if known in the current tag relations closure. */
	public Tag<? extends Comparable<?>> getCurrentTag(Clock<? extends Comparable<?>> clock) {
		for (Map<Clock<? extends Comparable<?>>, Tag<? extends Comparable<?>>> map : this.sync_tags_.values()) {
			Tag<? extends Comparable<?>> tag = map.get(clock);
			if (tag != null) {
				return tag;
			}
		}
		return null;
	}
	
	/*@ If none of the now ticks imply a tag on some clock which has no now tick,
	 *  we could nevertheless set its first tick to "now" if it is greedy. 
	 *  However, this could be incompatible with some other clocks because of tag 
	 *  relations because this could put a tick in the past without having been present at some instant.
	 *  Instead, we add a floating tick than may be merged with the first tick of the clock 
	 *  in case there are several clocks in this case and a choice must be made. 
	 */
	private boolean alignFloatingTime() throws Exception {
		boolean new_tick = false;

		for (Clock<? extends Comparable<?>> clock : this.clocks_) {
			if ( (! clock.isGreedy()) || clock.hasTickNow() || (!clock.hasTicks()) ) {
				// If the clock is not greedy or already has a "now" tick or has no ticks, skip it
				continue;
			}
			if (getCurrentTag(clock) == null) {
				// If the clock does not have a tag according to some other "now" tick, it floats
				// so add a ghost tick to allow it to have a tick now
				Tick<? extends Comparable<?>> t = clock.newTick().setNow(true);
				this.floatingGhosts_.add(t);
				new_tick = true;
			}
		}
		return new_tick;
	}

	/*@ Tell if a tick is a ghost and should not be considered as definitive in the current instant.
	 *  A null tick is considered as a ghost because it does not exist.
	 */
	public boolean isGhost(Tick<? extends Comparable<?>> tick) {
		return (tick == null) || this.floatingGhosts_.contains(tick);
	}
	
	@Override
	public String toString() {
		return this.toString("");
	}
	
	public String toString(String message) {
		StringBuffer buf = new StringBuffer();
		buf.append(message);
		buf.append(System.getProperty("line.separator"));
		for (ImplicationRelation r : this.c_implications_) {
			buf.append(r.toString());
			buf.append(System.getProperty("line.separator"));
		}
		buf.append(System.getProperty("line.separator"));
		for (TagRelation<?,?> r : this.t_relations_) {
			buf.append(r.toString());
			buf.append(System.getProperty("line.separator"));
		}
		buf.append(System.getProperty("line.separator"));
		for (Clock<?> c : this.clocks_) {
			buf.append(c.toString());
			buf.append(System.getProperty("line.separator"));
		}
		return buf.toString();
	}

	private class ClockPrinter {
		private String message_;
		
		public ClockPrinter(String message) {
			this.message_ = message;
		}
		
		public String toString() {
			StringBuffer buf = new StringBuffer();
			buf.append(message_);
			buf.append(Logger.EOL);
			for (ImplicationRelation r : c_implications_) {
				buf.append(Logger.TAB);
				buf.append(r.toString());
				buf.append(Logger.EOL);
			}
			for (TagRelation<?,?> r : t_relations_) {
				buf.append(Logger.TAB);
				buf.append(r.toString());
				buf.append(Logger.EOL);
			}
			for (Clock<?> c : clocks_) {
				buf.append(Logger.TAB);
				buf.append(c.toString());
				buf.append(Logger.EOL);
			}
			return buf.toString();
		}
	}
}

