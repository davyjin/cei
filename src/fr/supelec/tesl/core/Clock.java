/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.core;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * A clock with tags of type T.
 */
public class Clock<T extends Comparable<? super T>> {
	private Map<Tick<T>, Tag<T>> tags_;
	private TagCalculus<T> tag_calculus_;
	private List<Tick<T>> ticks_;
	private Tick<T> now_;
	private boolean greedy_;
	private String name_;
	private ClockSet solver_;
	
	/*@ A backup of a clock */
	private class ClockBackup implements ClockBackupInterface<T> {
		/*@ list of ticks */
		public List<Tick<T>> ticks_backup_;
		/*@ Tags of the ticks */
		public Map<Tick<T>, Tag<T>> tags_backup_;
		/*@ Tick which is now */
		public Tick<T> now_backup_;
		/*@ Is this clock greedy? */
		public boolean greedy_backup_;
		
		@SuppressWarnings("synthetic-access")
		public ClockBackup() {
			this.ticks_backup_ = new LinkedList<Tick<T>>();
			this.tags_backup_ = new HashMap<Tick<T>, Tag<T>>();
			for (Tick<T> t : Clock.this.getTicks()) {
				Tick<T> copy = Clock.this.copyTick(t);
				this.ticks_backup_.add(copy);
				if (t == Clock.this.now_) {
					this.now_backup_ = copy;
				}
				Tag<T> tag = Clock.this.getTag(t);
				if (tag != null) {
					this.tags_backup_.put(copy, tag.copy());
				}
			}
			this.greedy_backup_ = Clock.this.greedy_;
		}
		
		@Override
		public void restore() {
			Clock.this.restore(this);
		}
	}
	
	public Clock(String name, TagCalculus<T> calc, boolean greedy) {
		this.name_ = name;
		this.tags_ = new HashMap<Tick<T>, Tag<T>>();
		this.tag_calculus_ = calc;
		this.ticks_ = new LinkedList<Tick<T>>();
		this.greedy_ = greedy;
	}
	
	public Clock(String name, TagCalculus<T> calc) {
		this(name, calc, true);
	}
	
	public ClockSet setSolver(ClockSet cs) {
		this.solver_ = cs;
		return cs;
	}
	
	public ClockSet getSolver() {
		return this.solver_;
	}
	
	@SuppressWarnings("unchecked")
	public Tag<T> getCurrentTag() {
		if (this.solver_ != null) {
			return (Tag<T>) this.solver_.getCurrentTag(this);
		} else {
			return null;
		}
	}
    //@ Make this clock imply clock <code>slave</code>.
    @SuppressWarnings("unchecked")
	public <S extends Comparable<? super S>> Implication<T,S> implies(Clock<S> slave) {
		return (Implication<T, S>) this.solver_.addImplicationRelation(new Implication<T, S>(this, slave));
    }

    //@ Make this clock imply clock <code>slave</code> with delay <code>delay</code> counted on clock <code>counting</code>.
    @SuppressWarnings("unchecked")
	public <S extends Comparable<S>, C extends Comparable<C>>
      DelayedImplication<T,C,S> impliesWithDelay(Clock<S> slave, Clock<C> counting, int delay, boolean immediate) {
		return (DelayedImplication<T,C,S>) this.solver_.addImplicationRelation(
				new DelayedImplication<T, C, S>(this, counting, slave, delay, immediate)
		);
    }

    //@ Helper for creating a non immediate delayed implication.
	public <S extends Comparable<S>, C extends Comparable<C>>
      DelayedImplication<T,C,S> impliesWithDelay(Clock<S> slave, Clock<C> counting, int delay) {
		return this.impliesWithDelay(slave, counting, delay, false);
    }

    //@ Make this clock imply clock <code>slave</code> through a filtering relation.
    @SuppressWarnings("unchecked")
	public <S extends Comparable<S>>
      FilteredImplication<T,S> impliesThroughFilter(Clock<S> slave, int skip, int count, int repskip, int repcount) {
		return (FilteredImplication<T,S>) this.solver_.addImplicationRelation(
				new FilteredImplication<T,S>(this, slave, skip, count, repskip, repcount)
		);
    }

    /** Make this clock sustain clock <code>slave</code> between occurrences of <code>start</code> and <code>stop</code>.
     * 
     * @param slave the slave clock
     * @param start the clock that starts the sustaining of slave
     * @param stop  the clock that stops the sustaining of slave
     * @param immediate if true, slave has a tick when the master and start occur at the same time
     * @param weak if true, slave does not get a tick when stop occurs at the same time as the master
     */
    @SuppressWarnings("unchecked")
	public <M2 extends Comparable<M2>, M3 extends Comparable<M3>, S extends Comparable<S>>
      SustainedImplication<T,M2,M3,S> sustains(Clock<S> slave, Clock<M2> start, Clock<M3> stop, boolean immediate, boolean weak) {
		return (SustainedImplication<T,M2,M3,S>) this.solver_.addImplicationRelation(
				new SustainedImplication<T,M2,M3,S>(this, start, stop, slave, immediate, weak)
		);
    }

    //@ Helper for creating a strong delayed sustain
	public <M2 extends Comparable<M2>, M3 extends Comparable<M3>, S extends Comparable<S>>
      SustainedImplication<T,M2,M3,S> sustains(Clock<S> slave, Clock<M2> start, Clock<M3> stop) {
		return this.sustains(slave, start, stop, false, false);
    }

	@SuppressWarnings("unchecked")
	public <M extends Comparable<M>, S extends Comparable<S>>
	  TimeDelayedImplication<T, M, S> impliesWithTimeDelay(Clock<S> slave, Clock<M> measuring, M delay) {
	    return (TimeDelayedImplication<T, M, S>) this.solver_.addImplicationRelation(
	    	new TimeDelayedImplication<T, M, S>(this, measuring, slave, delay)
	    );
	}
	
    @SuppressWarnings("unchecked")
    @Deprecated
	public <S extends Comparable<S>> FilteredImplication<T,S> filteredImplies(Clock<S> slave, int skip, int keep, int repSkip, int repKeep) {
		return (FilteredImplication<T, S>) this.solver_.addImplicationRelation(new FilteredImplication<T, S>(this, slave, skip, keep, repSkip, repKeep));
    }

    @Deprecated
    public <U extends Comparable<U>> Implication<T, U> createMasterImplication(Clock<U> slave) {
		return new Implication<T, U>(this, slave);
	}
	
	public <U extends Comparable<U>> Implication<U, T> createSlaveImplication(Clock<U> master) {
		return new Implication<U, T>(master, this);
	}
	
	//@ Make this clock have the same tags as <code>clock</code>.
	@SuppressWarnings("unchecked")
	public SameTags<T> sameTags(Clock<T> clock) {
		return (SameTags<T>) this.solver_.addTagRelation(new SameTags<T>(this, clock));
	}
	
	//@ Create an affine relation between the tags of this clock and the tags as <code>clock</code>.
	@SuppressWarnings("unchecked")
//	public AffineTags<T> affineTags(TagCalculus<T> calc, Clock<T> clock, T coeff, T offset) {
//		return (AffineTags<T>) this.owner_.addTagRelation(new AffineTags<T>(calc, this, clock, coeff, offset));
//	}
	public AffineTags<T> affineTags(Clock<T> clock, T coeff, T offset) {
		return (AffineTags<T>) this.solver_.addTagRelation(new AffineTags<T>(this, clock, coeff, offset));
	}
	
	//@ Set the greedy state of this clock
	public void setGreedy(boolean greedy) {
		this.greedy_ = greedy;
	}
	
	//@ Is this clock greedy?
	public boolean isGreedy() {
		return this.greedy_;
	}
	
	//@ Get the tag calculus used by this clock
	public TagCalculus<T> getTagCalculus() {
		return this.tag_calculus_;
	}
	
	//@ Compute the sum of two tags on this clock
	public T tagSum(T t1, T t2) {
		return this.tag_calculus_.add(t1, t2);
	}

	//@ Compute the difference of two tags on this clock
	public T tagDifference(T t1, T t2) {
		return this.tag_calculus_.subtract(t1, t2);
	}

	//@ Compute the product of two tags on this clock
	public T tagProduct(T t1, T t2) {
		return this.tag_calculus_.multiply(t1, t2);
	}

	//@ Compute the product of two tags on this clock
	public T tagQuotient(T t1, T t2) {
		return this.tag_calculus_.divide(t1, t2);
	}

	//@ Create a tick tagged with <code>value</code> on this clock
	public Tick<T> newTick(T value) {
		Tick<T> t = new Tick<T>(this);
		this.ticks_.add(0, t);
		if (value != null) {
			this.setTagValue(t, value);
		}
		return t;
	}
	
	//@ Create a tick of the right type on this clock
	public Tick<T> newTick() {
		return newTick(null);
	}
	
	/*@ Make a copy of a tick (used for backups).
	 * This amounts to create a tick owned by this clock, 
	 * but without adding it to the list of ticks of the clock.
	 */
	private Tick<T> copyTick(Tick<T> t) {
		return new Tick<T>(this);
	}
	
	//@ Set the tag of a tick on this clock
	public Tick<T> setTagValue(Tick<T> tick, T tag) {
		if (tag == null) {
			setTag(tick, null);
		} else {
			setTag(tick, new Tag<T>(tag));
		}
		return tick;
	}
	
	//@ Set the tag of a tick on this clock
	public Tick<T> setTag(Tick<T> tick, Tag<T> tag) {
		if (tag == null) {
			this.tags_.remove(tick);
		} else {
			this.tags_.put(tick, tag);
		}
		Collections.sort(this.ticks_);
		return tick;
	}
	
	//@ Get the tag of a tick on this clock
	public Tag<T> getTag(Tick<T> tick) {
		return this.tags_.get(tick);
	}

	//@ Get the tag of a tick on this clock
	public T getTagValue(Tick<T> tick) {
		Tag<T> tag = this.tags_.get(tick);
		if (tag == null) {
			return null;
		} else {
			return tag.getValue();
		}
	}

	//@ Set a tick on this clock to now (or not)
	public Tick<T> setNow(Tick<T> tick, boolean now) {
		if (now) {
			this.now_ = tick;
		} else if (tick == this.now_) {
			this.now_ = null;
		}
		return tick;
	}
	
	//@ Is a tick on this clock now ?
	public boolean isNow(Tick<T> tick) {
		return tick == this.now_;
	}
	
	//@ Get the number of ticks on this clock
	public int getNumberOfTicks() {
		return this.ticks_.size();
	}
		
	//@ Remove a tick of this clock
	public void removeTick(Tick<T> tick) {
		this.ticks_.remove(tick);
		this.tags_.remove(tick);
		if (this.now_ == tick) {
			this.now_ = null;
		}
	}
	
	//@ Remove tick at index i of this clock
	public void removeTick(int i) {
		removeTick(getTick(i));
	}
	
	//@ Remove tick if I am the last owner of data. Returns true if the tick was removed.
	public boolean removeOwnedTick(Tick<T> tick, Object dataOwner) {
		tick.clearUserData(dataOwner);
		if (tick.userDataSize() == 0) {
			removeTick(tick);
			return true;
		}
		return false;
	}
	
	//@ Get the tick at index i of this clock
	public Tick<T> getTick(int i) {
		if (this.ticks_.size() <= i) {
			return null;
		}
		return this.ticks_.get(i);
	}
	
	//@ Get a tick with tag <code>tag</code> if any, or null if there is no such tick on this clock.
	public Tick<T> getTick(Tag<T> tag) {
		if (tag == null) {
			return getFloatingTick();
		} else {
			return getTick(tag.getValue());
		}
	}
	
	//@ Get a tick with tag <code>tag</code> if any, or null if there is no such tick on this clock.
	public Tick<T> getTick(T tag) {
		if (tag == null) {
			return getFloatingTick();
		} else {
			for (Tick<T> tick : this.tags_.keySet()) {
				Tag<T> t = this.tags_.get(tick);
				if (t != null) {
					if (t.getValue().equals(tag)) {
						return tick;
					}
				}
			}
			return null;
		}
	}
	
	//@ Tell if the clock has a floating tick (tick with a null tag)
	public boolean hasFloatingTick() {
		for (Tick<T> tick : this.ticks_) {
			if (this.tags_.get(tick) == null) {
				return true;
			}
		}
		return false;
	}
	
	//@ Get the floating tick if any, or null if there is no floating tick on this clock
	public Tick<T> getFloatingTick() {
		for (Tick<T> tick : this.ticks_) {
			if (this.tags_.get(tick) == null) {
				return tick;
			}
		}
		return null;
	}
	
	//@ Tell if this clock has ticks
	public boolean hasTicks() {
		return getNumberOfTicks() > 0;
	}
	
	//@ Tell if this clock has a tick at the current instant
	public boolean hasTickNow() {
		return this.now_ != null;
	}
	
	//@ Tell if this clock has a real tick (not a ghost) at the current instant
	public boolean hasRealTickNow() {
		return ! this.solver_.isGhost(now_);
	}
	
	//@ Get 'now' tick of this clock
	public Tick<T> getNowTick() {
		return this.now_;
	}
		
	//@ Tell if this clock has at least a tagged tick
	public boolean hasTaggedTick() {
		return ! this.tags_.isEmpty();
	}
	
	//@ Get the first tagged tick if any.
	public Tick<T> getFirstTaggedTick() {
		for (Tick<T> tick : this.ticks_) {
			if (this.tags_.get(tick) != null) {
				return tick;
			}
		}
		return null;
	}
	
	//@ Get all the ticks of this clock
	public Collection<Tick<T>> getTicks() {
		return this.ticks_;
	}
	
	//@ Delete all the ticks of this clock
	public void resetClock() {
		this.ticks_.clear();
		this.tags_.clear();
		this.now_ = null;
	}
	
	//@ Remove the now tick on this clock
	public void removeNowTick() {
		if (this.now_ != null) {
			removeTick(this.now_);
		}
	}
	
	//@ Get the name of this clock
    public String getName() {
    	return this.name_;
    }
    
    //@ Clean up : remove duplicate ticks, taking care of not removing "now" ticks
    public void cleanUp() {
    	int ntics = this.getNumberOfTicks();
    	int i = 0;
    	Tag<T> last_tag = null;
    	Tick<T> last_tick = null;
    	boolean last_floating = false;
    	while (i < ntics) {
    		// Look at all ticks, and remove successive ticks that are the same (have same tag or are both floating)
    		boolean remove = false;
    		Tick<T> tick = this.getTick(i);
    		Tag<T> curtag = tick.getTag();
    		if (curtag == null) {
    			if (last_floating) {
    				remove = true;
    			}
    		} else {
    			if (curtag.equals(last_tag)) {
    				remove = true;
    			}
    		}
    		if (remove) {
    			if (tick.isNow()) {
    				// merge user data from both tick into the remaining tick
    				tick.mergeUserData(last_tick);
    				removeTick(last_tick);
    				last_tick = tick;
    				last_tag = curtag;
    				last_floating = (curtag == null);
    			} else {
    				// merge user data from both tick into the remaining tick
    				last_tick.mergeUserData(tick);
    				removeTick(tick);
    			}
    			ntics--;
    		} else {
    			last_tick = tick;
    			last_tag = curtag;
    			last_floating = (curtag == null);
    			i++;
    		}
    	}
    }

	//@ Make a backup of this clock
    public ClockBackupInterface<T> backup() {
    	return new ClockBackup();
    }
    
    //@ Restore from backup
	public void restore(ClockBackupInterface<T> backup) {
		this.resetClock();
		ClockBackup bck = (ClockBackup)backup;
		for (Tick<T> t : bck.ticks_backup_) {
			Tick<T> copy = this.copyTick(t);
			this.ticks_.add(copy);
			if (t == bck.now_backup_) {
				this.now_ = copy;
			}
			Tag<T> tag = bck.tags_backup_.get(t);
			if (tag != null) {
				this.tags_.put(copy, tag.copy());
			}
		}
		this.greedy_ = bck.greedy_backup_;
	}
  
    @Override
	public String toString() {
    	StringBuffer buf = new StringBuffer("Clock \"");
    	buf.append(getName());
    	buf.append("\": ");
    	for (Tick<T> t : getTicks()) {
    		buf.append(t.toString());
    		buf.append(" ");
    	}
    	return buf.toString();
    }
}
