/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.ClockSet;
import fr.supelec.tesl.core.RoundDouble;
import fr.supelec.tesl.core.TagCalculus;


public class TagRelationTest {
	TagCalculus<Double> tcalc = new RoundDouble();
	@Test
	/**
	 * Test no 'now' tick, so chose the smallest values
	 */
	public void test1() {
	
		System.out.println("== Test 1 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));

		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick(3.0);
		c2.newTick(8.0);
		c3.newTick(17.0);
		
		Exception ex = null;
		try {
			solver.solve(c1.getTick(0));
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
	
		assertTrue(ex == null);
		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
		
		assertTrue(c1.getNowTick().getTagValue() == 3.0);
		assertTrue(c2.getNowTick().getTagValue() == 8.0);
		assertTrue(c3.getNowTick().getTagValue() == 17.0);
				
	}
	
	@Test
	/**
	 * Test when one of the clocks has a 'now' tick
	 */
	public void test2() {
	
		System.out.println("== Test 2 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick(3.0);
		c2.newTick(5.0);
		c3.newTick(17.0);
	
		Exception ex = null;
		try {
			solver.solve(c1.getTick(0));
		} catch (Exception e) {
			ex = e;
//			e.printStackTrace();
		}
	
		assertFalse(ex == null);
		assertTrue(c1.hasTickNow());
		
		assertTrue(c1.getNowTick().getTagValue() == 3.0);

	}
	
	@Test
	/**
	 * Test when one of the clocks has a 'now' tick
	 */
	public void test3() {
	
		System.out.println("== Test 3 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick(3.0);
		c2.newTick(7.0);
		c3.newTick(17.0);
	
		Exception ex = null;
		
		try {
			solver.solve(c2.getTick(0));
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
	
		assertTrue(ex == null);
		assertFalse(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		
		assertTrue(c2.getNowTick().getTagValue() == 7.0);
	}
		
	@Test
	/**
	 * Test when two clocks have a 'now' tick, and these ticks correspond to the tag relations
	 */
	public void test4() {
	
		System.out.println("== Test 4 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick(1.5);
		c2.newTick(5.0).setNow(true);
		c3.newTick(17.0);
	
		boolean not_to_many_nows = solver.checkNoNow();
		Exception ex = null;
		
		try {
			solver.solve(c1.getTick(0));
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
	
		assertTrue(ex == null);
		assertFalse(not_to_many_nows);
		
		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
	}
	
	@Test
	/**
	 * Test when two clocks have a 'now' tick, and these ticks correspond to the tag relations
	 */
	public void test5() {
	
		System.out.println("== Test 5 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick(2.0);
		c2.newTick(5.0).setNow(true);
		c3.newTick(17.0);
	
		boolean not_to_many_nows = solver.checkNoNow();
		Exception ex = null;
		try {
			solver.solve(c1.getTick(0));
		} catch (Exception e) {
			ex = e;
//			e.printStackTrace();
		}
	
		assertFalse(ex == null);
		assertFalse(not_to_many_nows);
		
		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
	}
	
	@Test
	/**
	 * Test when two clocks have a 'now' tick, and these ticks correspond to the tag relations
	 */
	public void test6() {
	
		System.out.println("== Test 6 ==");
		
		Throwable  e = null;
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick(3.0);
		c2.newTick(7.0);
		c3.newTick(15.0).setNow(true);
	
		boolean now_ok = solver.checkNoNow();
		try {
			solver.solve(c2.getTick(0));
		} catch (Exception ex) {
			e = ex;
		}
	
		assertFalse(now_ok);
		assertTrue(e == null);
		assertFalse(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
	}

	@Test
	/**
	 * Test when two clocks have a 'now' tick, and these ticks correspond to the tag relations
	 */
	public void test7() {
	
		System.out.println("== Test 7 ==");
		
		Throwable e = null;
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick(3.0);
		c2.newTick(5.0);
		c3.newTick(17.0).setNow(true);
	
		boolean no_now = solver.checkNoNow();
		try {
			solver.solve(c1.getTick(0));
		} catch (Exception ex) {
			e = ex;
//			ex.printStackTrace();
		}
	
		assertFalse(no_now);
		assertFalse(e == null);
		assertTrue(c1.hasTickNow());
		assertFalse(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
	}
	
	@Test
	/**
	 * Test when two clocks have a 'now' tick, and these ticks don't correspond to the tag relations, then we should throw an exception
	 */
	public void test8() {
	
		System.out.println("== Test 8 ==");
		
		Throwable  e = null;
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick(3.0).setNow(true);
		c2.newTick(5.0);
		c3.newTick(17.0);
	
		solver.checkNoNow();
		try {
			solver.solve(c2.getTick(0));
		} catch (Throwable ex) {
			e = ex;
//			ex.printStackTrace();
		}
	
		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		
		assertFalse(e == null);
		
	}
	
	@Test
	/**
	 * Test when two clocks have a 'now' tick, and these ticks don't correspond to the tag relations, then we should throw an exception
	 */
	public void test9() {
	
		System.out.println("== Test 9 ==");
		
		Throwable  e = null;
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick(3.0);
		c2.newTick(5.0);
		c3.newTick(15.0).setNow(true);
	
		solver.checkNoNow();
		try {
			solver.solve(c2.getTick(0));
		} catch (Throwable ex) {
			e = ex;
//			ex.printStackTrace();
		}
	
		assertFalse(c1.hasTickNow());
		assertTrue(c2.hasTickNow());		
		assertTrue(c3.hasTickNow());
		
		assertFalse(e == null);
	}
	
	@Test
	/**
	 * Test when two clocks have a 'now' tick, and these ticks don't correspond to the tag relations, then we should throw an exception
	 */
	public void test10() {
		
		System.out.println("== Test 10 ==");
	
		Throwable  e = null;
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick(3.0);
		c2.newTick(5.0);
		c3.newTick(15.0).setNow(true);
	
		solver.checkNoNow();
		try {
			solver.solve(c1.getTick(0));
		} catch (Throwable ex) {
			e = ex;
		}
	
		assertTrue(c1.hasTickNow());
		assertFalse(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
		
		assertFalse(e == null);
		
	}
	
	@Test
	/**
	 * Test when all clocks have a 'now' tick, and these ticks don't correspond to the tag relations, then we should throw an exception
	 */
	public void test11() {
	
		System.out.println("== Test 11 ==");
		
		Throwable  e = null;
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick(3.0).setNow(true);
		c2.newTick(8.0).setNow(true);
		c3.newTick(15.0).setNow(true);
	
		solver.checkNoNow();
		try {
			solver.solve(c1.getTick(0));
		} catch (Throwable ex) {
			e = ex;
		}
	
		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
		
		assertFalse(e == null);
	}
	
	@Test
	/**
	 * Test null tags and multiple 'now' ticks 
	 */
	public void test12() {
	
		
		System.out.println("== Test 12 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick();
		c2.newTick(8.0).setNow(true);
		c3.newTick(15.0).setNow(true);
	
		boolean now_ok = solver.checkNoNow();

		Exception  e = null;
		try {
			solver.solve(c1.getTick(0));
		} catch (Exception ex) {
			e = ex;
//			ex.printStackTrace();
		}
	
		assertFalse(now_ok);
		assertFalse(e == null);
		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
	}
	
	@Test
	/**
	 * Test null tags
	 */
	public void test13() {
	
		System.out.println("== Test 13 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick();
		c2.newTick(8.0);
		c3.newTick(15.0);
		
		assertFalse(c1.hasTickNow());
		assertFalse(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		
		assertEquals(null, c1.getTick(0).getTag());
				
		try {
			solver.solve(c3.getTick(0));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}	

		assertTrue(c1.hasTickNow());	
		assertFalse(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
		
		assertTrue(c1.getNowTick().getTagValue() == 2.5);
		assertTrue(c3.getNowTick().getTagValue() == 15.0);		
	}
	
	@Test
	/**
	 * Test two null tags
	 */
	public void test14() {
		
		System.out.println("== Test 14 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick();
		c1.newTick();
		c2.newTick(8.0);
		c3.newTick(15.0);
		
		assertFalse(c1.hasTickNow());
		assertFalse(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		
		assertEquals(null, c1.getTick(0).getTag());
		
		Exception e = null;
		try {
			solver.solve(c3.getTick(0));
		} catch (Exception ex) {
			e = ex;
			ex.printStackTrace();
		}	

		assertTrue(e == null);
		
		assertTrue(c1.hasTickNow());	
		assertFalse(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
		
		assertTrue(c1.getNowTick().getTagValue() == 2.5);
		assertTrue(c3.getNowTick().getTagValue() == 15.0);	
	}
	
	@Test
	/**
	 * Test two null tags
	 */
	public void test15() {
		
		System.out.println("== Test 15 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick();
		c2.newTick(8.0);
		c2.newTick(12.0);
		c3.newTick();
		
		assertFalse(c1.hasTickNow());
		assertFalse(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		
		assertEquals(null, c1.getTick(0).getTag());
		assertEquals(null, c3.getTick(0).getTag());
		
		try {
			solver.solve(c2.getTick(0));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}	

		assertTrue(c1.hasTickNow());	
		assertTrue(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
		
		assertTrue(c1.getNowTick().getTag().toString().equals("3.0"));
		assertTrue(c2.getNowTick().getTag().toString().equals("8.0"));
		assertTrue(c3.getNowTick().getTag().toString().equals("17.0"));	
	}
	
	@Test
	/**
	 * Test two null tags in two different clocks, with a 'now' tag on another clock
	 */
	public void test16() {
		
		System.out.println("== Test 16 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick();
		c2.newTick();
		c3.newTick(15.0);
		c3.newTick(17.0);
		
		assertFalse(c1.hasTickNow());
		assertFalse(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		
		assertEquals(null, c1.getTick(0).getTag());
		assertEquals(null, c2.getTick(0).getTag());
		
		try {
			solver.solve(c3.getTick(0));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}	

		assertTrue(c1.hasTickNow());	
		assertTrue(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
		
		assertTrue(c1.getNowTick().getTag().toString().equals("2.5"));
		assertTrue(c2.getNowTick().getTag().toString().equals("7.0"));
		assertTrue(c3.getNowTick().getTag().toString().equals("15.0"));	
	}
	
	@Test
	/**
	 * Test two null tags in two different clocks, with no 'now' tag on other clock
	 * This works only thanks to the alignFloatingTime step in the algorithm.
	 */
	public void test17() {
		
		System.out.println("== Test 17 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick();
		c2.newTick();
		c3.newTick(15.0);
		c3.newTick(11.0);
		
		assertFalse(c1.hasTickNow());
		assertFalse(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		
		assertEquals(null, c1.getTick(0).getTag());
		assertEquals(null, c2.getTick(0).getTag());
		
		try {
			solver.solve(c1.getTick(0));
		} catch (Throwable ex) {
			ex.printStackTrace();
		}

		assertTrue(c1.hasTickNow());	
		assertTrue(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
		
		assertTrue(c1.getNowTick().getTag().toString().equals("1.5"));
		assertTrue(c2.getNowTick().getTag().toString().equals("5.0"));
		assertTrue(c3.getNowTick().getTag().toString().equals("11.0"));	
	}
	
	@Test
	/**
	 * Test all clocks with null tags
	 */
	public void test18() {
		
		System.out.println("== Test 18 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
				
		c1.newTick();
		c2.newTick();
		c3.newTick();
	
		assertFalse(c1.hasTickNow());
		assertFalse(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		
		assertEquals(null, c1.getTick(0).getTag());
		assertEquals(null, c2.getTick(0).getTag());
		assertEquals(null, c3.getTick(0).getTag());
		
		Exception e = null;
		try {
			solver.solve(c1.getTick(c1.getNumberOfTicks()-1));
		} catch (Exception ex) {
			e = ex;
			ex.printStackTrace();
		}	

		assertTrue(e == null);
	}
	
	@Test
	/**
	 * Test all clocks with a null tag and a clock with a 'now' tag
	 */
	public void test19() {
	
		System.out.println("== Test 19 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick();
		c2.newTick();		
		c3.newTick();		
		c3.newTick(11.0);
		
		assertFalse(c1.hasTickNow());
		assertFalse(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		
		assertEquals(null, c1.getTick(0).getTag());
		assertEquals(null, c2.getTick(0).getTag());
		assertEquals(null, c3.getTick(0).getTag());
		
		Exception e = null;
		try {
			solver.solve(c3.getTick(0));
		} catch (Exception ex) {
			e = ex;
			ex.printStackTrace();
		}	

		assertTrue(e == null);
		
		assertTrue(c1.hasTickNow());	
		assertTrue(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
		
		assertTrue(c1.getNowTick().getTag().toString().equals("1.5"));
		assertTrue(c2.getNowTick().getTag().toString().equals("5.0"));
		assertTrue(c3.getNowTick().getTag().toString().equals("11.0"));	
	}
	
	@Test
	/**
	 * Test all clocks with a null tag and a clock with a value tag
	 */
	public void test20() {
	
		System.out.println("== Test 20 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick();
		c2.newTick();		
		c3.newTick();		
		c3.newTick(17.0);
		
		assertFalse(c1.hasTickNow());
		assertFalse(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		
		assertEquals(null, c1.getTick(0).getTag());
		assertEquals(null, c2.getTick(0).getTag());
		assertEquals(null, c3.getTick(0).getTag());
		
		Exception e = null;
		try {
			solver.solve(c1.getTick(0));
		} catch (Exception ex) {
			e = ex;
			ex.printStackTrace();
		}			

		assertTrue(e == null);

		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
		
		assertTrue(c1.getTick(0).getTagValue() == 3.0);
		assertTrue(c2.getTick(0).getTagValue() == 8.0);
		assertTrue(c3.getTick(0).getTagValue() == 17.0);	
		assertTrue(c3.getTick(c3.getNumberOfTicks()-1).getTagValue() == 17.0);	
	}
	
	@Test
	/**
	 * Ascending order before and after solving tag relations
	 * This test is tough !
	 * We have one floating now tick on c1, and ticks with tag 8 on c2 and tag 25 on c3.
	 * We can make either 8 or 25 as "now". This is similar to the merge of ticks, but
	 * it happens across clocks. Therefore, when a tick can be set to now because it
	 * has no constraints from other "now" ticks, we don't make it "now" but we create
	 * a floating "now" tick on its clock. At the next pass, mergeTicks will make the "right"
	 * tick now by merging the floating tick. However, these floating ticks should be removed
	 * when they are not merged because they were introduced only to solve a merge across clocks.
	 * There are stored in the floatingGhosts_ attribute of the solver.
	 */
	public void test21() {
		System.out.println("== Test 21 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
	
		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, 2.0, 1.0);
		
		c1.newTick();
		c2.newTick(8.0);
		c3.newTick(25.0);
	
		assertFalse(c1.hasTickNow());
		assertFalse(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		
		assertEquals(null, c1.getTick(0).getTag());

		Exception e = null;
		try {
			solver.solve(c1.getTick(0));
		} catch (Exception ex) {
			e = ex;
			ex.printStackTrace();
		}	

		assertTrue(e == null);
		
		assertTrue(c1.hasTickNow());	
		assertTrue(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		
		assertTrue(c1.getNowTick().getTagValue() == 3.0);
		assertTrue(c2.getNowTick().getTagValue() == 8.0);
		assertTrue(c3.getTick(0).getTagValue() == 25.0);	
	}
	
}
