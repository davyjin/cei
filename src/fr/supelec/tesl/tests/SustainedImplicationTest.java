/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.ClockSet;
import fr.supelec.tesl.core.RoundDouble;
import fr.supelec.tesl.core.TagCalculus;
import fr.supelec.tesl.core.Unit;
import fr.supelec.tesl.core.UnitCalc;


public class SustainedImplicationTest {

	Clock<Unit> master;
	Clock<Unit> start;
	Clock<Unit> stop;
	Clock<Unit> slave;

	ClockSet solver;

	public SustainedImplicationTest() {
		TagCalculus<Unit> tcalc = new UnitCalc();
		solver = new ClockSet();
		this.master = this.solver.addClock(new Clock<Unit>("master", tcalc, false));
		this.start = this.solver.addClock(new Clock<Unit>("start", tcalc, false));
		this.stop = this.solver.addClock(new Clock<Unit>("stop", tcalc, false));
		this.slave = this.solver.addClock(new Clock<Unit>("slave", tcalc, false));
	}

	@Test
	public void test1() {
		System.out.println("== Test 1 ==");
		// numbers of ticks on master before ticking on start
		final int precount = 1;
		// numbers of ticks on master between start and stop
		final int count = 4;
		// numbers of ticks on master after ticking on stop
		final int postcount = 1;

		// Create a default sustained implication (delayed/strong, i.e. not immediate and not weak)
		this.master.sustains(this.slave, this.start, this.stop);

		// master always ticks, start ticks at i=precount, stop ticks at i=precount+count
		for (int i = 0; i <= precount + count + postcount; i++) {
			solver.getLogger().log("info", "Phase ", i);

			this.master.newTick().setNow(true);

			if (i == precount) {
				this.start.newTick().setNow(true);
			}
			if (i == precount+count) {
				this.stop.newTick().setNow(true);
			}
			Exception ex = null;
			try {
				this.solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);
			if (i <= precount) { // <= because delayed
				assertFalse(this.slave.hasTickNow());
			} else if (i <= (precount+count)) { // <= because strong sustain
				assertTrue(this.slave.hasTickNow());
			} else {
				assertFalse(this.slave.hasTickNow());
			}
			this.solver.resetClocks();
		}
	}

	@Test
	public void test2() {
		System.out.println("== Test 2 ==");
		// numbers of ticks on master before ticking on start
		final int precount = 1;
		// numbers of ticks on master between start and stop
		final int count = 4;
		// numbers of ticks on master after ticking on stop
		final int postcount = 1;

		// Create a weak immediate sustained implication
		this.master.sustains(this.slave, this.start, this.stop, true, true);

		// master always ticks, start ticks at i=precount, stop ticks at i=precount+count
		for (int i = 0; i <= precount + count + postcount; i++) {
			solver.getLogger().log("info", "Phase ", i);

			this.master.newTick().setNow(true);

			if (i == precount) {
				this.start.newTick().setNow(true);
			}
			if (i == precount+count) {
				this.stop.newTick().setNow(true);
			}
			Exception ex = null;
			try {
				this.solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);
			if (i < precount) { // < because immediate sustain
				assertFalse(this.slave.hasTickNow());
			} else if (i < (precount+count)) { // < because weak sustain
				assertTrue(this.slave.hasTickNow());
			} else {
				assertFalse(this.slave.hasTickNow());
			}
			this.solver.resetClocks();
		}
	}

	@Test
	public void test3() {
		System.out.println("== Test 3 ==");
		// numbers of ticks on master before ticking on start
		final int precount = 1;
		// numbers of ticks on master between start and stop
		final int count = 4;
		// numbers of ticks on master after ticking on stop
		final int postcount = 1;

		// Create a strong immediate sustained implication
		this.master.sustains(this.slave, this.start, this.stop, true, false);

		// master always ticks, start ticks at i=precount, stop ticks at i=precount+count
		for (int i = 0; i <= precount + count + postcount; i++) {
			solver.getLogger().log("info", "Phase ", i);

			this.master.newTick().setNow(true);

			if (i == precount) {
				this.start.newTick().setNow(true);
			}
			if (i == precount+count) {
				this.stop.newTick().setNow(true);
			}
			Exception ex = null;
			try {
				this.solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);
			if (i < precount) { // < because immediate
				assertFalse(this.slave.hasTickNow());
			} else if (i <= (precount+count)) { // <= because strong sustain
				assertTrue(this.slave.hasTickNow());
			} else {
				assertFalse(this.slave.hasTickNow());
			}
			this.solver.resetClocks();
		}
	}

	@Test
	public void test4() {
		System.out.println("== Test 4 ==");
		// numbers of ticks on master before ticking on start
		final int precount = 1;
		// numbers of ticks on master between start and stop
		final int count = 4;
		// numbers of ticks on master after ticking on stop
		final int postcount = 1;

		// Create a delayed (not immediate) weak sustained implication
		this.master.sustains(this.slave, this.start, this.stop, false, true);

		// master always ticks, start ticks at i=precount, stop ticks at i=precount+count
		for (int i = 0; i <= precount + count + postcount; i++) {
			solver.getLogger().log("info", "Phase ", i);

			this.master.newTick().setNow(true);

			if (i == precount) {
				this.start.newTick().setNow(true);
			}
			if (i == precount+count) {
				this.stop.newTick().setNow(true);
			}
			Exception ex = null;
			try {
				this.solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);
			if (i <= precount) { // <= because not immediate
				assertFalse(this.slave.hasTickNow());
			} else if (i < (precount+count)) { // < because weak sustain
				assertTrue(this.slave.hasTickNow());
			} else {
				assertFalse(this.slave.hasTickNow());
			}
			this.solver.resetClocks();
		}
	}

	@Test
	public void test5() {
		System.out.println("== Test 5 ==");
		final int precount = 1;
		final int count = 0;
		final int postcount = 1;

		// delayed strong sustain
		this.master.sustains(this.slave, this.start, this.stop);

		// Test 0 delay : stop comes with start => no tick on slave
		for (int i = 0; i <= precount + count + postcount; i++) {
			solver.getLogger().log("info", "Phase ", i);

			this.master.newTick().setNow(true);

			if (i == precount) {
				this.start.newTick().setNow(true);
			}
			if (i == precount+count) {
				this.stop.newTick().setNow(true);
			}
			Exception ex = null;
			try {
				this.solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);
			
			assertFalse(this.slave.hasTickNow());

			this.solver.resetClocks();
		}
	}

	@Test
	public void test6() {
		System.out.println("== Test 6 ==");
		final int precount = 1;
		final int count = 0;
		final int postcount = 1;

		// Immediate and weak sustain
		this.master.sustains(this.slave, this.start, this.stop, true, true);

		// Test 0 delay with immediate and strong : stop comes with start => no tick on slave
		for (int i = 0; i <= precount + count + postcount; i++) {
			solver.getLogger().log("info", "Phase ", i);

			this.master.newTick().setNow(true);

			if (i == precount) {
				this.start.newTick().setNow(true);
			}
			if (i == precount+count) {
				this.stop.newTick().setNow(true);
			}
			Exception ex = null;
			try {
				this.solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);
			
			assertFalse(this.slave.hasTickNow());

			this.solver.resetClocks();
		}
	}

	@Test
	public void test7() {
		System.out.println("== Test 7 ==");
		final int precount = 1;
		final int count = 0;
		final int postcount = 1;

		// Immediate and strong sustain
		this.master.sustains(this.slave, this.start, this.stop, true, false);

		// Test 0 delay with immediate and weak : stop comes with start => tick on slave (because immediate start and weak stop)
		for (int i = 0; i <= precount + count + postcount; i++) {
			solver.getLogger().log("info", "Phase ", i);

			this.master.newTick().setNow(true);

			if (i == precount) {
				this.start.newTick().setNow(true);
			}
			if (i == precount+count) {
				this.stop.newTick().setNow(true);
			}
			Exception ex = null;
			try {
				this.solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	

			assertTrue(ex == null);

			if (i == precount) {
				assertTrue(this.slave.hasTickNow());
			} else {
				assertFalse(this.slave.hasTickNow());
			}

			this.solver.resetClocks();
		}
	}

	@Test
	public void test8() {
		System.out.println("== Test 8 ==");
		final int precount = 1;
		final int count = 0;
		final int postcount = 1;

		// Delayed strong sustain
		this.master.sustains(this.slave, this.start, this.stop, false, false);

		// Test 0 delay with not immediate and strong sustain : stop comes with start => no tick on slave (because not immediate start)
		for (int i = 0; i <= precount + count + postcount; i++) {
			solver.getLogger().log("info", "Phase ", i);

			this.master.newTick().setNow(true);

			if (i == precount) {
				this.start.newTick().setNow(true);
			}
			if (i == precount+count) {
				this.stop.newTick().setNow(true);
			}
			Exception ex = null;
			try {
				this.solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			assertFalse(this.slave.hasTickNow());

			this.solver.resetClocks();
		}
	}
	
	@Test
	public void test9() {
		System.out.println("== Test 9 ==");
		// Test greedy behavior, no tick is set to now
		ClockSet cs = new ClockSet();
		Clock<Double> m = cs.addClock(new Clock<Double>("master", new RoundDouble()));
		Clock<Double> b = cs.addClock(new Clock<Double>("begin", new RoundDouble()));
		Clock<Double> e = cs.addClock(new Clock<Double>("end", new RoundDouble()));
		Clock<Double> s = cs.addClock(new Clock<Double>("slave", new RoundDouble()));
		// Same time scale on all clocks
		m.sameTags(b);
		m.sameTags(e);
		m.sameTags(s);
		
		// Delayed strong sustain
		m.sustains(s, b, e, false, false);
		
		m.newTick(0.0);
		m.newTick(1.0);
		b.newTick(1.0); // not tick on slave because sustain is delayed
		m.newTick(2.0); // tick on slave
		m.newTick(3.0);
		e.newTick(3.0); // tick on slave because strong sustain
		b.newTick(3.5); // no tick on slave because no tick on master
		m.newTick(4.0); // tick on slave
		m.newTick(5.0); // tick on slave
		e.newTick(5.5); // no tick on slave because no tick on master
		m.newTick(6.0); // no tick on slave because after stop
		
		// Sequence of expected time tags (on m, but all clocks have the same time scale here
		double tag[] =  {0.0,   1.0,   2.0,  3.0,  3.5,   4.0,  5.0,  5.5,   6.0};
		// Sequence of expected presences of a now tick on the slave clock
		boolean now[] = {false, false, true, true, false, true, true, false, false};
		
//		cs.getLogger().setLoggingAllKinds(true);
		
		int i = 0;
		while (m.hasTicks()) {
			try {
				cs.solve();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			// Check that time progresses as expected.
			// getCurrentTag() returns the time tag computed from tag relations for a clock,
			// even if the clock has no tick at this time
			assertTrue(m.getCurrentTag().getValue() == tag[i]);
			// Check that the slave has ticks when expected
			assertTrue(s.hasTickNow() == now[i]);
			cs.removeNowTicks();
			i++;
		}
		
	}

}