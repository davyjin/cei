/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

// TODO Check if this example is still meaningful!

package fr.supelec.tesl.tests;

import fr.supelec.tesl.core.AffineTags;
import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.ClockSet;
import fr.supelec.tesl.core.Implication;
import fr.supelec.tesl.core.RoundDouble;
import fr.supelec.tesl.core.TagCalculus;
import fr.supelec.tesl.core.TagRelation;

public class Example {
	public static void main(String[] args) {
		TagCalculus<Double> tcalc = new RoundDouble();
		Clock<Double> c1 = new Clock<Double>("c1", tcalc);
		Clock<Double> c2 = new Clock<Double>("c2", tcalc);
		Clock<Double> c3 = new Clock<Double>("c3", tcalc);

		TagRelation<Double, Double> t = new AffineTags<Double>(c1, c2, 2.0, 2.0);
		TagRelation<Double, Double> tt = new AffineTags<Double>(c2, c3, 2.0, 1.0);

		ClockSet solver = new ClockSet();
		
		solver.addClock(c1);
		solver.addClock(c2);
		solver.addClock(c3);

		solver.addTagRelation(t);
		solver.addTagRelation(tt);

		// Create a floating tick on c1
		c1.newTick();
		
		// Create a tick on c1, with tag 3
		c1.newTick().setTagValue(3.0);

		
		// Create a tick on c2, with tag 5
		// This means the next tick on c2 should have tag 5
		c2.newTick().setTagValue(12.0);

		c2.newTick().setTagValue(7.0);
		
		c2.newTick().setTagValue(8.0);
		
		c3.newTick().setTagValue(11.0);
		
		c3.newTick().setTagValue(17.0);

	
		// Say that c1 is a subclock of c2 => c1 master and c2 slave
		 Implication<Double, Double> r = new Implication<Double, Double>(c1, c2);
		 solver.addImplicationRelation(r);
		
		// Since c1 is a subclock of c2, there must be a tick on c2 "now", and
		// its tag is 2 * 8 + 2 = 18
		
		System.out.println("Solve tag relations after adding clock relations!");
		ClockSet result = null;
		
		if(solver.checkNoNow()){
			System.out.println("It's OK, there is no 'now' tick!");
		} else {
			System.out.println("\nERROR: There should be no 'now' ticks!");
			return; 
		}
		
		try {
			result = solver.solve(c1.getTick(0));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		if((result == null) || (!result.getClocks().iterator().hasNext())) { // if the result has no clock
			System.out.println("\nThis set of clocks doesn't have a solution!\n");
			return; 
		}
		System.out.println("\nFINALE CLOCKS: ");
		for (Clock<?> c : result.getClocks()) {
			System.out.println(c.toString());
		}
		System.out.println();
		
		System.out.println("\nCLOCKSET - FINALE CLOCKS: ");
		for (Clock<?> c : solver.getClocks()) {
			System.out.println(c.toString());
		}
		System.out.println();
		
	}
}
