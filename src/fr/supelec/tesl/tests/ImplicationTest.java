/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.ClockSet;
import fr.supelec.tesl.core.RoundDouble;
import fr.supelec.tesl.core.TagCalculus;

public class ImplicationTest {
	TagCalculus<Double> tcalc;
	Clock<Double> c3;
	
	public ImplicationTest() {
		tcalc = new RoundDouble();
		c3 = new Clock<Double>("c3", this.tcalc);
	}
	
	@Test
	/**
	 * Test the add of ticks on the Implication or slave clock (c1)
	 */
	public void test1() {
		System.out.println("== Test 1 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));

		c1.implies(c2);
		
		c1.newTick(3.0);
		c2.newTick(5.0);
		c2.newTick(15.0);
		
		solver.getLogger().setLoggingAllKinds(false);
		
		Exception ex = null;
		try {
			solver.solve(c2.getTick(c2.getNumberOfTicks() - 1));  // last tick
		} catch (Exception e) {
			ex = e;
//			e.printStackTrace();
		}	
		
		assertTrue(ex != null);  // c2 has a tick in the past with tag 5.0
		assertTrue(c1.hasTickNow());
		assertTrue(c1.getNowTick().getTagValue() == 3.0);
		assertTrue(c2.hasTickNow());
		assertTrue(c2.getNowTick().getTagValue() == 15.0);
	}
	
	@Test
	/**
	 * Test the add of ticks on the Implication or slave clock (c1)
	 * No 'now' ticks => should chose the smallest values
	 */
	public void test2() {
		System.out.println("== Test 2 ==");
	
		ClockSet solver = new ClockSet();

		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		
		c1.newTick(3.0);
		c1.newTick(2.5);
		c2.newTick(5.0);

		c2.implies(c1);
		
		c2.newTick(15.0);
		 
		solver.getLogger().setLoggingAllKinds(false);

		Exception ex = null;
		try {
			solver.solve(c2.getTick(0));
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}	
		
		assertTrue(ex == null);			

		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());

		assertTrue(c1.getNowTick().getTag().toString().equals("2.5"));
		assertTrue(c2.getNowTick().getTag().toString().equals("5.0"));			
	}
	
	
	@Test
	/**
	 * Test the add of ticks on the Implication or slave clock (c1)
	 * Multiple 'now' ticks on the master clock => take the one with the smallest tag value
	 */
	public void test3() {
		System.out.println("== Test 3 ==");

		ClockSet solver = new ClockSet();

		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		
		c1.newTick(3.0);
		c1.newTick(2.5);
		
		c2.implies(c1);
		
		c2.newTick(7.0);
		c2.newTick(8.0);
		c2.newTick(12.0);
		
		solver.getLogger().setLoggingAllKinds(false);

		Exception ex = null;
		try {
			solver.solve(c2.getTick(0));
		} catch (Exception e) {
			ex = e;
			ex.printStackTrace();
		}	
		
		assertTrue(ex == null);

		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
	}
	
	@Test
	/**
	 * Test the add of ticks on the Implication or slave clock (c1)
	 * Both master and slave clock have 'now' tick => align their value
	 */
	public void test4() {
		System.out.println("== Test 4 ==");
	
		ClockSet solver = new ClockSet();

		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		
		c1.newTick(3.0);
		c1.newTick(2.5);
		
		c2.implies(c1);
		
		c2.newTick(7.0);
		c2.newTick(8.0);
		c2.newTick(12.0);
		
		solver.getLogger().setLoggingAllKinds(false);

		Exception ex = null;
		try {
			solver.solve(c1.getTick(0));
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}	
		
		assertTrue(ex == null);

		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());

		assertTrue(c1.getNowTick().getTagValue() == 2.5);
		assertTrue(c2.getNowTick().getTagValue() == 7.0);
	}
	
	@Test
	/**
	 * Test the add of ticks on the Implication or slave clock (c1)
	 * when we already have tag relations
	 * 'now' tick is on the slave clock
	 */
	public void test5() {
	
		System.out.println("== Test 5 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));

		c1.affineTags(c2, 2.0, 2.0);
		
		c1.newTick(3.0);
		c2.newTick(15.0);
		c2.newTick(12.0);
		
		c1.implies(c2);
				
		solver.getLogger().setLoggingAllKinds(false);

		Exception ex = null;
		try {
			solver.solve(c1.getTick(0));
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}	
		
		assertTrue(ex == null);				

		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		
		assertTrue(c1.getNowTick().getTag().toString().equals("3.0"));
		assertTrue(c2.getNowTick().getTag().toString().equals("8.0"));				
	}
	
	@Test
	/**
	 * Test the add of ticks on the Implication or slave clock (c2)
	 * when we already have tag relations
	 * no 'now' tick
	 */
	public void test6() {
	
		System.out.println("== Test 6 ==");
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));

		c1.affineTags(c2, 2.0, 2.0);
		
		c1.newTick(3.0);
		c2.newTick(7.0);
	
		c1.implies(c2);
		
		c1.newTick(5.0);
		
		solver.getLogger().setLoggingAllKinds(false);
//		solver.getLogger().setLoggingKind("error", true);

		Exception ex = null;
		try {
			solver.solve(c1.getTick(0));
		} catch (Exception e) {
			ex = e;
//			e.printStackTrace();
		}
	
		assertTrue(ex != null); // c2 has a tick in the past with tag 7.0 (now is at 8.0)				

		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		
		assertTrue(c1.getNowTick().getTag().toString().equals("3.0"));
		assertTrue(c2.getNowTick().getTag().toString().equals("8.0"));				
	}
	
	@Test
	/**
	 * Test the add of ticks on the Implication or slave clock (c2)
	 * when we already have tag relations
	 *  'now' tick is on the master clock
	 */
	public void test7() {
	
		System.out.println("== Test 7 ==");
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));

		c1.affineTags(c2, 2.0, 2.0);
		
		c1.newTick(8.0);
		c2.newTick(7.0);
	
		c1.implies(c2);
		
		c1.newTick(5.0);
		
		solver.getLogger().setLoggingAllKinds(false);
//		solver.getLogger().setLoggingKind("error", true);

		Exception ex = null;
		try {
			solver.solve(c1.getTick(0));
		} catch (Exception e) {
			ex = e;
//			e.printStackTrace();
		}
	
		assertTrue(ex != null); // c2 has a tick in the past with tag 7.0 (now is at 12.0)				

		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		
		assertTrue(c1.getNowTick().getTag().toString().equals("5.0"));
		assertTrue(c2.getNowTick().getTag().toString().equals("12.0"));				
	}
	
	@Test
	/**
	 * Test the add of ticks on the Implication or slave clock (c2)
	 * when we already have tag relations
	 * slave clock has a null tick
	 */
	public void test8() {
	
		System.out.println("== Test 8 ==");
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));

		c1.affineTags(c2, 2.0, 2.0);

		c2.implies(c1);
		
		c1.newTick(3.0);
		c2.newTick();
		c2.newTick(12.0);
		
		solver.getLogger().setLoggingAllKinds(false);
//		solver.getLogger().setLoggingKind("error", true);

		Exception ex = null;
		try {
			solver.solve(c2.getTick(c2.getNumberOfTicks()-1));
		} catch (Exception e) {
			ex = e;
//			e.printStackTrace();
		}
	
		assertTrue(ex != null);  // c1 has a tick in the past with tag 3.0 (now is at 5.0)

		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		
		assertTrue(c1.getNowTick().getTag().toString().equals("5.0"));
		assertTrue(c2.getNowTick().getTag().toString().equals("12.0"));				
	}
	
	@Test
	/**
	 * Test the add of ticks on the Implication or slave clock (c2)
	 * when we already have tag relations
	 * master clock has a null tick
	 */
	public void test9() {
	
		System.out.println("== Test 9 ==");
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));

		c1.affineTags(c2, 2.0, 2.0);
		
		c1.implies(c2);
		
		c1.newTick(3.0);
		c1.newTick();
		c2.newTick(12.0);
		
		solver.getLogger().setLoggingAllKinds(false);

		Exception ex = null;
		try {
			solver.solve(c1.getTick(0));
		} catch (Exception e) {
			ex = e;
			e.printStackTrace();
		}
	
		assertTrue(ex == null);				

		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		
		assertTrue(c1.getNowTick().getTag().toString().equals("3.0"));
		assertTrue(c2.getNowTick().getTag().toString().equals("8.0"));				
	}
}
