/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.ClockSet;
import fr.supelec.tesl.core.TagCalculus;
import fr.supelec.tesl.core.Unit;
import fr.supelec.tesl.core.UnitCalc;


@SuppressWarnings("unqualified-field-access")
public class FilteredImplicationTest {
	Clock<Unit> master;
	Clock<Unit> slave;

	ClockSet solver;

	public FilteredImplicationTest() {
		solver = new ClockSet();
		TagCalculus<Unit> tcalc = new UnitCalc();
		this.master = this.solver.addClock(new Clock<Unit>("master", tcalc, false));
		this.slave = this.solver.addClock(new Clock<Unit>("slave", tcalc, false));		
		solver.getLogger().setLoggingAllKinds(false);
	}

	@Test
	public void test1() {
		// Basic test without repeats
		System.out.println("== Test 1 ==");
		int skipcount = 2;
		int count = 3;
		int repskip = 0;
		int repcount = 0;
		int repeats = 2;

		this.master.impliesThroughFilter(this.slave, skipcount, count, repskip, repcount);

		int phase = 0;
		for (int i = 0; i < skipcount; i++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			assertFalse(slave.hasTickNow());

			phase++;
			this.solver.resetClocks();
		}
		
		for (int i = 0; i < count; i++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			assertTrue(slave.hasTickNow());

			phase++;
			this.solver.resetClocks();
		}
		
		for (int k = 0; k < repeats; k++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			if ((repskip + repcount) == 0) {
				assertFalse(slave.hasTickNow());
			} else {
				if (k%(repskip+repcount) < repskip) {
					assertFalse(slave.hasTickNow());
				} else if (k%(repskip+repcount) < repcount) {
					assertTrue(slave.hasTickNow());
				}
			}
			phase++;
			this.solver.resetClocks();
		}
	}
	
	@Test
	public void test2() {
		// Basic test without repeats but null skip
		System.out.println("== Test 2 ==");
		int skipcount = 0;
		int count = 3;
		int repskip = 0;
		int repcount = 0;
		int repeats = 2;

		this.master.impliesThroughFilter(this.slave, skipcount, count, repskip, repcount);

		int phase = 0;
		for (int i = 0; i < skipcount; i++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			assertFalse(slave.hasTickNow());

			phase++;
			this.solver.resetClocks();
		}
		
		for (int i = 0; i < count; i++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			assertTrue(slave.hasTickNow());

			phase++;
			this.solver.resetClocks();
		}
		
		for (int k = 0; k < repeats; k++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			if ((repskip + repcount) == 0) {
				assertFalse(slave.hasTickNow());
			} else {
				if (k%(repskip+repcount) < repskip) {
					assertFalse(slave.hasTickNow());
				} else if (k%(repskip+repcount) < repcount) {
					assertTrue(slave.hasTickNow());
				}
			}
			phase++;
			this.solver.resetClocks();
		}
	}
	
	@Test
	public void test3() {
		// Basic test without repeats but null skip and count
		System.out.println("== Test 3 ==");
		int skipcount = 0;
		int count = 0;
		int repskip = 0;
		int repcount = 0;
		int repeats = 2;

		this.master.impliesThroughFilter(this.slave, skipcount, count, repskip, repcount);

		int phase = 0;
		for (int i = 0; i < skipcount; i++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			assertFalse(slave.hasTickNow());

			phase++;
			this.solver.resetClocks();
		}
		
		for (int i = 0; i < count; i++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			assertTrue(slave.hasTickNow());

			phase++;
			this.solver.resetClocks();
		}
		
		for (int k = 0; k < repeats; k++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			if ((repskip + repcount) == 0) {
				assertFalse(slave.hasTickNow());
			} else {
				if (k%(repskip+repcount) < repskip) {
					assertFalse(slave.hasTickNow());
				} else if (k%(repskip+repcount) < repcount) {
					assertTrue(slave.hasTickNow());
				}
			}
			phase++;
			this.solver.resetClocks();
		}
	}
	
	@Test
	public void test4() {
		// Test with repeats
		System.out.println("== Test 4 ==");
		int skipcount = 1;
		int count = 2;
		int repskip = 2;
		int repcount = 3;
		int repeats = 10;

		this.master.impliesThroughFilter(this.slave, skipcount, count, repskip, repcount);

		int phase = 0;
		for (int i = 0; i < skipcount; i++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			assertFalse(slave.hasTickNow());

			phase++;
			this.solver.resetClocks();
		}
		
		for (int i = 0; i < count; i++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			assertTrue(slave.hasTickNow());

			phase++;
			this.solver.resetClocks();
		}
		
		for (int k = 0; k < repeats; k++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			if ((repskip + repcount) == 0) {
				assertFalse(slave.hasTickNow());
			} else {
				if (k%(repskip+repcount) < repskip) {
					assertFalse(slave.hasTickNow());
				} else {
					assertTrue(slave.hasTickNow());
				}
			}
			phase++;
			this.solver.resetClocks();
		}
	}
	
	@Test
	public void test5() {
		// Test with repeats and null repeat skip
		System.out.println("== Test 5 ==");
		int skipcount = 1;
		int count = 2;
		int repskip = 0;
		int repcount = 3;
		int repeats = 10;

		this.master.impliesThroughFilter(this.slave, skipcount, count, repskip, repcount);

		int phase = 0;
		for (int i = 0; i < skipcount; i++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			assertFalse(slave.hasTickNow());

			phase++;
			this.solver.resetClocks();
		}
		
		for (int i = 0; i < count; i++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			assertTrue(slave.hasTickNow());

			phase++;
			this.solver.resetClocks();
		}
		
		for (int k = 0; k < repeats; k++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			if ((repskip + repcount) == 0) {
				assertFalse(slave.hasTickNow());
			} else {
				if (k%(repskip+repcount) < repskip) {
					assertFalse(slave.hasTickNow());
				} else {
					assertTrue(slave.hasTickNow());
				}
			}
			phase++;
			this.solver.resetClocks();
		}
	}
	
	@Test
	public void test6() {
		// Test with repeats and null repeat count
		System.out.println("== Test 6 ==");
		int skipcount = 1;
		int count = 2;
		int repskip = 2;
		int repcount = 0;
		int repeats = 10;

		this.master.impliesThroughFilter(this.slave, skipcount, count, repskip, repcount);

		int phase = 0;
		for (int i = 0; i < skipcount; i++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			assertFalse(slave.hasTickNow());

			phase++;
			this.solver.resetClocks();
		}
		
		for (int i = 0; i < count; i++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			assertTrue(slave.hasTickNow());

			phase++;
			this.solver.resetClocks();
		}
		
		for (int k = 0; k < repeats; k++) {
			solver.getLogger().log("info", "Phase ", phase);

			master.newTick().setNow(true);

			Exception ex = null;
			try {
				solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);

			if ((repskip + repcount) == 0) {
				assertFalse(slave.hasTickNow());
			} else {
				if (k%(repskip+repcount) < repskip) {
					assertFalse(slave.hasTickNow());
				} else {
					assertTrue(slave.hasTickNow());
				}
			}
			phase++;
			this.solver.resetClocks();
		}
	}
}