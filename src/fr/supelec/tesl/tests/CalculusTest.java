/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.tests;

import fr.supelec.tesl.core.RoundDouble;

/*
 * This example shows why raw double arithmetic operators should not
 * be used when computing clock tags since tags that should be equals
 * may not match because of rounding errors.
 * Using the RoundDouble tag calculus, there are no such discrepancies.
 */
public class CalculusTest {
	public static void main(String args[]) {
		double t1 = 1.8;
		double t2 = t1 + 0.1;
		double t3 = 2.0 * t2 + 1.0;
		double t4 = (t3 - 1.0) / 2.0;
		System.out.println("Computation with doubles: " + t1 + " + " + 0.1 + " = " + t2);
		System.out.println("x2 + 1, then -1 /2 => " + t4);
		
		t2 = RoundDouble.do_add(t1, 0.1);
		t3 = RoundDouble.do_add(RoundDouble.do_multiply(2.0, t2), 1.0);
		t4 = RoundDouble.do_divide(RoundDouble.do_subtract(t3, 1.0), 2.0);
		System.out.println("Computation with RoundDouble: " + t1 + " + " + 0.1 + " = " + t2);
		System.out.println("x2 + 1, then -1 /2 => " + t4);
	}
}
