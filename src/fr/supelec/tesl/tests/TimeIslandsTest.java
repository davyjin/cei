/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.tests;

import static org.junit.Assert.*;
import org.junit.Test;

import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.ClockSet;
import fr.supelec.tesl.core.RoundDouble;
import fr.supelec.tesl.core.TagCalculus;


public class TimeIslandsTest {
	TagCalculus<Double> tcalc = new RoundDouble();

	@Test
	public void test1() {
		// Two time islands: (c1 c2) and (c3 c4).
		// Start the solver with a "now" tick in each island
		System.out.println("== Test 1 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
		Clock<Double> c4 = solver.addClock(new Clock<Double>("c4", this.tcalc));

		c1.affineTags(c2, 2.0, 2.0);
		c3.affineTags(c4, 2.0, 1.0);
		
		c1.newTick(3.0).setNow(true);
		c2.newTick(8.0);
		c3.newTick(5.0).setNow(true);
		c4.newTick(11.0);
			
//		solver.getLogger().setLoggingKinds(true, "error", "info", "fine");

		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
		assertTrue(c4.hasTickNow());
		
		assertTrue(c1.getNowTick().getTagValue() == 3.0);
		assertTrue(c2.getNowTick().getTagValue() == 8.0);
		assertTrue(c3.getNowTick().getTagValue() == 5.0);
		assertTrue(c4.getNowTick().getTagValue() == 11.0);
				
	}
	
	@Test
	public void test2() {
		// Two time islands: (c1 c2) and (c3 c4).
		// Start the solver with no "now" tick in each island
		// If the clocks are greedy, we should get the same result as in test 1
		System.out.println("== Test 2 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
		Clock<Double> c4 = solver.addClock(new Clock<Double>("c4", this.tcalc));

		c1.affineTags(c2, 2.0, 2.0);
		c3.affineTags(c4, 2.0, 1.0);
		
		c1.newTick(3.0);
		c2.newTick(8.0);
		c3.newTick(5.0);
		c4.newTick(11.0);
		
//		c1.getTick(0).setNow(true);
//		c3.getTick(0).setNow(true);
	
//		solver.getLogger().setLoggingKinds(true, "error", "info", "fine");
		
		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		assertTrue(c3.hasTickNow());
		assertTrue(c4.hasTickNow());
		
		assertTrue(c1.getNowTick().getTagValue() == 3.0);
		assertTrue(c2.getNowTick().getTagValue() == 8.0);
		assertTrue(c3.getNowTick().getTagValue() == 5.0);
		assertTrue(c4.getNowTick().getTagValue() == 11.0);
				
	}
	
	@Test
	public void test3() {
		// Two time islands: (c1 c2) and (c3 c4).
		// Start the solver with no "now" tick in each island
		// There is a mismatch between the tags of the ticks so there should be only two now ticks
		System.out.println("== Test 3 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
		Clock<Double> c4 = solver.addClock(new Clock<Double>("c4", this.tcalc));

		c1.affineTags(c2, 2.0, 2.0);
		c3.affineTags(c4, 2.0, 1.0);
		
		c2.newTick(6.0);
		c1.newTick(3.0);
		c3.newTick(5.0);
		c4.newTick(9.0);
		
//		c1.getTick(0).setNow(true);
//		c3.getTick(0).setNow(true);
	
//		solver.getLogger().setLoggingKinds(true, "error", "info", "fine");
		
		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		assertFalse(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		assertTrue(c4.hasTickNow());
		
		assertTrue(c2.getNowTick().getTagValue() == 6.0);
		assertTrue(c4.getNowTick().getTagValue() == 9.0);
				
	}
	
	@Test
	public void test4() {
		// Inconsistent tag relations
		System.out.println("== Test 4 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));

		c1.affineTags(c2, 2.0, 2.0);
		c2.affineTags(c3, -1.0, 3.0);
		
		c1.newTick(2.0);
		c2.newTick(6.0);
		c3.newTick(0.0);
	
//		solver.getLogger().setLoggingKinds(true, "error", "info", "fine");
		
		Exception ex = null;
		try {
			solver.solve();
		} catch (Exception e) {
			ex = e;
//			e.printStackTrace();
		}
	
		assertFalse(ex == null);
	}
	
	@Test
	public void test5() {
		// Two time islands: (c1 c2) and (c3 c4).
		// Start the solver with no "now" tick in each island
		// c3 and c4 are not greedy, so they should have no tick
		System.out.println("== Test 5 ==");
		
		ClockSet solver = new ClockSet();
		Clock<Double> c1 = solver.addClock(new Clock<Double>("c1", this.tcalc));
		Clock<Double> c2 = solver.addClock(new Clock<Double>("c2", this.tcalc));
		Clock<Double> c3 = solver.addClock(new Clock<Double>("c3", this.tcalc));
		Clock<Double> c4 = solver.addClock(new Clock<Double>("c4", this.tcalc));

		c1.affineTags(c2, 2.0, 2.0);
		c3.affineTags(c4, 2.0, 1.0);

		c3.setGreedy(false);
		c4.setGreedy(false);
		
		
		c1.newTick(3.0);
		c2.newTick(8.0);
		c3.newTick(5.0);
		c4.newTick(11.0);
		
//		c1.getTick(0).setNow(true);
//		c3.getTick(0).setNow(true);
	
//		solver.getLogger().setLoggingKinds(true, "error", "info", "fine");
		
		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		assertTrue(c1.hasTickNow());
		assertTrue(c2.hasTickNow());
		assertFalse(c3.hasTickNow());
		assertFalse(c4.hasTickNow());
		
		assertTrue(c1.getNowTick().getTagValue() == 3.0);
		assertTrue(c2.getNowTick().getTagValue() == 8.0);
				
	}
	
}
