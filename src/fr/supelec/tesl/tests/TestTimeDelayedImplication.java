package fr.supelec.tesl.tests;

import org.junit.Test;
import static org.junit.Assert.*;

import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.ClockSet;
import fr.supelec.tesl.core.RoundDouble;
import fr.supelec.tesl.core.TagCalculus;
import fr.supelec.tesl.core.Unit;
import fr.supelec.tesl.core.UnitCalc;

public class TestTimeDelayedImplication {
	TagCalculus<Double> tcalc = new RoundDouble();
	
	@Test
	public void test1() {
		// Test basic time delayed implication
		ClockSet solver = new ClockSet();
		Clock<Double> measuring = solver.addClock(new Clock<Double>("measuring", this.tcalc));
		Clock<Unit> master = solver.addClock(new Clock<Unit>("master", new UnitCalc()));
		Clock<Unit> slave = solver.addClock(new Clock<Unit>("slave", new UnitCalc()));
		master.impliesWithTimeDelay(slave, measuring, 1.0);
		
		// Set the current time on the measuring clock
		measuring.newTick(0.0);
		// Set a triggering tick on the master clock
		master.newTick();
		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// We should have a tick at 1.0 on the measuring clock because it will trigger a tick on the slave at that time
		assertTrue(measuring.getTick(1.0) != null);
		
		// Go to next instant
		solver.removeNowTicks();
		
		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// The tick at 1.0 on the measuring clock should be now
		assertTrue((measuring.getNowTick() != null) && (measuring.getNowTick().getTagValue() == 1.0));
		// and there should be an implied now tick on the slave clock
		assertTrue(slave.getNowTick() != null);
	}
	
	@Test
	public void test2() {
		// Test the definition of a periodic clock, which implies itself with a time delay equal to its period
		ClockSet solver = new ClockSet();
		Clock<Double> clock = solver.addClock(new Clock<Double>("periodic", this.tcalc));
		clock.impliesWithTimeDelay(clock, clock, 1.0);
		clock.newTick(0.0);
		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		assertTrue(clock.getTick(1.0) != null);
		
		// Go to next instant
		solver.removeNowTicks();
		
		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}

		assertTrue((clock.getNowTick() != null) && (clock.getNowTick().getTagValue() == 1.0));
		assertTrue(clock.getTick(2.0) != null);
		
		// Go to next instant
		solver.removeNowTicks();
		
		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}

		assertTrue((clock.getNowTick() != null) && (clock.getNowTick().getTagValue() == 2.0));
		assertTrue(clock.getTick(3.0) != null);
	}
	
	@Test
	public void test3() {
		// Test with activation interval shorter than delay
		ClockSet solver = new ClockSet();
		Clock<Double> measuring = solver.addClock(new Clock<Double>("measuring", this.tcalc));
		Clock<Double> master = solver.addClock(new Clock<Double>("master", this.tcalc));
		Clock<Unit> slave = solver.addClock(new Clock<Unit>("slave", new UnitCalc()));
		master.impliesWithTimeDelay(slave, measuring, 1.0);
		master.sameTags(measuring);
		
		// Set the current time on the measuring clock
		measuring.newTick(0.0);
		// Set a triggering tick on the master clock
		master.newTick(0.5);
		// Set another tick which occurs before the delayed tick on counting
		master.newTick(1.2);

		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// We should have a now tick at 0.0 on the measuring clock, and no other tick
		assertTrue(measuring.getNowTick().getTagValue() == 0.0);
		assertTrue(measuring.getTicks().size() == 1);
		assertFalse(master.hasTickNow());
		assertFalse(slave.hasTickNow());

		// Go to next instant
		solver.removeNowTicks();
		
		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// We should now be at 0.5, so a tick should be posted at 1.5 on the measuring clock
		assertTrue(master.getNowTick().getTagValue() == 0.5);
		assertFalse(slave.hasTickNow());
		assertTrue(measuring.getTick(1.5) != null);
		assertTrue(measuring.getTicks().size() == 1);
		
		// Go to next instant
		solver.removeNowTicks();
				
		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// We should now be at 1.2 because of the master clock, so a tick should be posted at 2.2 on the measuring clock
		assertTrue(master.getNowTick().getTagValue() == 1.2);
		assertFalse(slave.hasTickNow());
		assertFalse(measuring.hasTickNow());
		assertTrue(measuring.getTick(1.5) != null);
		assertTrue(measuring.getTick(2.2) != null);
		assertTrue(measuring.getTicks().size() == 2);
		
		// Go to next instant
		solver.removeNowTicks();
				
		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// We should now be at 1.5, because we have a tick to create on the slave clock
		assertFalse(master.hasTickNow());
		assertTrue(slave.hasTickNow());
		assertTrue(measuring.getNowTick().getTagValue() == 1.5);
		assertTrue(measuring.getTick(2.2) != null);
		assertTrue(measuring.getTicks().size() == 2);
		
		// Go to next instant
		solver.removeNowTicks();
				
		try {
			solver.solve();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// We should now be at 2.2, because we have a tick to create on the slave clock
		assertFalse(master.hasTickNow());
		assertTrue(slave.hasTickNow());
		assertTrue(measuring.getNowTick().getTagValue() == 2.2);
		assertTrue(measuring.getTicks().size() == 1);
		
	}
}
