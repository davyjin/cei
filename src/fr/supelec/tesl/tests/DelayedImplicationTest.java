/*
 * This file is part of TESL.
 *
 * TESL is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * TESL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with TESL. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.tesl.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.ClockSet;
import fr.supelec.tesl.core.RoundDouble;
import fr.supelec.tesl.core.TagCalculus;
import fr.supelec.tesl.core.Unit;
import fr.supelec.tesl.core.UnitCalc;


public class DelayedImplicationTest {
	Clock<Unit> master;
	Clock<Unit> counting;
	Clock<Unit> slave;
	
	ClockSet solver = new ClockSet();

	public DelayedImplicationTest() {
		TagCalculus<Unit> tcalc = new UnitCalc();
		this.master = this.solver.addClock(new Clock<Unit>("master", tcalc, false));
		this.counting = this.solver.addClock(new Clock<Unit>("counting", tcalc, false));
		this.slave = this.solver.addClock(new Clock<Unit>("slave", tcalc, false));
		solver.getLogger().setLoggingAllKinds(true);
	}
	
	@Test
	public void test1() {
		System.out.println("== Test 1 ==");
		final int count = 3;
		
		this.master.impliesWithDelay(this.slave, this.counting, count);
		
		// Test basic behavior: tick on master, then 'count' ticks on counting => tick on slave
		for (int i = 0; i <= count; i++) {
			solver.getLogger().log("info", "Phase ", i);
			if (i == 0) {
				this.master.newTick().setNow(true);
			}
			if (i > 0) {
				this.counting.newTick().setNow(true);
			}
			Exception ex = null;
			try {
				this.solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);
			if (i == count) {
				assertTrue(this.slave.hasTickNow());
			} else {
				assertFalse(this.slave.hasTickNow());
			}
			this.solver.resetClocks();
		}
	}

	@Test
	public void test2() {
		System.out.println("== Test 2 ==");
		final int count = 3;
		
		this.master.impliesWithDelay(this.slave, this.counting, count);
		
		// Test not immediate behavior: tick on master and counting, then 'count' ticks on counting => tick on slave
		for (int i = 0; i <= count; i++) {
			solver.getLogger().log("info", "Phase ", i);
			if (i == 0) {
				this.master.newTick().setNow(true);
			}
			if (i >= 0) {
				this.counting.newTick().setNow(true);
			}
			Exception ex = null;
			try {
				this.solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);
			if (i == count) {
				assertTrue(this.slave.hasTickNow());
			} else {
				assertFalse(this.slave.hasTickNow());
			}
			this.solver.resetClocks();
		}
	}

	@Test
	public void test3() {
		System.out.println("== Test 3 ==");
		final int count = 3;
		
		// Immediate delayed implication
		this.master.impliesWithDelay(this.slave, this.counting, count, true);
		
		// Test immediate behavior: tick on master and counting, then 'count'-1 ticks on counting => tick on slave
		for (int i = 1; i <= count; i++) {
			solver.getLogger().log("info", "Phase ", i);
			if (i == 1) {
				this.master.newTick().setNow(true);
			}
			if (i >= 1) {
				this.counting.newTick().setNow(true);
			}
			Exception ex = null;
			try {
				this.solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);
			if (i == count) {
				assertTrue(this.slave.hasTickNow());
			} else {
				assertFalse(this.slave.hasTickNow());
			}
			this.solver.resetClocks();
		}
	}

	@Test
	public void test4() {
		System.out.println("== Test 4 ==");
		// Test for 0 delay
		final int count = 0;
		
		this.master.impliesWithDelay(this.slave, this.counting, count, false);
		
		// Test not immediate behavior: tick on master and counting => tick on slave
		for (int i = 0; i < 2; i++) {
			solver.getLogger().log("info", "Phase ", i);
			if (i == 0) {
				this.master.newTick().setNow(true);
			}
			if (i >= 0) {
				this.counting.newTick().setNow(true);
			}
			Exception ex = null;
			try {
				this.solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);
			if (i == 0) {
				assertTrue(this.slave.hasTickNow());
			} else {
				assertFalse(this.slave.hasTickNow());
			}

			this.solver.resetClocks();
		}
	}

	@Test
	public void test5() {
		System.out.println("== Test 5 ==");
		// Test for 0 delay
		final int count = 0;
		
		// Immediate delay
		this.master.impliesWithDelay(this.slave, this.counting, count, true);
		
		// Test immediate behavior: tick on master and counting => negative delay => no tick on slave (it misses the zero delay)
		for (int i = 0; i < 2; i++) {
			solver.getLogger().log("info", "Phase ", i);
			if (i == 0) {
				this.master.newTick().setNow(true);
			}
			if (i >= 0) {
				this.counting.newTick().setNow(true);
			}
			Exception ex = null;
			try {
				this.solver.solve();
			} catch (Exception e) {
				ex = e;
				e.printStackTrace();
			}	
			assertTrue(ex == null);
			assertFalse(this.slave.hasTickNow());

			this.solver.resetClocks();
		}
	}
	
	@Test
	public void testCJT() {
		ClockSet set = new ClockSet();

		Clock<Double> e = set.addClock(new Clock<Double>("E", new RoundDouble()));
		Clock<Double> a = set.addClock(new Clock<Double>("A", new RoundDouble()));
		Clock<Double> b = set.addClock(new Clock<Double>("B", new RoundDouble()));

		e.impliesThroughFilter(a, 0, 1, 5, 1);
		
		a.impliesWithDelay(b, e, 4);
		
		for(double i = 0; i < 20; i++) {
			e.newTick(i);
		}

		for(int step = 1; step <= 20; step++) {
			System.err.println("##### Solve [" + step + "] ######");
			Exception ex = null;
			try {
				set.solve();
			} catch (Exception e1) {
				ex = e1;
				e1.printStackTrace();
			}

			// There should not be any exception
			assertTrue(ex == null);
			// a is periodic on e with period 6 because of the filtering pattern 0:1(5:1)
			if ((step - 1) % 6 == 0) {
				assertTrue(a.hasTickNow());
			} else {
				assertFalse(a.hasTickNow());
			}
			// b is a delayed by 4 on e, so it ticks four steps after a (e ticks at each step) 
			if ((step - 5) % 6 == 0) {
				assertTrue(b.hasTickNow());
			} else {
				assertFalse(b.hasTickNow());
			}
			
			System.err.println(set);

			set.removeNowTicks();
		}
	}
}
