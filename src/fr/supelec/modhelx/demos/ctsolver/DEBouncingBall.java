package fr.supelec.modhelx.demos.ctsolver;


import java.util.HashMap;
import java.util.concurrent.Semaphore;

import cei.EquationSimple;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.AbstractDEMoC;
import fr.supelec.modhelx.core.simulation.DrivingClock;
import fr.supelec.modhelx.core.state_management.Attribute;
import fr.supelec.modhelx.core.state_management.ManagedBlock;
import fr.supelec.tesl.core.RoundDouble;
import fr.supelec.tesl.core.Tag;
import fr.supelec.tesl.core.TagCalculus;

public class DEBouncingBall extends ManagedBlock {
	
	public final static String PIN_EVENT = "event";
	public final static String PIN_RESET = "reset";
	public final static String PIN_Y_OUT = "yOut";
	public final static String PIN_YDOT_OUT = "yDotOut";
	public final static String PIN_YDDOT_OUT = "yDDotOut";
	public final static String PIN_Y_IN = "yIn";
	public final static String PIN_YDOT_IN = "yDotIn";
	public final static String PIN_YDDOT_IN = "yDDotIn";
	
	private HashMap<String, Pin> pinTable;
	
	private Attribute<DrivingODESolverSimple> ode_solver;
		
	public final static double YINIT = 10.0;
	public final static double YDOTINIT = 0.0;
	public final static double YDDOTINIT = 0.0;

	private Attribute<Double> y;
	private Attribute<Double> yDot;
	private Attribute<Double> yDDot;	
	
	public DEBouncingBall(String name, Object... properties) throws Exception {
		super(name, properties);
		this.pinTable = new HashMap<String, Pin>();
		this.initializeIO();		
		setProperties(properties);		
	}
	
	public void initializeIO() {
		addPinToModel(PIN_EVENT);
		addPinToModel(PIN_RESET);
		addPinToModel(PIN_Y_OUT);
		addPinToModel(PIN_YDOT_OUT);
		addPinToModel(PIN_YDDOT_OUT);
		addPinToModel(PIN_Y_IN);
		addPinToModel(PIN_YDOT_IN);
		addPinToModel(PIN_YDDOT_IN);
	}
		
	public Pin getPinByName(String name) {
		return this.pinTable.get(name);
	}
	
	public void addPinToModel(String name) {
		Pin p = new Pin(name);
		this.addPin(p);
		this.pinTable.put(name, p);
	}
	
	@Override
	public void doStartOfSnapshot() {
		
	}

	@Override
	public void doPreSetup() {
	}
	
	@Override
	public void wrapup() {
		this.ode_solver.getValue().finish();
	}
	@Override
	public void doSetup() {		
		this.initializeAttributes();
		DrivingClock<Double> drivingClock = this.ode_solver.getValue().getDrivingClock();
		getSolver().addClock(drivingClock);
		drivingClock.setListener(getEngine());
		// Use the same time scale for my driving clock and my MoC clock
		drivingClock.sameTags(((AbstractDEMoC)getMoC()).getClock());
		// My driving clock implies my MoC clock
		drivingClock.implies(getMoC().getClock());
		this.ode_solver.getValue().start(); // Start the solver
	}
	
	public void initializeAttributes() {
		this.ode_solver = new Attribute<DrivingODESolverSimple>(DrivingODESolverSimple.class, false);
		this.ode_solver.init(new DrivingODESolverSimple(this, this.pinTable));
		this.y = new Attribute<Double>(Double.class);
		this.y.init(DEBouncingBall.YINIT);
		this.yDot = new Attribute<Double>(Double.class);
		this.yDot.init(DEBouncingBall.YDOTINIT);
		this.yDDot = new Attribute<Double>(Double.class);
		this.yDDot.init(DEBouncingBall.YDDOTINIT);
	}
	
	@Override
	public boolean hasObservationRequest() {
		return this.ode_solver.getValue().hasNewValue();
	}
	
	@Override
	public final Tag<Double> getTimeOfRequest() {
		return this.ode_solver.getValue().getDrivingClock().getNowTick().getTag();
	}
	
	@Override
	public void doUpdate() {
		if (this.ode_solver.getValue().hasNewValue()) {
			double y0 = this.ode_solver.getValue().currentValue();
			double y1 = this.ode_solver.getValue().currentDerivative();
			this.y.setValue(y0);
			this.y.commit();
			this.yDot.setValue(y1);
			this.yDot.commit();
			getPinByName(PIN_Y_OUT).putValue(y0);
			getPinByName(PIN_YDOT_OUT).putValue(y1);
		}
	}

	@Override
	public void doReset() {
		
	}

	@Override
	public void doEndOfSnapshot() {
		
	}
	
	private class DrivingODESolverSimple extends Thread {
		private double currentTime;
		private boolean running;
		private double value;
		private double derivative;
		private double eventSignal;
		private ODESolverClockSimple solvingClock;
		private Semaphore tick_production;
				
		private EquationSimple equation;
		private HashMap<String, Pin> pinTable;
		private double t0 = 0.0;
		private double tFinal = 20.0;
		private double h = 0.01;		
		
		public DrivingODESolverSimple(DEBouncingBall block, HashMap<String, Pin> t) {
			this.solvingClock = new ODESolverClockSimple(this, block.getName() + " driving clock", new RoundDouble());
			this.tick_production = new Semaphore(0);
			this.running = true;			
			this.currentTime = t0;
			this.pinTable = t;				
			this.equation = new EquationSimple(t0, tFinal, h, this.pinTable, false);			
		}
		
		public DrivingClock<Double> getDrivingClock() {
			return this.solvingClock;
		}

		public boolean hasNewValue() {
			return solvingClock.hasTickNow();
		}

		public void allowOneTick() {
			this.tick_production.release();
		}
		
		public void run() {
			this.equation.integrate();
			while (this.running) {
				// solve equation to next sample time
				solve();
				// Wait to be allowed to produce a tick
				try {
					this.tick_production.acquire();
				} catch (InterruptedException e) {
					// Interrupted ? => may be we should stop
					continue;
				}
				this.solvingClock.newTick(currentTime()).setNow(true);
				this.solvingClock.endOfProduction();
			}
		}
		
		private void solve() {
			try {
				this.equation.update();
			} catch (InterruptedException e) {
				System.err.println("Error while updating equation (cei module)");
				e.printStackTrace();
			}
			this.currentTime = this.equation.getCurrentTime();
			double[] state = this.equation.getCurrentState();
			this.value = state[0];
			this.derivative = state[1];
			this.eventSignal = this.equation.getCurrentEvent();
		}
		
		public void finish() {
			this.running = false;
			this.interrupt();
		}
		
		public double currentTime() {
			// Should return the current time in the solver
			return this.currentTime;
		}
		
		@SuppressWarnings("unused")
		public double currentEvent() {
			// Should return -1.0 for a decreasing event, +1.0 for an increasing event, 0.0 if no event
			return this.eventSignal;
		}

		public double currentValue() {
			// Should return the current value of the function
			return this.value;
		}

		public double currentDerivative() {
			// Should return the current derivative of the function
			return this.derivative;
		}
	}
	
	private class ODESolverClockSimple extends DrivingClock<Double> {
		private DrivingODESolverSimple ode_solver;
		private Semaphore production_finished;
		
		public ODESolverClockSimple(DrivingODESolverSimple solver, String name, TagCalculus<Double> tcalc) {
			super(name, tcalc);
			this.ode_solver = solver;
			this.production_finished = new Semaphore(0);
		}

		public void endOfProduction() {
			this.production_finished.release();
		}

		@Override
		public void foresee() {
			// This clock is always able to tick, so it foresees a tick
			newTick(0.0);
		}

		@Override
		public void backToPresent() {
			// Nothing to do, backup/restore will clean the tick produces in foresee
		}
		
		@Override
		public void activate() {
			this.ode_solver.allowOneTick();
		}

		@Override
		public void cancel() {
			// Wait for the solver to finish adding ticks
			try {
				this.production_finished.acquire();
			} catch (InterruptedException e) {
				System.err.println("# Error: "+this+" interrupted while waiting in cancel()");
				e.printStackTrace();
			}		
		}

		@Override
		public void wrapup() {
			// Nothing special to do		
		}
	}
}
