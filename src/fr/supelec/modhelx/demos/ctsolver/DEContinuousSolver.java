/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.ctsolver;

import java.util.concurrent.Semaphore;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.AbstractDEMoC;
import fr.supelec.modhelx.core.state_management.Attribute;
import fr.supelec.modhelx.core.state_management.Parameter;
import fr.supelec.modhelx.core.state_management.TimedManagedBlock;
import fr.supelec.tesl.core.RoundDouble;

/**
 * A DEContinuous solver is a Block for the Discrete Events MoC, which produces the value 
 * of a function of continuous time on its "output" pin at each update. The instants at which 
 * the function is sampled is given by the "period" property (default 1.0) and the "inittime" 
 * property (default 0.0).
 * The value of the function is obtained by solving an ordinary differential equation
 * which is given in property "equation". Property "eventTrigger" gives a function which
 * should change sign when an event is detected. If this property is not null, an event
 * will be produced on the "event" pin, and the corresponding value of the function at
 * this time will be produced on the "output" pin. 
 * 
 * @author frederic.boulanger@supelec.fr, davy.jin@supelec.fr, matthieu.vanluipen@supelec.fr
 */
public class DEContinuousSolver extends TimedManagedBlock<Double> {
	/** The equation of this DEContinuousSolver. */
	private Attribute<String> equation_;
	/** The solver of this DEContinuousSolver. */
	private Attribute<ODESolver> solver_;
	/** The period with which the value is produced. */
	private Parameter<Double> period_;
	/** The output pins of the block. */
	private final Pin event_;
	private final Pin output_;
	/** Semaphore for the availability of the next sample. */
	private final Semaphore nextSample_;

	public DEContinuousSolver(String name, Object ... properties) {
		super(Double.class, new RoundDouble(), name,
				"equation", "y=0", "period", 0.1, "inittime", 0.0);
		this.event_ = addPin(new Pin(getName() + "_event"));
		this.output_ = addPin(new Pin(getName() + "_output"));
		this.nextSample_ = new Semaphore(0);
		setProperties(properties);
	}

	public Pin output() {
		return this.output_; 
	}
	
	public Pin event() {
		return this.event_;
	}
	
	public void nextSampleIsAvailable() {
		this.nextSample_.release();
	}
	
	@Override
	public void doSetup() {
		// Create the attributes and parameters
		this.solver_ = new Attribute<ODESolver>(ODESolver.class, false); // No warning if no property for initializing the solver
		this.equation_ = new Attribute<String>(String.class);
		this.period_ = new Parameter<Double>(Double.class);
		// Initialize attributes and parameter from the properties, with eventual name mappings
		initFromProperties("equation_=equation", "period_=period", nextObsTimeAttribute()+"=inittime");
		this.solver_.init(new ODESolver(this, this.equation_.getValue()));
		this.nextSample_.drainPermits(); // Reset the semaphore to 0
		this.solver_.getValue().start(); // Start the solver
		// Use the same time scale for my observation clock and the clock of my MoC
		observationClock().sameTags(((AbstractDEMoC)getMoC()).getClock());
		// Let the solver compute the first sample
		this.solver_.getValue().setNextSampleTime(getNextObservationTime());
	}
	
	@Override
	public void doUpdate() {
		try {
			this.nextSample_.acquire();
		} catch (InterruptedException e) {
			System.err.println("# Error: exception while waiting for next sample in " + this);
			e.printStackTrace();
			System.exit(1);
		}
		this.output_.putValue(this.solver_.getValue().currentValue());
		double evt = this.solver_.getValue().currentEvent();
		if (evt != 0.0) {
			this.event_.putValue(evt);
		}
	}

	@Override
	public void doEndOfSnapshot() {
		// If I was updated during this snapshot, schedule my next observation time according to my period
		if (wasUpdated()) {
			double nt = tagSum(((AbstractDEMoC)getMoC()).getCurrentTime(), this.period_.getValue());
			setNextObservationTime(nt);
			this.solver_.getValue().setNextSampleTime(nt);
		}
	}
	
	@Override
	public void wrapup() {
		this.solver_.getValue().finish();
	}
}

class ODESolver extends Thread {
	private DEContinuousSolver myBlock;
	private Semaphore go;
	private double nextTime;
	private boolean running;
	private double value;
	private double derivative;
	// private Equation eq;
	
	public ODESolver(DEContinuousSolver block, String equation) {
		this.myBlock = block;
		this.go = new Semaphore(0);
		this.running = true;
		//this.eq = parseEquation(equation);
	}
	
	public void run() {
		while (this.running) {
			try {
				this.go.acquire();
			} catch (InterruptedException e) {
				// Was interrupted => check this.running again without solving ODE
				continue;
			}
			// solve equation to next sample time
			solve();
			// Tell my block the next sample is available
			this.myBlock.nextSampleIsAvailable();
		}
	}
	
	private void solve() {
		this.value = Math.sin(currentTime());
		this.derivative = Math.cos(currentTime());
	}
	
	public void finish() {
		this.running = false;
		this.interrupt();
	}
	
	public void setNextSampleTime(double time) {
		this.nextTime = time;
		this.go.release();
	}
	
	public double currentTime() {
		// Should return the current time in the solver
		return this.nextTime;
	}
	
	public double currentEvent() {
		// Should return -1.0 for a decreasing event, +1.0 for an increasing event, 0.0 if no event
		return 0.0;
	}

	public double currentValue() {
		// Should return the current value of the function
		return this.value;
	}

	public double currentDerivative() {
		// Should return the current derivative of the function
		return this.derivative;
	}
}
