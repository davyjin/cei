/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.ctsolver;

import java.io.IOException;
import java.util.concurrent.Semaphore;

import org.antlr.runtime.RecognitionException;

import cei.*;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.AbstractDEMoC;
import fr.supelec.modhelx.core.simulation.DrivingClock;
import fr.supelec.modhelx.core.state_management.Attribute;
import fr.supelec.modhelx.core.state_management.ManagedBlock;
import fr.supelec.tesl.core.RoundDouble;
import fr.supelec.tesl.core.Tag;
import fr.supelec.tesl.core.TagCalculus;

/**
 * A DEDrivingContinuous solver is a Block for the Discrete Events MoC, which produces the value 
 * of a function of continuous time on its "output" pin at each update. The instants at which 
 * the function is sampled are determined by the continuous solver and signaled to the simulation 
 * engine using a driving clock.
 * The value of the function is obtained by solving an ordinary differential equation
 * which is given in property "equation". Property "eventTrigger" gives a function which
 * should change sign when an event is detected. If this property is not null, an event
 * will be produced on the "event" pin, and the corresponding value of the function at
 * this time will be produced on the "output" pin. 
 * 
 * @author frederic.boulanger@supelec.fr, davy.jin@supelec.fr, matthieu.vanluipen@supelec.fr
 */
public class DEDrivingContinuousSolver extends ManagedBlock {
	/** The equation of this DEContinuousSolver. */
	private Attribute<String> equation_;
	/** The solver of this DEContinuousSolver. */
	private Attribute<DrivingODESolver> ode_solver_;
	/** The output pins of the Delay. */
	private final Pin event_;
	private final Pin output_;
	
	public DEDrivingContinuousSolver(String name, Object ... properties) {
		super(name, "equation", "y=0", "inittime", 0.0);
		this.event_ = addPin(new Pin(getName() + "_event"));
		this.output_ = addPin(new Pin(getName() + "_output"));
		setProperties(properties);
	}

	public Pin output() {
		return this.output_; 
	}
	
	public Pin event() {
		return this.event_;
	}
	
	@Override
	public void doSetup() {
		// Create the attributes and parameters
		this.ode_solver_ = new Attribute<DrivingODESolver>(DrivingODESolver.class, false); // No warning if no property for initializing the solver
		this.equation_ = new Attribute<String>(String.class);
		// Initialize attributes and parameter from the properties, with eventual name mappings
		initFromProperties("equation_=equation");
		this.ode_solver_.init(new DrivingODESolver(this, this.equation_.getValue()));
		DrivingClock<Double> drivingClock = this.ode_solver_.getValue().getDrivingClock();
		getSolver().addClock(drivingClock);
		drivingClock.setListener(getEngine());
		// Use the same time scale for my driving clock and my MoC clock
		drivingClock.sameTags(((AbstractDEMoC)getMoC()).getClock());
		// My driving clock implies my MoC clock
		drivingClock.implies(getMoC().getClock());
		this.ode_solver_.getValue().start(); // Start the solver
	}
	
	@Override
	public boolean hasObservationRequest() {
		return this.ode_solver_.getValue().hasNewValue();
	}
	
	@Override
	public final Tag<Double> getTimeOfRequest() {
		return this.ode_solver_.getValue().getDrivingClock().getNowTick().getTag();
	}
	
	@Override
	public void doUpdate() {
		if (this.ode_solver_.getValue().hasNewValue()) {
			this.output_.putValue(this.ode_solver_.getValue().currentValue());
			double evt = this.ode_solver_.getValue().currentEvent();
			if (evt != 0.0) {
				this.event_.putValue(evt);
			}
		}
	}

	@Override
	public void wrapup() {
		this.ode_solver_.getValue().finish();
	}

	@Override
	public void doEndOfSnapshot() {
		// Nothing special to do
	}

	@Override
	public void doStartOfSnapshot() {
		// Nothing special to do		
	}

	@Override
	public void doPreSetup() {
		// Nothing special to do		
	}

	@Override
	public void doReset() {
		// Nothing special to do		
	}

}

class ODESolverClock extends DrivingClock<Double> {
	private DrivingODESolver ode_solver;
	private Semaphore production_finished;
	
	public ODESolverClock(DrivingODESolver solver, String name, TagCalculus<Double> tcalc) {
		super(name, tcalc);
		this.ode_solver = solver;
		this.production_finished = new Semaphore(0);
	}

	public void endOfProduction() {
		this.production_finished.release();
	}

	@Override
	public void foresee() {
		// This clock is always able to tick, so it foresees a tick
		newTick(0.0);
	}

	@Override
	public void backToPresent() {
		// Nothing to do, backup/restore will clean the tick produces in foresee
	}
	
	@Override
	public void activate() {
		this.ode_solver.allowOneTick();
	}

	@Override
	public void cancel() {
		// Wait for the solver to finish adding ticks
		try {
			this.production_finished.acquire();
		} catch (InterruptedException e) {
			System.err.println("# Error: "+this+" interrupted while waiting in cancel()");
			e.printStackTrace();
		}		
	}

	@Override
	public void wrapup() {
		// Nothing special to do		
	}
}

class DrivingODESolver extends Thread {
	private double currentTime;
	// private double nextTime;
	private boolean running;
	private double value;
	private double derivative;
	private double eventSignal;
	private ODESolverClock solvingClock;
	private Semaphore tick_production;
	
	
	private Equation equation;
	private double t0 = 0.0;
	private double tFinal = 20.0;
	private double h = 0.01;	
	
	
	public DrivingODESolver(DEDrivingContinuousSolver block, String equation) {
		this.solvingClock = new ODESolverClock(this, block.getName() + " driving clock", new RoundDouble());
		this.tick_production = new Semaphore(0);
		this.running = true;
		
		this.currentTime = t0;
		// this.nextTime = t0 + h;
			
		try {
			this.equation = new Equation(equation, t0, tFinal, h, false);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RecognitionException e) {
			e.printStackTrace();
		} 
	}
	
	public DrivingClock<Double> getDrivingClock() {
		return this.solvingClock;
	}

	public boolean hasNewValue() {
		return solvingClock.hasTickNow();
	}

	public void allowOneTick() {
		this.tick_production.release();
	}
	
	public void run() {
		this.equation.integrate();
		while (this.running) {
			// solve equation to next sample time
			solve();
			// Wait to be allowed to produce a tick
			try {
				this.tick_production.acquire();
			} catch (InterruptedException e) {
				// Interrupted ? => may be we should stop
				continue;
			}
			this.solvingClock.newTick(currentTime()).setNow(true);
			this.solvingClock.endOfProduction();
		}
	}
	
	private void solve() {
		try {
			this.equation.update();
		} catch (InterruptedException e) {
			System.err.println("Error while updating equation (cei module)");
			e.printStackTrace();
		}
		this.currentTime = this.equation.getCurrentTime();
		// this.nextTime = this.currentTime + this.h;
		double[] state = this.equation.getCurrentState();
		this.value = state[0];
		this.derivative = state[1];
		this.eventSignal = this.equation.getCurrentEvent();
	}
	
	public void finish() {
		this.running = false;
		this.interrupt();
	}
	
	public double currentTime() {
		// Should return the current time in the solver
		return this.currentTime;
	}
	
	public double currentEvent() {
		// Should return -1.0 for a decreasing event, +1.0 for an increasing event, 0.0 if no event
		return this.eventSignal;
	}

	public double currentValue() {
		// Should return the current value of the function
		return this.value;
	}

	public double currentDerivative() {
		// Should return the current derivative of the function
		return this.derivative;
	}
}
