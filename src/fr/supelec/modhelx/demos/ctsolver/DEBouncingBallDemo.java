package fr.supelec.modhelx.demos.ctsolver;

import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.DEMoC;
import fr.supelec.modhelx.demos.atomicblocks.DEConst;
import fr.supelec.modhelx.demos.atomicblocks.DEPlotter;

public class DEBouncingBallDemo {

	public static void main(String[] args) throws Exception {
		final double GRAVITYVAL = -9.81;
		final String PIN_GRAVITY = "g";
		// Model
		Model<Double> deModel = new Model<Double>("DeModel", new DEMoC());
		
		//Solver
		DEBouncingBall solver = deModel.add(new DEBouncingBall("CT Solver"));
		
		// Constant gravity block
		DEConst gravity = deModel.add(new DEConst("BlocG", "value", GRAVITYVAL, "period", 0.01, "inittime", 0.01));
		Pin pinG = gravity.addPin(new Pin(PIN_GRAVITY));
		
		// Display
		DEPlotter display = deModel.add(new DEPlotter("DE display",
				"title", "Solver output",
				"connect", false,
				"manhattan", false,
				"xscale", 50.0, "yscale", 100.0,
				"xmin", 0.0, "xmax", 25.0,
				"ymin", 0.0, "ymax", 10.0,
				"xticks", 5.0, "yticks", 0.5,
				"dotw", 1, "doth", 1));
		Pin display_value = display.addPin(new Pin("value"));
		
		// yDDot = -g
		solver.getPinByName(DEBouncingBall.PIN_YDDOT_IN).connectTo(pinG);
		// event according to y
		solver.getPinByName(DEBouncingBall.PIN_Y_OUT).connectTo(solver.getPinByName(DEBouncingBall.PIN_EVENT));
		// reset using this value
		solver.getPinByName(DEBouncingBall.PIN_RESET).connectTo(solver.getPinByName(DEBouncingBall.PIN_YDOT_OUT));
		
		// Plug in the display
		solver.getPinByName(DEBouncingBall.PIN_Y_OUT).connectTo(display_value);		

		////////////// Simulation //////////////////////
		Engine exec = new Engine("ModHelX", deModel);

		// Run as fast as possible
		//exec.getSolver().addClock(new ASAPClock("ASAP"));

		// Stop after 200 snapshots
		exec.stopAfter(2000);

		exec.getSolver().getLogger().setLoggingAllKinds(false);
		exec.getLogger().setLoggingAllKinds(false);

		exec.run();
	}

}
