/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.ctsolver;

import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.DEMoC;
//import fr.supelec.modhelx.core.simulation.ASAPClock;
import fr.supelec.modhelx.demos.atomicblocks.DEPlotter;

/**
 * A demo of DeContinuousSolver block.
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class DECTSolverDemo {
	public static void main(String[] args) {
		Model<Double> deModel = new Model<Double>("DeModel", new DEMoC());
//		DEContinuousSolver solver = deModel.add(new DEContinuousSolver("CT Solver", "equation", "y''=-9.81"));
		DEDrivingContinuousSolver solver = deModel.add(new DEDrivingContinuousSolver("CT Solver", "equation", "test/balleode.txt"));

		DEPlotter display = deModel.add(new DEPlotter("DE display",
				                                      "title", "Solver output",
				                                      "connect", false,
				                                      "manhattan", false,
				                                      "xscale", 50.0, "yscale", 100.0,
				                                      "xmin", 0.0, "xmax", 25.0,
				                                      "ymin", -2.0, "ymax", 10.0,
				                                      "xticks", 5.0, "yticks", 0.5,
				                                      "dotw", 1, "doth", 1));
		Pin display_value= display.addPin(new Pin("value"));
		Pin display_event= display.addPin(new Pin("event"));
		
		solver.output().connectTo(display_value);
		solver.event().connectTo(display_event);
		
		////////////// Simulation //////////////////////
		Engine exec = new Engine("ModHelX", deModel);

		// Run as fast as possible
		//exec.getSolver().addClock(new ASAPClock("ASAP"));

		// Stop after 200 snapshots
		exec.stopAfter(2000);

		exec.getSolver().getLogger().setLoggingAllKinds(false);
		exec.getLogger().setLoggingAllKinds(false);

		exec.run();
	}
}
