/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.semantic_adaptation;

import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.AbstractDEMoC;
import fr.supelec.modhelx.core.mocs.sdf.SDFMoC;
import fr.supelec.modhelx.core.state_management.Attribute;
import fr.supelec.modhelx.core.state_management.MapAttribute;
import fr.supelec.modhelx.core.state_management.Parameter;
import fr.supelec.modhelx.core.state_management.SnapshotAttribute;
import fr.supelec.modhelx.core.state_management.TimedManagedInterfaceBlock;
import fr.supelec.modhelx.core.state_management.TransientMap;
import fr.supelec.modhelx.core.state_management.TransientValue;
import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.RoundDouble;
import fr.supelec.tesl.core.TagCalculus;
import fr.supelec.tesl.core.TickCreation;
import fr.supelec.tesl.core.Unit;

/**
 * This is a special interface block (IB) between DE and SDF that can react
 * to threshold crossing on the outputs of its internal SDF model.
 * For each output with a "threshold" property, the IB compares the current and previous
 * values produces by the internal model to the threshold. If the threshold was crossed
 * from below to above, an event with value +1 is produced. If the threshold was crossed
 * from above to below, an event with value -1 is produced.
 * If the output also has a "precision" property, it gives the precision required on the 
 * date of the event. So, if the threshold was crossed, and the time elapsed between the 
 * linearly estimated crossing time t and the current time is greater than the precision, the 
 * snapshot is not validated, and a tick is added to the control clock of the IB to make 
 * the next update occur at t. Since the snapshot has not been validated, this
 * next update will occur during the same snapshot. This means that the DE time will 
 * change. Since we cannot update the SDF model at any time, the IB computes the current
 * value of the outputs by linear interpolation between the previous and current values,
 * without updating the internal SDF model.
 * 
 * @author frederic.boulanger@supelec.fr
 */

public class DE_SDF_LinInterpolator extends TimedManagedInterfaceBlock<Double, Double> {
	/** Current value of each input pin. */
	private MapAttribute<Pin, Object> inputPinValue_;
	/** Current value of each output pin. */
	private MapAttribute<Pin, Object> outputPinValue_;
	
	/** Should we backtrack in time to interpolate an output? */
	private TransientValue<Boolean> backtrack_;
	/** Upper bound of the value of the output pins of the internal model in an interpolation step. */
	@SnapshotAttribute  // this transient attribute should be reverted only at startOfSnapshot, not at reset()
	private TransientMap<Pin, Object> interpolationTable_;
	/** Time at which the upper bounds in interpolationTable_ were reached. */
	@SnapshotAttribute  // this transient attribute should be reverted only at startOfSnapshot, not at reset()
	private TransientValue<Double> interpolation_time_;
	
	/** The update period of the internal SDF model. */
	private Parameter<Double> update_period_ = null;
	/** The last update time of this interface block, including interpolation updates. */
	private Attribute<Double> last_update_time_;
	
	public DE_SDF_LinInterpolator(String name, Model<Unit> internalModel, Object ... properties) {
		super(Double.class, new RoundDouble(),
			  Double.class, new RoundDouble(), name, internalModel, properties);
	}

	@Override
	public void doSetup() {
		this.update_period_ = new Parameter<Double>(Double.class);
		this.last_update_time_ = new Attribute<Double>(Double.class, false); // No warning if not initialized
		this.backtrack_ = new TransientValue<Boolean>(Boolean.class);
		this.backtrack_.init(false);
		this.interpolation_time_ = new TransientValue<Double>(Double.class);
		this.interpolationTable_ = new TransientMap<Pin, Object>();
		this.inputPinValue_ = new MapAttribute<Pin, Object>();
		this.outputPinValue_ = new MapAttribute<Pin, Object>();
		Clock<Double> deClock = ((AbstractDEMoC)getMoC()).getClock();
		Clock<Unit> sdfClock = ((SDFMoC) this.getInternalModel().getMoC()).getClock();
		sdfClock.implies(deClock);
		this.observationClock().implies(deClock);
		this.observationClock().sameTags(deClock);
		this.internalObservationClock().sameTags(this.observationClock());
		initFromProperties(nextInternalObsTimeAttribute()+"=initial_update", "update_period_=update_period");
	}
	
	@Override
	public void adaptIn() {
		// This interface block keep a record of the most recent value received on each input
		// We update the input value each time an event occurs
		for (Pin p : this.getInputPins()) {
			if (p.hasToken()) {
				this.inputPinValue_.put(p, p.readValue(Object.class, true));
			}
		}
	}
	
	@Override
	public void doPreUpdate() {
		double current_time = ((AbstractDEMoC)this.getMoC()).getCurrentTime();
		// Normal update
		// For each input pin, add as many tokens as required on the SDF model corresponding inputs
		for (Pin p : this.getInputPins()) {
			Object cur_value = this.inputPinValue_.get(p);
			if (cur_value == null) {
				throw new Error("### DE-SDF adaptation error: no current value for input " + p.getName());
			}
			for (Pin target : p.getSuccessorPins()) {
				int rate = SDFMoC.getPinRate(target);
				for (int i = 0; i < rate; i++) {
					target.putValue(cur_value);
				}
			}
		}
		System.out.println("## Update at " + current_time);
	}

	@Override
	public void doPostUpdate() {
		// Nothing to do
	}
	
	@Override
	public void doGhostUpdate() {
		// Update of the block when the internal model should not be updated
		if (this.interpolation_time_.getValue() != null) {
			// This is an interpolation update, do not update the internal model
			double current_time = ((AbstractDEMoC)this.getMoC()).getCurrentTime();
			System.out.println("# Interpolation at " + current_time);
			// Interpolate
			double deltatime = this.interpolation_time_.getValue() - this.last_update_time_.getValue();
			double prorata = current_time - this.last_update_time_.getValue();
			for (Pin p : this.getInternalModel().getOutputPins()) {
				double curval = (Double)this.outputPinValue_.get(p);
				double nxtval = (Double) this.interpolationTable_.get(p);
				p.putValue(curval + (nxtval - curval)*prorata/deltatime);
			}
		}
	}

	@Override
	public void adaptOut() {
		TagCalculus<Double> tcalc = ((AbstractDEMoC)getMoC()).getClock().getTagCalculus();
		// Build DE events from SDF outputs
		for (Pin p : this.getInternalModel().getOutputPins()) {
			if (p.hasToken()) {
				double cur_value = p.readValue(Number.class, true).doubleValue();
				Double prev_value = (Double) this.outputPinValue_.get(p);
				for (Pin target : p.getSuccessorPins()) {
					if (target.hasProperty("threshold")) {
						// The DE output pin produces threshold crossing events
						if ((prev_value != null) && (cur_value != prev_value)) {
							double thres = target.getProperty("threshold", Number.class).doubleValue();
							double change = 0.0;
							if ((cur_value >= thres) && (prev_value < thres)) {
								change = 1.0;  // up crossing
							} else if ((cur_value <= thres) && (prev_value > thres)) {
								change = -1.0; // down crossing
							}
							if (change != 0) {
								// There was a level crossing. At what time?
								if (target.hasProperty("precision") && (this.last_update_time_ != null)) {
									double currentTime = ((AbstractDEMoC)getMoC()).getCurrentTime();
									double cross_time = tcalc.add(this.last_update_time_.getValue(),
											                      (thres - prev_value)
											                      * tcalc.subtract(currentTime, this.last_update_time_.getValue())
											                      / (cur_value - prev_value));
									double precision = target.getProperty("precision", Number.class).doubleValue();
									if ((currentTime - cross_time) < precision) {
										// Time precision is good enough, produce the event now
										target.putValue(change);
									} else {
										// We have a problem: the threshold crossing time is not know with enough precision
										// Try again at the time of crossing according to linear interpolation
										Engine engine  = getEngine();
										// We add a tick on the control clock so that the solver 
										// sets the right time in DE at the next iteration
										engine.addClockOperation(new TickCreation<Double>(this.observationClock(), cross_time));
										// Time of the current sample (for interpolation)
										this.interpolation_time_.setValue(currentTime);
										// We have to backtrack in time
										this.backtrack_.setValue(true);
									}
								} else { // No constraint on time precision, produce the event now
									target.putValue(change);
								}
							}
						}
					} else {
						// No "threshold" attribute => regular "event = value change" behavior
						if ((prev_value == null) || (cur_value != prev_value)) {
							target.putValue(cur_value);
						}
					}
				}
				// Remember the value produced by the SDF model 
				this.outputPinValue_.put(p, cur_value);
			}
		}
		// If we have to backtrack (the precision on the date of the threshold crossing event is not good enough)
		// store the necessary information on the value of the output to  make a linear interpolation
		if (this.backtrack_.getValue()) {
			for (Pin p : this.getInternalModel().getOutputPins()) {
				this.interpolationTable_.put(p, this.outputPinValue_.get(p));
			}
		}
	}

	@Override
	public boolean validate() {
		// The snapshot is valid if we don't have to backtrack in time
		return !this.backtrack_.getValue();
	}

	@Override
	public void doEndOfSnapshot() {
		if (this.update_period_.getValue() != null) {
			// If we are periodic
			if (internalWasUpdated()) {
				// and we were updated, update the next time for update
				this.last_update_time_.setValue(((AbstractDEMoC)this.getMoC()).getCurrentTime());
				advanceNextInternalObservationTime(this.update_period_);
			}
		}
	}
}
