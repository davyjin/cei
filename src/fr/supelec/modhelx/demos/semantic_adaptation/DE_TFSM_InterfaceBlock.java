/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.semantic_adaptation;

import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.abstractsemantics.Relation;
import fr.supelec.modhelx.core.abstractsemantics.Token;
import fr.supelec.modhelx.core.mocs.de.AbstractDEMoC;
import fr.supelec.modhelx.core.mocs.tfsm.TFSMMoC;
import fr.supelec.modhelx.core.state_management.ManagedInterfaceBlock;
import fr.supelec.modhelx.core.state_management.Parameter;
import fr.supelec.modhelx.core.state_management.TransientValue;
//import modhelxs.mocs.tfsm.TFSMMoC;
import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.Tag;

/**
 * This DE/TFSM interface block implements decoding input DE events to
 * TFSM input symbols and encoding TFSM output symbols into DE events.
 * 
 * The matching between DE events and TFSM symbols is described by setting
 * relations between DE pins and TFSM pins. When a DE pin is connected to 
 * several TFSM pins, the value of a DE events on this pin is compared to
 * the "match" property of the TFSM pins. A token is put on every TFSM pin
 * whose "match" property is null or equals the value of the DE event. If
 * no TFSM pin match the value, a warning is emitted. To avoid this warning,
 * it is possible to tell which values should be ignored in the "discard"
 * property of the DE pin (the type of this property is Object[]).
 * 
 * The "match" property of output TFSM pins is used to set the value of the
 * DE event produced on all the output pins of the interface block which are
 * connected to them.
 * 
 * Two properties "a" and "b" configure the adaptation of time scales between 
 * DE and TFSM. Time in TFSM is a*t+b if 't' is the time in DE. This means that
 * time in TFSM runs 'a' times faster than in DE and is offset by 'b'.
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class DE_TFSM_InterfaceBlock extends ManagedInterfaceBlock {
	private Parameter<Double> timescale_coeff;
	private Parameter<Double> timescale_offset;
	private TransientValue<Boolean> incoming_event;
	
	private final TFSMMoC internal_moc_;
	

	public DE_TFSM_InterfaceBlock(String name, Model<Double> model, Object ... properties) {
		super(name, model, properties);
		this.internal_moc_ = (TFSMMoC) getInternalModel().getMoC();
	}

	@Override
	public void doStartOfSnapshot() {
		// Nothing special to do
	}

	@Override
	public void doPreSetup() {
		// Nothing special to do
	}

	@Override
	public void doSetup() {
		this.timescale_coeff = new Parameter<Double>(Double.class);
		this.timescale_offset = new Parameter<Double>(Double.class);
		this.incoming_event = new TransientValue<Boolean>(Boolean.class);
		initFromProperties("timescale_coeff=a", "timescale_offset=b");
		this.incoming_event.init(false);
		Clock<Double> externalClock = ((AbstractDEMoC)(this.getParent().getModel().getMoC())).getClock();
		Clock<Double> internalClock = this.internal_moc_.getClock();
		externalClock.affineTags(internalClock, this.timescale_coeff.getValue(), this.timescale_offset.getValue());
		internalClock.implies(externalClock);
	}

	@Override
	public void doReset() {
		// Nothing to reset
	}

	@Override
	public void adaptIn() {
		// Decode DE events into FSM symbols 
		for (Pin pIn : this.getInputPins()) {
			while (pIn.hasToken()) {
				Object value = pIn.readValue(Object.class, true);
				// Values that should be ignored for this input
				Object[] ignored_values = pIn.getProperty("discard", Object[].class);
				boolean ignore = false;
				if (ignored_values != null) {
					for (Object ignored : ignored_values) {
						if (value.equals(ignored)) {
							ignore = true;
							break;
						}
					}
				}
				// If the value should be ignored, skip to next token or pin
				if (ignore) {
					continue;
				}
//				System.err.println("## DE_TFSM IB receiving " + value + " on " + pIn.getName() + " @" + ((AbstractDEMoC)getMoC()).getCurrentTime());
				boolean matched = false;
				// Look for a TFSM symbol matching the value of the event
				for (Relation r : pIn.getOutgoingRelations()) {
					Pin target = r.getTarget();
					Object match = target.getProperty("match", Object.class);
					if ((match == null) || (match.equals(value))) {
						target.addToken(new Token(this.internal_moc_));
						this.incoming_event.setValue(true);
						matched = true;
					}
				}
				if (! matched) {
					System.err.println("## Warning: no match for value " + value + " on pin " + pIn.getName() + " in " + getName());
				}
			}
		}
	}

	@Override
	public void doPreUpdate() {
		// Nothing special to do
	}

	@Override
	public boolean shouldUpdateModel() {
		return this.incoming_event.getValue() || this.internal_moc_.getClock().hasTickNow();
	}

	@Override
	public void doPostUpdate() {
		// Nothing special to do
	}

	@Override
	public void doGhostUpdate() {
		// Nothing special to do
	}

	@Override
	public void adaptOut() {
		for (Pin pOut : this.getInternalModel().getOutputPins()) {
			while (pOut.hasToken()) {
				// Consume token
				pOut.getToken(true);
				// Get encoded value for DE event
				Object match = pOut.getProperty("match", Object.class);
				if (match == null) {
					System.err.println("## Warning: no match value for event " + pOut.getName() 
				                       + " in " + getName());
				}
				for (Relation r : pOut.getOutgoingRelations()) {
					Pin target = r.getTarget();
					target.putValue(match);		
				}
			}
		}
	}

	@Override
	public void doEndOfSnapshot() {
		// Nothing special to do
	}

	@Override
	public boolean hasObservationRequest() {
		return this.internal_moc_.getClock().hasTickNow();
	}

	@Override
	public Tag<Double> getTimeOfRequest() {
		Tag<Double> tag = ((AbstractDEMoC)(getParent().getModel().getMoC())).getClock().getNowTick().getTag();
		return tag;
	}
}
