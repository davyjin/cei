/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.semantic_adaptation;

import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.AbstractDEMoC;
import fr.supelec.modhelx.core.mocs.sdf.SDFMoC;
import fr.supelec.modhelx.core.state_management.MapAttribute;
import fr.supelec.modhelx.core.state_management.Parameter;
import fr.supelec.modhelx.core.state_management.TimedManagedInterfaceBlock;
import fr.supelec.tesl.core.RoundDouble;
import fr.supelec.tesl.core.Unit;

/**
 * An interface block between DE and SDF which takes care of adapting data, time
 * and control between the two MoCs.
 * 
 * The "period" property sets the period at which the internal model should be observed.
 * The "initial_update" property sets the time of the first observation of the internal model
 * Input values received from DE are stored to maintain the current value of each input.
 * When the internal model is updated, it is provided the necessary number of data samples
 * on each of its inputs. DE events are produced on output only when the SDF signal
 * changes its value.
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class DE_SDF_InterfaceBlock extends TimedManagedInterfaceBlock<Double, Double> {
	private MapAttribute<Pin, Object> currentPinValue_;
	private Parameter<Double> update_period;
	
	public DE_SDF_InterfaceBlock(String name, Model<Unit> internalModel, Object ... properties) {
		super(Double.class, new RoundDouble(),
			  Double.class, new RoundDouble(), name, internalModel, properties);
	}

	@Override
	public void doSetup() {
		// We give observation dead lines in terms of DE time
		this.observationClock().sameTags(((AbstractDEMoC)getMoC()).getClock());
		// We give internal model observation dead lines in terms of observation time 
		this.internalObservationClock().sameTags(this.observationClock());
		
		this.currentPinValue_ = new MapAttribute<Pin, Object>();
		this.update_period = new Parameter<Double>(Double.class);
		this.initFromProperties(nextInternalObsTimeAttribute()+"=initial_update", "update_period=period");
	}

	@Override
	public void adaptIn() {
		// Update input values when events occur
		for (Pin p : this.getInputPins()) {
			if (p.hasToken()) {
				Object value = p.readValue(Object.class, true);
				this.currentPinValue_.put(p, value);
			}
		}
	}

	@Override
	public void doPreUpdate() {
		// For each input pin, add as many token as required on the SDF model corresponding inputs
		for (Pin p : this.getInputPins()) {
			Object cur_value = this.currentPinValue_.get(p);
			if (cur_value == null) {
				throw new Error("### DE-SDF adaptation error: no current value for input " + p.getName());
			}
			for (Pin target : p.getSuccessorPins()) {
				int rate = SDFMoC.getPinRate(target);
				for (int i = 0; i < rate; i++) {
					target.putValue(cur_value);
				}
			}
		}
	}

	@Override
	public void doPostUpdate() {
		// Nothing to do
	}
	
	@Override
	public void doGhostUpdate() {
		// Nothing to do
	}
	
	@Override
	public void adaptOut() {
		for (Pin p : this.getInternalModel().getOutputPins()) {
			if (p.hasToken()) {
				Object cur_value = p.readValue(Object.class, true);
				Object prev_value = this.currentPinValue_.get(p);
				if (!cur_value.equals(prev_value)) {
					this.currentPinValue_.put(p, cur_value);
					for (Pin target : p.getSuccessorPins()) {
						target.putValue(cur_value);
					}
				}
			}
		}
	}

	@Override
	public void doEndOfSnapshot() {
		if (this.update_period.getValue() != null) {
			if (this.internalWasUpdated()) {
				advanceNextInternalObservationTime(this.update_period);
			}
		}
	}
}
