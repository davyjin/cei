/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.semantic_adaptation;

import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.state_management.ManagedInterfaceBlock;
import fr.supelec.tesl.core.Unit;

/**
 * A dumb interface block between DE and SDF.
 * This interface block ignore the specificities of DE and SDF.
 * It simply copies the value of DE input events to SDF data samples
 * and updates its internal model each time it is updated.
 * On output, each data sample produced by the SDF model is transformed
 * into a DE event.
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class DE_SDF_DumbInterfaceBlock extends ManagedInterfaceBlock {	
	public DE_SDF_DumbInterfaceBlock(String name, Model<Unit> internalModel, Object ... properties) {
		super(name, internalModel, properties);
	}

	@Override
	public void doPreSetup() {
		// Nothing to do
	}

	@Override
	public void doSetup() {
		// Nothing to do, I am dumb
	}

	@Override
	public void doStartOfSnapshot() {
		// Nothing to do
	}

	@Override
	public void doReset() {
		// Nothing to do
	}

	@Override
	public void adaptIn() {
		// Transfer input events to the inputs of the model
		for (Pin p : this.getInputPins()) {
			if (p.hasToken()) {
				Object value = p.readValue(Object.class, true);
				for (Pin target : p.getSuccessorPins()) {
					target.putValue(value);
				}
			}
		}
	}

	@Override
	public boolean shouldUpdateModel() {
		// Update internal model each time the interface block is updated
		return true;
	}

	@Override
	public void doPreUpdate() {
		// Nothing to do, I am dumb
	}

	@Override
	public void doPostUpdate() {
		// Nothing to do
	}
	
	@Override
	public void doGhostUpdate() {
		// Nothing to do
	}
	
	@Override
	public void adaptOut() {
		// Transfer the outputs of the model to output events
		for (Pin p : this.getInternalModel().getOutputPins()) {
			if (p.hasToken()) {
				Object value = p.readValue(Object.class, true);
				for (Pin target : p.getSuccessorPins()) {
					target.putValue(value);
				}
			}
		}
	}

	@Override
	public void doEndOfSnapshot() {
		// Nothing to do, I am dumb
	}
}
