package fr.supelec.modhelx.demos.elec;

import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;

import fr.supelec.modhelx.core.abstractsemantics.Block;
import fr.supelec.modhelx.core.abstractsemantics.BlockStructure;
import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.ModelOfComputationImpl;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.abstractsemantics.Token;
import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.ClockSet;
import fr.supelec.tesl.core.Unit;
import fr.supelec.tesl.core.UnitCalc;

/**
 * "Electronics" MoC. Each block has input and output voltages and currents. 
 * The MoC ensures that two connected blocks have the same voltage and the
 * same current.
 * 
 * Currently this is static only: only one snapshot is done, which computes
 * the static state of the circuit.
 * 
 * @author Christophe Jacquet
 * @version 2014-01-10
 *
 */
public class ElecMoC extends ModelOfComputationImpl<Unit> {
	private final Clock<Unit> elecClock;
	private final Deque<Block> schedulingQueue = new LinkedList<Block>();
	private Block currentBlock = null;
	private boolean hasPropagatedValue = false;
	private int noPropagatedValuesCount = 0;
	
	public ElecMoC(String name) {
		super(name);
		this.elecClock = new Clock<Unit>(name+" clock", new UnitCalc());
	}
	
	@Override
	public Clock<Unit> getClock() {
		return this.elecClock;
	}

	@Override
	public Unit getCurrentTime() {
		return Unit.getInstance();
	}

	@Override
	public void reset(BlockStructure s) {
		super.reset(s);
		// add all blocks to the scheduling queue, order is arbitrary
		this.schedulingQueue.addAll(s.getBlocks());
		noPropagatedValuesCount = 0;
	}
	
	@Override
	public void schedule(BlockStructure s) {
		this.currentBlock = this.schedulingQueue.pop();
		this.schedulingQueue.addLast(this.currentBlock);
		
		/// LOG
		System.out.println("======================");
		for(Block b : s.getBlocks()) {
			System.out.println(b.getName() + (b == this.currentBlock ? "*" : "") + ":");
			for(Pin p : b.getInputPins()) System.out.println("   " + p.getName() + " " + (p.hasToken() ? p.getToken(false).getValue() : ""));
			for(Pin p : b.getOutputPins()) System.out.println("   " + p.getName() + " " + (p.hasToken() ? p.getToken(false).getValue() : ""));
		}
	}

	@Override
	public void update(BlockStructure s) {
		this.currentBlock.update();
	}

	@Override
	public void propagate(BlockStructure s) {
		hasPropagatedValue = false;
		this.propagateFrom(this.currentBlock.getInputPins());
		this.propagateFrom(this.currentBlock.getOutputPins());
		
		if(hasPropagatedValue) noPropagatedValuesCount = 0;
		else {
			noPropagatedValuesCount++;
			System.out.println("No propagation");
		}
	}
	
	private void propagateFrom(Collection<Pin> pins) {
		for(Pin p : pins) {
			if(! p.hasToken()) continue;
			Object value = p.getToken(false).getValue();
			
			Pin connectedPin = null;
			for(Pin pp : p.getPredecessorPins()) {
				if(connectedPin != null) throw new RuntimeException(
						"There should be 1-1 relation only, but " + p + ", " + 
						connectedPin + " and " + pp + " are connected");
				connectedPin = pp;
			}
			for(Pin pp : p.getSuccessorPins()) {
				if(connectedPin != null) throw new RuntimeException(
						"There should be 1-1 relation only, but " + p + ", " + 
						connectedPin + " and " + pp + " are connected");
				connectedPin = pp;
			}
			
			if(connectedPin != null && !connectedPin.hasToken()) {
				Token t = new Token(this);
				t.setValue(value);
				connectedPin.addToken(t);
				hasPropagatedValue = true;
			}
		}
	}

	@Override
	public boolean canGoFurther(BlockStructure s) {
		return noPropagatedValuesCount < s.getBlocks().size();
	}

	@Override
	public void startOfUpdate(BlockStructure s) {
		// nothing to do
	}

	@Override
	public void endOfUpdate(BlockStructure s) {
		// nothing to do
	}
	
	@Override
	public void setup(BlockStructure s) {
		super.setup(s);
		
		@SuppressWarnings("unchecked")
		Model<Unit> m = (Model<Unit>) s.getModel();
		Engine engine = m.getEngine();
		ClockSet solver = engine.getSolver();
		// Add our clock to the clock solver
		solver.addClock(this.elecClock);

		
		// auto-trigger
		this.elecClock.newTick();
	}

}
