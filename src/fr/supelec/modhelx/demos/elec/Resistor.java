package fr.supelec.modhelx.demos.elec;

/**
 * Standard resistor, with characteristic U = R * I.
 * 
 * @author Christophe Jacquet
 * @version 2014-01-10
 *
 */
public class Resistor extends ElectronicDipole {
	public Resistor(String name, Double resistance) {
		super(name, "resistance", resistance);
	}
	
	@Override
	protected void applyLaw(Quantities q) {
		double r = this.getProperty("resistance", Double.class);
		
		if(q.inVoltage != null && q.current != null && q.outVoltage == null) q.outVoltage = q.inVoltage - r * q.current;
		
		if(q.inVoltage != null && q.outVoltage != null && q.current == null) q.current = (q.inVoltage - q.outVoltage) / r;
		
		if(q.current != null && q.outVoltage != null && q.inVoltage == null) q.inVoltage = q.outVoltage + r * q.current;
	}

	@Override
	protected boolean isValid(Quantities q) {
		return q.inVoltage == null || q.outVoltage == null || q.current == null ||
				Math.abs(q.inVoltage - q.outVoltage - this.getProperty("resistance", Double.class) * q.current) < EPSILON;
	}

}
