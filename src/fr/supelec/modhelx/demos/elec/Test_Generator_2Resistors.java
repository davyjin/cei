package fr.supelec.modhelx.demos.elec;


import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.simulation.ASAPClock;
import fr.supelec.tesl.core.Unit;

/**
 * Test with a generator (2 V) connected to two resistors. As the MoC has no
 * global view of the model, it is unable to compute the current in the 
 * resistors. A human would calculate the equivalent resistor, then the
 * current, then the voltages.
 * 
 * @author Christophe Jacquet
 * @version 2014-01-10
 *
 */
public class Test_Generator_2Resistors {

	public static void main(String[] args) {
		Model<Unit> elecModel = new Model<Unit>("elec", new ElecMoC("elecMoC"));
		
		VoltageGenerator g = elecModel.add(new VoltageGenerator("g", 2.));
		Resistor r1 = elecModel.add(new Resistor("r1", 100.));
		Resistor r2 = elecModel.add(new Resistor("r2", 400.));
		
		
		g.connectOutputTo(r1);
		r1.connectOutputTo(r2);
		r2.connectOutputTo(g);
		
		Engine exec = new Engine("ModHelX", elecModel);
		
		// Run as fast as possible
		exec.getSolver().addClock(new ASAPClock("ASAP"));

		
		// Stop after 1 snapshot
		exec.stopAfter(1);

		exec.getSolver().getLogger().setLoggingAllKinds(false);
		exec.getLogger().setLoggingAllKinds(true);
		
		exec.run();
	}

}
