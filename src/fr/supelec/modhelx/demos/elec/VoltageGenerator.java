package fr.supelec.modhelx.demos.elec;

/**
 * Ideal voltage generator. One pin is connected to the ground, the other one
 * is set to the nominal voltage, regardless of the current. 
 * 
 * @author Christophe Jacquet
 * @version 2014-01-10
 *
 */
public class VoltageGenerator extends ElectronicDipole {

	public VoltageGenerator(String name, Double voltage) {
		super(name, "voltage", voltage);
	}

	@Override
	protected void applyLaw(Quantities q) {
		if(q.inVoltage == null) q.inVoltage = 0.;
		
		if(q.outVoltage == null) {
			q.outVoltage = this.getProperty("voltage", Double.class);
		}
	}

	@Override
	protected boolean isValid(Quantities q) {
		return q.inVoltage == null || q.outVoltage == null ||
				Math.abs(q.outVoltage - q.inVoltage - this.getProperty("voltage", Double.class)) < EPSILON;
	}
}
