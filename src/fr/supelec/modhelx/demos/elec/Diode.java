package fr.supelec.modhelx.demos.elec;

import fr.supelec.modhelx.core.state_management.NotManagedAttribute;

/**
 * A simple diode, with simplified characteristic: 
 * V < Vthreshold and I=0, or V = Vthreshold and I > 0.
 * 
 * 
 * @author Christophe Jacquet
 * @version 2014-01-10
 *
 */
public class Diode extends ElectronicDipole {
	@NotManagedAttribute
	private boolean conducting;
	
	public Diode(String name, Double thresholdVoltage) {
		super(name, "thresholdVoltage", thresholdVoltage);
	}

	@Override
	protected void applyLaw(Quantities q) {
		if(conducting) {
			double t = this.getProperty("thresholdVoltage", Double.class);
			if(q.inVoltage != null && q.outVoltage == null) q.outVoltage = q.inVoltage - t;
			if(q.outVoltage != null && q.inVoltage == null) q.inVoltage = q.outVoltage + t;
		} else {
			if(q.current == null) q.current = 0.;
		}
	}

	@Override
	protected boolean isValid(Quantities q) {
		if(conducting && q.current > 0 || !conducting && q.current == 0) return true;
		else {
			conducting = ! conducting;
			System.out.println("Diode " + this.getName() + ": is not valid. Trying with conducting = " + conducting);
			return false;
		}
	}

	@Override
	public void startOfSnapshot() {
		super.startOfSnapshot();
		
		this.conducting = true;
		System.out.println("Diode: trying with conducting = true");
	}
}
