package fr.supelec.modhelx.demos.elec;

import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.simulation.ASAPClock;
import fr.supelec.tesl.core.Unit;

/**
 * Test with a generator (6 V), a diode (LED, threshold 2.5 V) and a resistor.
 * As the generator is above the diode's threshold, the diode conducts
 * current.
 * 
 * @author Christophe Jacquet
 * @version 2014-01-10
 *
 */
public class Test_Generator_Diode_Resistor_Conducting {

	public static void main(String[] args) {
		Model<Unit> elecModel = new Model<Unit>("elec", new ElecMoC("elecMoC"));
		
		VoltageGenerator g = elecModel.add(new VoltageGenerator("g", 6.));
		Resistor r = elecModel.add(new Resistor("r", 100.));
		Diode d = elecModel.add(new Diode("d", 2.5));
		
		
		g.connectOutputTo(d);
		d.connectOutputTo(r);
		r.connectOutputTo(g);
		
		Engine exec = new Engine("ModHelX", elecModel);
		
		// Run as fast as possible
		exec.getSolver().addClock(new ASAPClock("ASAP"));

		
		// Stop after 1 snapshot
		exec.stopAfter(1);

		exec.getSolver().getLogger().setLoggingAllKinds(false);
		exec.getLogger().setLoggingAllKinds(true);
		
		exec.run();
	}

}
