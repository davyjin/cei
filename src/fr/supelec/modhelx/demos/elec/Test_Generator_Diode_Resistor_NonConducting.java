package fr.supelec.modhelx.demos.elec;

import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.simulation.ASAPClock;
import fr.supelec.tesl.core.Unit;

/**
 * Test with a generator (2 V), a diode (LED, threshold 2.5 V) and a resistor.
 * As the generator is below the diode's threshold, the diode is non 
 * conducting. However the first guess of the model is that the diode may be
 * conducting, which leads to a non-validation of the first iteration.
 * 
 * @author Christophe Jacquet
 * @version 2014-01-10
 *
 */
public class Test_Generator_Diode_Resistor_NonConducting {

	public static void main(String[] args) {
		Model<Unit> elecModel = new Model<Unit>("elec", new ElecMoC("elecMoC"));
		
		VoltageGenerator g = elecModel.add(new VoltageGenerator("g", 2.));
		Resistor r = elecModel.add(new Resistor("r", 100.));
		Diode d = elecModel.add(new Diode("d", 2.5));
		
		
		g.connectOutputTo(d);
		d.connectOutputTo(r);
		r.connectOutputTo(g);
		
		Engine exec = new Engine("ModHelX", elecModel);
		
		// Run as fast as possible
		exec.getSolver().addClock(new ASAPClock("ASAP"));

		
		// Stop after 1 snapshot
		exec.stopAfter(1);

		exec.getSolver().getLogger().setLoggingAllKinds(false);
		exec.getLogger().setLoggingAllKinds(true);
		
		exec.run();
	}

}
