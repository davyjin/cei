package fr.supelec.modhelx.demos.elec;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.abstractsemantics.Token;
import fr.supelec.modhelx.core.state_management.PlainManagedBlock;

/**
 * This class factors out behavior common to electronic dipoles.
 * 
 * @author Christophe Jacquet
 * @version 2014-01-10
 *
 */
public abstract class ElectronicDipole extends PlainManagedBlock {
	protected final Pin inCurrent, inVoltage, outCurrent, outVoltage;
	protected final static double EPSILON = 1e-6;

	public ElectronicDipole(String name, Object ... properties) {
		super(name, properties);
		
		this.inCurrent = this.addPin(new Pin(name + "inA"));
		this.inVoltage = this.addPin(new Pin(name + "inV"));
		this.outCurrent = this.addPin(new Pin(name + "outA"));
		this.outVoltage = this.addPin(new Pin(name + "outV"));
	}
	
	@Override
	public void doSetup() {
		// nothing
	}

	@Override
	public void doUpdate() {
		this.homogenizeCurrents();
		
		Quantities q = new Quantities();
		
		applyLaw(q);
		
		if(q.currentIni != q.current) setValueOfPin(q.current, this.inCurrent);
		if(q.inVoltageIni != q.inVoltage) setValueOfPin(q.inVoltage, this.inVoltage);
		if(q.outVoltageIni != q.outVoltage) setValueOfPin(q.outVoltage, this.outVoltage);
		
		this.homogenizeCurrents();
	}
	
	@Override
	public void doReset() {
		for(Pin p : this.getInputPins()) {
			p.clearTokens();
		}
		for(Pin p : this.getOutputPins()) {
			p.clearTokens();
		}
	}
	
	private static Double getNullableValue(Pin p) {
		if(p.hasToken()) {
			return (Double) p.getToken(false).getValue();
		} else {
			return null;
		}
	}
	
	private void setValueOfPin(Object value, Pin p) {
		Token t = new Token(this.getMoC());
		t.setValue(value);
		p.addToken(t);		
	}
	
	private void homogenizeCurrents() {
		if(this.inCurrent.hasToken() && ! this.outCurrent.hasToken()) {
			setValueOfPin(this.inCurrent.getToken(false).getValue(), this.outCurrent);
		}
		if(this.outCurrent.hasToken() && ! this.inCurrent.hasToken()) {
			setValueOfPin(this.outCurrent.getToken(false).getValue(), this.inCurrent);
		}
	}
	
	abstract protected void applyLaw(Quantities q);

	@Override
	public boolean validate() {
		Quantities q = new Quantities();
		// q.current is indeed the "inCurrent". Check if it is the same as the "outCurrent"
		Double outCurrent = getNullableValue(ElectronicDipole.this.outCurrent);
		if(q.current != outCurrent) return false;
		
		boolean v = isValid(q);
		if(! v) System.out.println(this.getName() + ": does not validate");
		return v;
	}
	
	abstract protected boolean isValid(Quantities q);

	
	protected class Quantities {
		protected Double current, inVoltage, outVoltage;
		protected final Double currentIni, inVoltageIni, outVoltageIni;
		
		public Quantities() {
			this.current = getNullableValue(ElectronicDipole.this.inCurrent);
			this.inVoltage = getNullableValue(ElectronicDipole.this.inVoltage);
			this.outVoltage = getNullableValue(ElectronicDipole.this.outVoltage);

			this.currentIni = this.current;
			this.inVoltageIni = this.inVoltage;
			this.outVoltageIni = this.outVoltage;
		}
	}
	
	public void connectOutputTo(ElectronicDipole d) {
		this.outCurrent.connectTo(d.inCurrent);
		this.outVoltage.connectTo(d.inVoltage);
	}
	
}
