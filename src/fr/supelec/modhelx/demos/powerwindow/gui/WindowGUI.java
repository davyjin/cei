/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.powerwindow.gui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.supelec.modhelx.core.abstractsemantics.ObservableEntity;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.demos.atomicblocks.EventFeeder;
import fr.supelec.modhelx.demos.atomicblocks.Probe;

/**
 * WindowGUI is a Swing graphical interface for the power window.
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
@SuppressWarnings("serial")
public class WindowGUI extends JFrame implements ActionListener, Probe.PinListener {
	/**
	 * WindowGUIComponent is the Swing component which draws the window frame.
	 * 
	 * @author frederic.boulanger@supelec.fr
	 *
	 */
	private static class WindowGUIComponent extends JComponent {
		private static final long serialVersionUID = 1L;
		private double pos_;
		private Color win_color_tint_;
		private Color win_color_clear_;
		private Color win_color_;
		private Image door_;
		private Image hand_;
		private int y_basewin_;
		private int y_wintop_;
		private int y_obstacle_;
		
		private static final String classpath_ = "fr/supelec/modhelx/demos/powerwindow/gui/";
		private URL getRessource(String name) {
			return getClass().getClassLoader().getResource(classpath_ +name);
		}
		private Image getImage(String name) {
			return java.awt.Toolkit.getDefaultToolkit().createImage(getRessource(name));
		}
		public WindowGUIComponent() {
			this.pos_ = 0.0;
			this.win_color_tint_ = new Color(0.4f, 0.4f, 0.4f, 0.7f);
			this.win_color_clear_ = new Color(0.6f, 0.6f, 0.6f, 0.4f);
			this.win_color_ = this.win_color_clear_;
			this.door_ = getImage("Portiere.png");
			this.hand_ = getImage("main_small.png");
			this.y_basewin_ = 167;
			this.y_wintop_ = 45;
			this.y_obstacle_ = -1;
		}
		
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2d = (Graphics2D) g;
			g2d.drawImage(this.door_, 0, 0, null);
			int zone_x[] = {180, 150, 122, 102, 102, 328, 328, 354, 350, 340, 320, 272, 180 };
			int zone_y[] = { 45,  80, 120, 167, 300, 300, 167, 136,  98,  84,  74,  60,  45 };
			Polygon zone = new Polygon(zone_x, zone_y, zone_x.length);
			g2d.setClip(zone);
			if (this.y_obstacle_ >= 0) {
				g2d.drawImage(this.hand_, 150, this.y_obstacle_, null);
			}
			int frame_x[] = {180, 150, 122, 102, 328, 354, 350, 340, 320, 272, 180 };
			int frame_y[] = { 45,  80, 120, 167, 167, 136,  98,  84,  74,  60,  45 };
			Polygon window = new Polygon(frame_x, frame_y, frame_x.length);
			window.translate((int)((this.pos_ - 1.0) * 95),
					         (int)((1.0 - this.pos_) * (this.y_basewin_ - this.y_wintop_)));
			Polygon frame = new Polygon(frame_x, frame_y, frame_x.length);
			g2d.setClip(frame);
			g2d.setColor(this.win_color_);
			g2d.fillPolygon(window);
		}
	
		@Override
		public Dimension getPreferredSize() {
			return new Dimension(400, 400);
		}

		public void setTinted(boolean tinted) {
			if (tinted) {
				this.win_color_ = this.win_color_tint_;
			} else {
				this.win_color_ = this.win_color_clear_;
			}
//			repaint();
		}

		public void setObstacle(boolean present) {
			if (present) {
				this.y_obstacle_ = this.y_wintop_ + (int)((1 - this.pos_) * (this.y_basewin_ - this.y_wintop_)) - 50;
			} else {
				this.y_obstacle_ = -1;
			}
//			repaint();
		}
		
		public void setPos(double pos) {
			this.pos_ = pos;
//			repaint();
//			paintComponent(getGraphics());  // required for avoiding lag in the window's movements
		}
	}
	
	protected WindowGUIComponent myWindow_ = null;
	protected EventFeeder myFeeder_ = null;
	protected Pin userCmdPin_ = null;
	private Pin obstaclePin_ = null;
	private Pin positionPin_;
	private Pin motorPin_;
	
	private static WindowGUI instance_ = null;
	
	private JLabel upButton_ = null;
	private JLabel downButton_ = null;
	private boolean winClosing_ = false;
	private boolean winOpening_ = false;
	protected boolean upPushed_ = false;
	protected boolean downPushed_ = false;
	
	public static WindowGUI getInstance() {
		if (instance_ == null) {
			instance_ = new WindowGUI();
		}
		return instance_;
	}
	
	protected void updateButtons() {
		this.upButton_.setIcon(triangleUp(this.upPushed_, this.winClosing_));
		this.downButton_.setIcon(triangleDown(this.downPushed_, this.winOpening_));
//		repaint();
	}
	
	/** The icon of the up button. */
	private static Icon triangleUp(boolean pushed, boolean on) {
		Color bg = new Color(1.0f, 1.0f, 1.0f, 0.0f); // transparent white
		Color fg = new Color(0.5f, 0.5f, 0.5f, 1.0f); // opaque dark gray
		Color line = new Color(0.2f, 0.2f, 0.2f, 1.0f); // opaque very dark gray
		if (pushed) {
			line = new Color(0.8f, 0.8f, 0.8f, 1.0f); // opaque very light gray
		}
		if (on) {
			fg = new Color(0.5f, 1.0F, 0.5f, 1.0f);
		}
		Image img = new BufferedImage(20, 20, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) img.getGraphics();
		g.setBackground(bg);
		g.setColor(fg);
		Polygon p = new Polygon();
		p.addPoint(0, 19);
		p.addPoint(19, 19);
		p.addPoint(10,0);
		p.addPoint(0, 19);
		g.clearRect(0, 0, 20, 20);
		g.fillPolygon(p);
		g.setStroke(new BasicStroke(2.0f));
		g.setColor(line);
		g.drawPolygon(p);
		return new ImageIcon(img);
	}
	
	/** The icon of the down button. */
	private static Icon triangleDown(boolean pushed, boolean on) {
		Color bg = new Color(1.0f, 1.0f, 1.0f, 0.0f); // transparent white
		Color fg = new Color(0.5f, 0.5f, 0.5f, 1.0f); // opaque dark gray
		Color line = new Color(0.2f, 0.2f, 0.2f, 1.0f); // opaque very dark gray
		if (pushed) {
			line = new Color(0.8f, 0.8f, 0.8f, 1.0f); // opaque very light gray
		}
		if (on) {
			fg = new Color(0.5f, 1.0F, 0.5f, 1.0f);
		}
		Image img = new BufferedImage(20, 20, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) img.getGraphics();
		g.setBackground(bg);
		g.setColor(fg);
		Polygon p = new Polygon();
		p.addPoint(1, 1);
		p.addPoint(10, 19);
		p.addPoint(19, 1);
		p.addPoint(1, 1);
		g.clearRect(0, 0, 20, 20);
		g.fillPolygon(p);
		g.setStroke(new BasicStroke(2.0f));
		g.setColor(line);
		g.drawPolygon(p);
		return new ImageIcon(img);
	}
	
	private WindowGUI() {
		getContentPane().setLayout(new BorderLayout());
		JPanel cmd = new JPanel(new GridLayout(6, 1));
		// "Up" button
		this.upButton_ = new JLabel(triangleUp(false, this.winClosing_));
		this.upButton_.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				WindowGUI.this.myFeeder_.feed(WindowGUI.this.userCmdPin_, 1);
				WindowGUI.this.upPushed_ = true;
				updateButtons();
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				WindowGUI.this.myFeeder_.feed(WindowGUI.this.userCmdPin_, 0);
				WindowGUI.this.upPushed_ = false;
				updateButtons();
			}
		});
		cmd.add(this.upButton_);

		// Down button
		this.downButton_ = new JLabel(triangleDown(false, this.winOpening_));
		this.downButton_.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				WindowGUI.this.myFeeder_.feed(WindowGUI.this.userCmdPin_, -1);
				WindowGUI.this.downPushed_ = true;
				updateButtons();
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				WindowGUI.this.myFeeder_.feed(WindowGUI.this.userCmdPin_, 0);
				WindowGUI.this.downPushed_ = false;
				updateButtons();
			}
		});
		cmd.add(this.downButton_);
		// Obstacle
		JCheckBox obst = new JCheckBox("Obstacle", false);
		obst.setActionCommand("OBSTACLE");
		obst.addActionListener(this);
		cmd.add(obst);
		//
		JCheckBox tint = new JCheckBox("Tinted windows", false);
		tint.setActionCommand("TINT");
		tint.addActionListener(this);
		cmd.add(tint);
		//
		this.myWindow_ = new WindowGUIComponent();
		JPanel left = new JPanel();
		left.add(cmd);
		getContentPane().add(left, BorderLayout.WEST);
		getContentPane().add(this.myWindow_, BorderLayout.EAST);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if (cmd.equals("TINT")) {
			this.myWindow_.setTinted(((JCheckBox)e.getSource()).isSelected());
		} else if (cmd.equals("OBSTACLE")) {
			if (((JCheckBox)e.getSource()).isSelected()) {
				this.myFeeder_.feed(this.obstaclePin_, 1);
				this.myWindow_.setObstacle(true);
			} else {
				this.myFeeder_.feed(this.obstaclePin_, -1);
				this.myWindow_.setObstacle(false);
			}
		}
	}

	public void setFeeder(EventFeeder feeder) {
		this.myFeeder_  = feeder;
		if (feeder == null) {
			throw new Error(getClass().getName()+": feeder is null");
		}
		for (Pin p : this.myFeeder_.getInterface()) {
			if (p.getName().equals("userCmd_out")) {
				this.userCmdPin_ = p;
			} else if (p.getName().equals("obstacle_out")) {
				this.obstaclePin_ = p;
			}
		}
		if (this.userCmdPin_== null) {
			throw new Error(getClass().getName()+": feeder has no userCmd_out output pin");
		}
		if (this.obstaclePin_== null) {
			throw new Error(getClass().getName()+": feeder has no obstacle_out output pin");
		}
	}
	
	/**
	 * Since calling repaint() each time there is a change leads to the window
	 * lagging behind the simulation, we don't call repaint at each change.
	 * Instead, we use a thread to periodically call repaint.
	 * There is something weird in the management of the graphical updates in Swing...
	 * 
	 * @author frederic.boulanger@supelec.fr
	 *
	 */
	private static class RepaintThread extends Thread {
		private WindowGUI myWindow_;
		private long delay_;
		private boolean run_;
		
		public RepaintThread(WindowGUI win, long delay) {
			this.myWindow_ = win;
			this.delay_ = delay;
			this.run_ = true;
		}
		
		@Override
		public void run() {
			while (this.run_) {
				this.myWindow_.repaint();
				this.myWindow_.myWindow_.repaint();
				try {
					Thread.sleep(this.delay_);
				} catch (InterruptedException e) {
					// check again if we should run
				}
			}
		}
	}
	private RepaintThread repaint_thread_;
	
	@Override
	public void setup() {
		pack();
		setVisible(true);
		this.repaint_thread_ = new RepaintThread(this, 100);
		this.repaint_thread_.start();
	}

	@Override
	public void wrapup() {
		setVisible(false);
		this.repaint_thread_.interrupt();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

	@Override
	public void pinChanged(Pin p) {
		if (p == this.positionPin_) {
			this.myWindow_.setPos(p.readValue(Number.class, true).doubleValue() / 5);
		}
		if (p == this.motorPin_) {
			double value = p.readValue(Number.class, true).doubleValue();
			if (value == -1.0) {
				this.winOpening_ = true;
				this.winClosing_ = false;
			} else if (value == 0.0) {
				this.winOpening_ = false;
				this.winClosing_ = false;
			} else if (value == 1.0) {
				this.winOpening_ = false;
				this.winClosing_ = true;
			}
			updateButtons();
		}
	}

	@Override
	public void setSource(ObservableEntity src) {
		for (Pin p : src.getInterface()) {
			String name = p.getName();
			if (name.equals("position")) {
				this.positionPin_ = p;
			} else if (name.equals("motor")) {
				this.motorPin_ = p;
			}
		}
		if (this.positionPin_== null) {
			throw new Error(getClass().getName()+": source has no position input pin");
		}
		if (this.motorPin_== null) {
			throw new Error(getClass().getName()+": source has no motor input pin");
		}
	}
}
