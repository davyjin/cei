/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.powerwindow;

import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.AbstractDEMoC;
import fr.supelec.modhelx.core.mocs.de.DEMoC;
import fr.supelec.modhelx.core.simulation.SynchronizingRTClock;
import fr.supelec.modhelx.demos.atomicblocks.DEDelay;
import fr.supelec.modhelx.demos.atomicblocks.EventFeeder;
import fr.supelec.modhelx.demos.atomicblocks.Probe;
import fr.supelec.modhelx.demos.powerwindow.gui.WindowGUI;
import fr.supelec.modhelx.demos.powerwindow.models.WindowController;
import fr.supelec.modhelx.demos.powerwindow.models.WindowModel;
import fr.supelec.modhelx.demos.semantic_adaptation.DE_SDF_InterfaceBlock;
import fr.supelec.modhelx.demos.semantic_adaptation.DE_TFSM_InterfaceBlock;
import fr.supelec.tesl.core.Implication;
import fr.supelec.tesl.core.TagRelation;

/**
 * A demo of the power window with a graphical user interface showing the window, 
 * and with buttons to drive it and display the operation of the motor.
 * 
 * The time in TFSM runs twice as fast as in DE and is offset by one. The 1 delay
 * for switching to automatic mode in TFSM is therefore a 0.5 delay in DE. 
 * The time in DE is synchronized with the system time with 1 unit of DE time for 
 * 1000 units of system time. Since system time is counted in milliseconds, we
 * therefore have 1s of system time for 1 unit of DE time. The delay for switching
 * to automatic mode is 500ms.
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class PowerWindowWithGUI {
	public static void main(String[] args) {
		/////////////////DE MODEL///////////////////
		Model<Double> deModel = new Model<Double>("DeModel", new DEMoC());

		WindowGUI gui = WindowGUI.getInstance();

		EventFeeder feeder = deModel.add(new EventFeeder("GUI feeder"));
		Pin userCmd_out = feeder.addPin(new Pin("userCmd_out"));
		Pin obstacle_out = feeder.addPin(new Pin("obstacle_out"));
		
		gui.setFeeder(feeder);
		
		Probe probe = deModel.add(new Probe("GUI probe", gui));
		Pin probePos = probe.addPin(new Pin("position"));
		Pin probeMotor = probe.addPin(new Pin("motor"));
		gui.setSource(probe);
		
		////////////////TFSM MODEL//////////////////
		WindowController controller = new WindowController("TFSM controller");
		
		///////////////DE-TFSM-IB///////////////////
		DE_TFSM_InterfaceBlock controllerIB = deModel.add(new DE_TFSM_InterfaceBlock("Controller IB", controller,
				                                                                     "a", 2.0, "b", 1.0));
		Pin userCmd_in = controllerIB.addPin(new Pin("userCmd_in"));
		Pin obstacle_in = controllerIB.addPin(new Pin("obstacle_in"));
		Pin windowEnd_in = controllerIB.addPin(new Pin("windowEnd_in"));
		Pin motorCmd_out = controllerIB.addPin(new Pin("motorCmd_out"));
		
		// userCmd is decoded into down(-1)/stop(0)/up(1) symbols for the FSM
		userCmd_in.connectTo(controller.cmd_down());
		controller.cmd_down().setProperty("match", -1);
		userCmd_in.connectTo(controller.cmd_stop());
		controller.cmd_stop().setProperty("match", 0);
		userCmd_in.connectTo(controller.cmd_up());
		controller.cmd_up().setProperty("match", 1);
		
		obstacle_in.connectTo(controller.win_obstacle());
		
		// windowEnd is decoded into window_closed (1)/window_opened (-1) symbols for the FSM
		// Input value 0 (the window is neither fully opened or closed) is discarded
		windowEnd_in.setProperty("discard", new Integer[]{0});
		windowEnd_in.connectTo(controller.win_closed());
		controller.win_closed().setProperty("match", 1);
		windowEnd_in.connectTo(controller.win_opened());
		controller.win_opened().setProperty("match", -1);
		
		// motor_down, motor_stop and motor_up symbols from the FSM are encoded into motorCmd_out -1/0/1
		controller.motor_down().connectTo(motorCmd_out);
		controller.motor_down().setProperty("match", -1);
		controller.motor_stop().connectTo(motorCmd_out);
		controller.motor_stop().setProperty("match", 0);
		controller.motor_up().connectTo(motorCmd_out);
		controller.motor_up().setProperty("match", 1);
		
		///////////////SDF MODEL////////////////////
		WindowModel winModel = new WindowModel("Window SDF model", "scale", 0.2);
				
		//////////////DE-SDF-IB/////////////////////
		// Proper DE_SDF interface block, with periodic activation and level change detection
		DE_SDF_InterfaceBlock windowIB = deModel.add(new DE_SDF_InterfaceBlock("Window IB", winModel,
                "initial_update", 0.1,
                "period", 0.2));

		Pin motorCmd_in = windowIB.addPin(new Pin("motorCmd_in"));
		Pin windowPos_out = windowIB.addPin(new Pin("windowPos_out"));
		Pin windowEnd_out = windowIB.addPin(new Pin("windowEnd_out"));
		
		motorCmd_in.connectTo(winModel.motor_cmd());
		winModel.window_pos().connectTo(windowPos_out);
		winModel.window_end().connectTo(windowEnd_out);
				
		//////////////DE RELATIONS//////////////////
		windowPos_out.connectTo(probePos);
		userCmd_out.connectTo(userCmd_in);
		obstacle_out.connectTo(obstacle_in);
		motorCmd_out.connectTo(motorCmd_in);
		motorCmd_out.connectTo(probeMotor);
		
		DEDelay delay = deModel.add(new DEDelay("end stop delay", "delay", 0.1));
		windowEnd_out.connectTo(delay.input());
		delay.output().connectTo(windowEnd_in);
		
		//////////////Scénario//////////////////////
		Engine exec = new Engine("ModHelX", deModel);

		SynchronizingRTClock rtclock = new SynchronizingRTClock("Real time");
		exec.getSolver().addClock(rtclock);
		AbstractDEMoC deMoC = (AbstractDEMoC)(deModel.getMoC());
		
		// The clock of DE implies the RTclock, so that events in DE are mapped onto real time
		exec.getSolver().addImplicationRelation(new Implication<Double, Long>(deMoC.getClock(), rtclock));
		// Cannot use implies before setup()...
//		deMoC.getClock().implies(rtclock);
		// Make DE time advance by 1 each second (1000 milliseconds)
		exec.getSolver().addTagRelation(new TagRelation<Long, Double>(rtclock, deMoC.getClock()) {
			@Override
			public Double directConversion(Long tag) {
				return toClock().tagQuotient(tag.doubleValue(), 1000.0);
			}

			@Override
			public Long reverseConversion(Double tag) {
				return toClock().tagProduct(tag, 1000.0).longValue();
			}
		});
		
		exec.getSolver().getLogger().setLoggingAllKinds(false);
//		exec.getSolver().getLogger().setLoggingKind("info", true);
		exec.getLogger().setLoggingAllKinds(false);
//		exec.getLogger().setLoggingKind("info", true);

		exec.run();
	}
}
