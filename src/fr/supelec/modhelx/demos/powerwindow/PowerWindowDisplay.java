/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.powerwindow;

import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.InterfaceBlock;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.DEMoC;
import fr.supelec.modhelx.core.simulation.ASAPClock;
import fr.supelec.modhelx.demos.atomicblocks.DEDelay;
import fr.supelec.modhelx.demos.atomicblocks.DEPlotter;
import fr.supelec.modhelx.demos.atomicblocks.DEScenario;
import fr.supelec.modhelx.demos.powerwindow.models.WindowController;
import fr.supelec.modhelx.demos.powerwindow.models.WindowModel;
import fr.supelec.modhelx.demos.semantic_adaptation.DE_SDF_DumbInterfaceBlock;
import fr.supelec.modhelx.demos.semantic_adaptation.DE_SDF_InterfaceBlock;
import fr.supelec.modhelx.demos.semantic_adaptation.DE_TFSM_InterfaceBlock;

/**
 * A demo of the power window with a graphical display of the user command, 
 * end stops and position of the window.
 * We use a DEScenario block to simulate the user pushing the buttons
 * of the power window, and we use a DEPlotter to display the outputs.
 * The time in TFSM runs twice as fast as in DE and is offset by one. The 1 delay
 * for switching to automatic mode in TFSM is therefore a 0.5 delay in DE. 
 * If called without arguments, the semantic adaptation between DE and SDF
 * imposes a periodic activation of the SDF model every 1 unit of DE time, starting at date 1.0.
 * On output, events are produced only on changes of the SDF outputs.
 * If called with at least an argument, the semantic adaptation between DE and SDF is
 * set to "raw" mode: the SDF model is updated each time it receives an input from DE
 * (this may not work with an SDF model that has several or multi-rate inputs), and
 * a DE event is produced for each data sample on the outputs. This shows the importance
 * of explicit semantic adaptation between heterogeneous models.
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class PowerWindowDisplay {
	// If called with arguments, use the basic (Ptolemy like) DE_SDF interface block
	// If called without argument, use the correct interface block
	public static void main(String[] args) {
		/////////////////DE MODEL///////////////////
		Model<Double> deModel = new Model<Double>("DeModel", new DEMoC());

		DEScenario scenario = deModel.add(new DEScenario("DE scenario",
				"scenario", "userCmd_out=(Integer)1@2.0;" +
					        "userCmd_out=(Integer)0@2.8;" +
						    "userCmd_out=(Integer)1@3.0;" +
						    "userCmd_out=(Integer)0@5.0;" +
						    "userCmd_out=(Integer)1@7.0;" +
						    "userCmd_out=(Integer)0@7.4;" +
						    "userCmd_out=(Integer)-1@10.0;" +
						    "userCmd_out=(Integer)0@10.1;"
		));
		
		Pin userCmd_out = scenario.addPin(new Pin("userCmd_out"));
		Pin obstacle_out = scenario.addPin(new Pin("obstacle_out"));
		
		////////////////TFSM MODEL//////////////////
		WindowController controller = new WindowController("TFSM controller");
		
		///////////////DE-TFSM-IB///////////////////
		DE_TFSM_InterfaceBlock controllerIB = deModel.add(new DE_TFSM_InterfaceBlock("Controller IB", controller,
				                                                                     "a", 2.0, "b", 1.0));
		Pin userCmd_in = controllerIB.addPin(new Pin("userCmd_in"));
		Pin obstacle_in = controllerIB.addPin(new Pin("obstacle_in"));
		Pin windowEnd_in = controllerIB.addPin(new Pin("windowEnd_in"));
		Pin motorCmd_out = controllerIB.addPin(new Pin("motorCmd_out"));
		
		// userCmd is decoded into down(-1)/stop(0)/up(1) symbols for the FSM
		userCmd_in.connectTo(controller.cmd_down());
		controller.cmd_down().setProperty("match", -1);
		userCmd_in.connectTo(controller.cmd_stop());
		controller.cmd_stop().setProperty("match", 0);
		userCmd_in.connectTo(controller.cmd_up());
		controller.cmd_up().setProperty("match", 1);
		
		obstacle_in.connectTo(controller.win_obstacle());
		
		// windowEnd is decoded into window_closed (1)/window_opened (-1) symbols for the FSM
		// Input value 0 (the window is neither fully opened or closed) is discarded
		windowEnd_in.setProperty("discard", new Integer[]{0});
		windowEnd_in.connectTo(controller.win_closed());
		controller.win_closed().setProperty("match", 1);
		windowEnd_in.connectTo(controller.win_opened());
		controller.win_opened().setProperty("match", -1);
		
		// motor_down, motor_stop and motor_up symbols from the FSM are encoded into motorCmd_out -1/0/1
		controller.motor_down().connectTo(motorCmd_out);
		controller.motor_down().setProperty("match", -1);
		controller.motor_stop().connectTo(motorCmd_out);
		controller.motor_stop().setProperty("match", 0);
		controller.motor_up().connectTo(motorCmd_out);
		controller.motor_up().setProperty("match", 1);
		
		///////////////SDF MODEL////////////////////
		WindowModel winModel = new WindowModel("Window SDF model");
				
		//////////////DE-SDF-IB/////////////////////
		InterfaceBlock windowIB;
		if (args.length == 0) {
			// Proper DE_SDF interface block, with periodic activation and level change detection
			windowIB = deModel.add(new DE_SDF_InterfaceBlock("Window IB", winModel,
	                "initial_update", 1.0,
//	                "initial_update", 0.0,
	                "period", 1.0));
		} else {
			// Dumb DE_SDF interface block, with almost no semantic adaptation (raw Ptolemy behavior)
			windowIB = deModel.add(new DE_SDF_DumbInterfaceBlock("Window IB", winModel));
		}
		Pin motorCmd_in = windowIB.addPin(new Pin("motorCmd_in"));
		Pin windowPos_out = windowIB.addPin(new Pin("windowPos_out"));
		Pin windowEnd_out = windowIB.addPin(new Pin("windowEnd_out"));
		
		motorCmd_in.connectTo(winModel.motor_cmd());
		winModel.window_pos().connectTo(windowPos_out);
		winModel.window_end().connectTo(windowEnd_out);
		
		///////////// DE event display /////////////
		DEPlotter plotter = deModel.add(new DEPlotter("Window position",
				                                      "xmin", -1.0, "xmax", 16.0,
				                                      "ymin", -2.0, "ymax", 6.0,
				                                      "xticks", 1.0, "yticks", 1.0,
				                                      "xscale", 30.0, "yscale", 20.0
				                                      ));
		Pin display_pos = plotter.addPin(new Pin("position"));
		Pin display_end = plotter.addPin(new Pin("end stop"));
		Pin display_cmd = plotter.addPin(new Pin("user cmd"));
		
		//////////////DE RELATIONS//////////////////
		windowPos_out.connectTo(display_pos);
		windowEnd_out.connectTo(display_end);
		userCmd_out.connectTo(userCmd_in);
		userCmd_out.connectTo(display_cmd);
		obstacle_out.connectTo(obstacle_in);
		motorCmd_out.connectTo(motorCmd_in);
		
		DEDelay delay = deModel.add(new DEDelay("end stop delay", "delay", 0.1));
		windowEnd_out.connectTo(delay.input());
		delay.output().connectTo(windowEnd_in);
		
		//////////////Scénario//////////////////////
		Engine exec = new Engine("ModHelX", deModel);

		// Run as fast as possible
		exec.getSolver().addClock(new ASAPClock("ASAP"));

		// Stop after 25 snapshots
		exec.stopAfter(25);

		exec.getSolver().getLogger().setLoggingAllKinds(false);
		exec.getLogger().setLoggingAllKinds(false);

		exec.run();
	}
}
