/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.powerwindow.models;

import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.sdf.SDFMoC;
import fr.supelec.modhelx.demos.atomicblocks.SDFLimitedIntegrator;
import fr.supelec.modhelx.demos.atomicblocks.SDFLimiter;
import fr.supelec.modhelx.demos.atomicblocks.SDFScale;
import fr.supelec.tesl.core.Unit;

/**
 * The model of the mechanical part of the window.
 * This is an SDF model whose inputs and outputs are:
 *   - motor_cmd: the command of the motor, with positive values 
 *                closing the window, negative value opening the value,
 *                and 0 stopping the motor
 *   - window_pos: the position of the window, 0.0 being fully opened, and 5.0 being fully closed
 *   - window_end: the end stops, 0 means that no end stop is reached,
 *                               -1 means fully opened end stop
 *                                1 means fully closed end stop
 * 
 * The demultiplication of the motor and mechanical part of the window is set
 * by the "scale" property of the model. The value of motor_cmd is multiplied
 * by this scale to compute by how much the window moves at each update.
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class WindowModel extends Model<Unit> {
	private final Pin motor_cmd;
	private final Pin window_pos;
	private final Pin window_end;
	
	public WindowModel(String name, Object ... properties) {
		super(name, new SDFMoC("SDF MoC"), "scale", 1.0);
		setProperties(properties);
		this.motor_cmd = addPin(new Pin("motor_cmd"));
		this.window_pos = addPin(new Pin("window_pos"));
		this.window_end = addPin(new Pin("window_end"));
		
		SDFScale scale = add(new SDFScale("Scale", "scale", getProperty("scale", Double.class)));
		
		SDFLimitedIntegrator integ = add(new SDFLimitedIntegrator("Integrator", "lower_limit", 0.0, "upper_limit", 5.0));
		Pin integ_in = integ.addPin(new Pin("integrator_in"));
		Pin integ_out = integ.addPin(new Pin("integrator_out"));
		
		SDFLimiter lim = add(new SDFLimiter("limiter", "low", 0.0, "high", 5.0));
		
		this.motor_cmd.connectTo(scale.input());
		scale.output().connectTo(integ_in);
		integ_out.connectTo(this.window_pos);
		integ_out.connectTo(lim.input());
		lim.output().connectTo(this.window_end);
	}

	public Pin motor_cmd() {
		return this.motor_cmd;
	}

	public Pin window_pos() {
		return this.window_pos;
	}

	public Pin window_end() {
		return this.window_end;
	}
}
