/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.powerwindow.models;

import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.tfsm.FSMState;
import fr.supelec.modhelx.core.mocs.tfsm.TFSMMoC;

/**
 * The controller of the power window.
 * This is a TFSM model, whose inputs and outputs are:
 *   - win_openend: the window reached the low end stop (it is fully opened)
 *   - win_closed: the window reached the high end stop (it is fully closed)
 *   - win_obstacle: the window detected an obstacle
 *   - cmd_up: the user pressed the up button
 *   - cmd_down: the user pressed the down button
 *   - cmd_stop: the user released the button
 *   - motor_up: start the motor to raise the window
 *   - motor_down: start the motor to lower the window
 *   - motor_stop: stop the motor
 *   
 *  This controller has automatic impulse mode: if the user presses the up or down
 *  button and releases it before one unit of time in TFSM, the window will 
 *  automatically close or open until it reaches an end stop.
 *  When in automatic closing mode, if an obstacle is detected, the window is
 *  lowered during 4 TFSM unit of time and then stopped.
 *   
 * @author frederic.boulanger@supelec.fr
 *
 */
public class WindowController extends Model<Double> {
	private final Pin win_opened;
	private final Pin win_closed;
	private final Pin win_obstacle;
	private final Pin cmd_up;
	private final Pin cmd_stop;
	private final Pin cmd_down;
	private final Pin motor_up;
	private final Pin motor_stop;
	private final Pin motor_down;
	
	public WindowController(String name, Object ... properties) {
		super(name, new TFSMMoC("New TFSM MoC"), properties);
		this.win_opened = addPin(new Pin("win_opened"));
		this.win_closed = addPin(new Pin("win_closed"));
		this.win_obstacle = addPin(new Pin("win_obstacle"));
		this.cmd_up = addPin(new Pin("cmd_up"));
		this.cmd_stop = addPin(new Pin("cmd_stop"));
		this.cmd_down = addPin(new Pin("cmd_down"));
		this.motor_up = addPin(new Pin("motor_up"));
		this.motor_stop = addPin(new Pin("motor_stop"));
		this.motor_down = addPin(new Pin("motor_down"));
		
		FSMState init = add(new FSMState("Init"));
		((TFSMMoC)getMoC()).setInitialState(init);
		this.setProperty("initTime", 0.0);
//		this.setProperty("initTime", 1.0);
		FSMState stop = add(new FSMState("Stop"));
		FSMState up= add(new FSMState("Up"));
		FSMState upManu= add(new FSMState("Up Manu"));
		FSMState upAuto= add(new FSMState("Up Auto"));
		FSMState down= add(new FSMState("Down"));
		FSMState downManu= add(new FSMState("Down Manu"));
		FSMState downAuto = add(new FSMState("Down Auto"));
		FSMState emergencyDown = add(new FSMState("Emergency Down"));
		
		// Initial transition to set the output of the controller
		init.transitionTo(stop, "", "motor_stop");
		
		stop.transitionTo(up, "cmd_up", "motor_up");
		stop.transitionTo(down, "cmd_down", "motor_down");
		up.transitionTo(upAuto, "cmd_stop", null);
		up.transitionTo(upManu, "(D 1.0)", null);
		up.transitionTo(stop, "win_closed", "motor_stop");
		upAuto.transitionTo(stop, "(win_closed | cmd_stop) | (cmd_up | cmd_down)", "motor_stop");
		upAuto.transitionTo(emergencyDown, "win_obstacle", "motor_down");
		upManu.transitionTo(stop, "(win_closed | cmd_stop)", "motor_stop");
		upManu.transitionTo(emergencyDown, "win_obstacle", "motor_down");
		emergencyDown.transitionTo(stop, "(D 4.0)", "motor_stop");
		down.transitionTo(downManu, "(D 1.0)", null);
		down.transitionTo(downAuto, "cmd_stop", null);
		down.transitionTo(stop, "win_opened", "motor_stop");
		downManu.transitionTo(stop, "(win_opened | cmd_stop)", "motor_stop");
		downAuto.transitionTo(stop, "(win_opened | cmd_stop) | (cmd_up | cmd_down)", "motor_stop");
		
		// This is necessary with the current version of TFSM (auto detection of inputs and outputs does not work)
		getInputPins().add(this.win_opened);
		getInputPins().add(this.win_closed);
		getInputPins().add(this.win_obstacle);
		getInputPins().add(this.cmd_up);
		getInputPins().add(this.cmd_stop);
		getInputPins().add(this.cmd_down);
		
		getOutputPins().add(this.motor_up);
		getOutputPins().add(this.motor_stop);
		getOutputPins().add(this.motor_down);
	}

	public Pin win_opened() {
		return this.win_opened;
	}

	public Pin win_closed() {
		return this.win_closed;
	}

	public Pin win_obstacle() {
		return this.win_obstacle;
	}

	public Pin cmd_up() {
		return this.cmd_up;
	}

	public Pin cmd_stop() {
		return this.cmd_stop;
	}

	public Pin cmd_down() {
		return this.cmd_down;
	}

	public Pin motor_up() {
		return this.motor_up;
	}

	public Pin motor_stop() {
		return this.motor_stop;
	}

	public Pin motor_down() {
		return this.motor_down;
	}
}
