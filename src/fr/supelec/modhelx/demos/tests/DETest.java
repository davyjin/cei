/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.tests;

import javax.swing.JFrame;

import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.mocs.de.DEMoC;
import fr.supelec.modhelx.core.simulation.ASAPClock;
import fr.supelec.modhelx.core.simulation.PeriodicRTClock;
import fr.supelec.modhelx.core.simulation.SwingButtonClock;
import fr.supelec.modhelx.demos.atomicblocks.DERamp;
import fr.supelec.modhelx.demos.atomicblocks.DETextDisplay;

/**
 * Test a simple DE model with two ramps connected to two displays.
 * The execution is driven by a Swing button and a clock of period 2s.
 * It can also run as fast as possible by setting RunASAP to true.
 * 
 * The expected output is:
 * 
 * input = 0 @ time 0.5
 * input = 1 @ time 1.5
 * input = 0 @ time 2.0
 * input = 2 @ time 2.5
 * input = 1 @ time 3.0
 * input = 3 @ time 3.5
 * input = 2 @ time 4.0
 * input = 4 @ time 4.5
 * input = 3 @ time 5.0
 * input = 5 @ time 5.5
 * 
 */
public class DETest {
	// Run as fast as possible or synchronize with a Swing button?
	private static final boolean RunASAP = false;

	public static void main(String[] args) {
		// Build a model with a Ramp connected to a TextDisplay
		Model<Double> root = new Model<Double>("DETest", new DEMoC());

		DERamp c1 = root.add(new DERamp("source1", "inittime", 2.0));
		DETextDisplay d1 = root.add(new DETextDisplay("sink1"));
		c1.output().connectTo(d1.input());

		DERamp c2 = root.add(new DERamp("source2", "inittime", 0.5));
		DETextDisplay d2 = root.add(new DETextDisplay("sink2"));
		c2.output().connectTo(d2.input());
		
		Engine exec = new Engine("ModHelX", root);

		JFrame window;
		if (RunASAP) {
			// Run as fast as possible
			exec.getSolver().addClock(new ASAPClock("ASAP"));
		} else {
			// Run a snapshot each time a button is clicked
			SwingButtonClock btnClock = new SwingButtonClock("SwingButton", "Next");
			exec.getSolver().addClock(btnClock);
			window = new JFrame("ModHel'X simulation control");
			window.getContentPane().add(btnClock.getButton());
			window.setVisible(true);
			window.pack();
			// or every 3 seconds, whichever comes first
			exec.getSolver().addClock(new PeriodicRTClock("Second", 2000, 0));
		}
		
		// Stop after 10 snapshots
		exec.stopAfter(10);

		exec.getSolver().getLogger().setLoggingAllKinds(false);
		exec.getLogger().setLoggingAllKinds(false);
		
		exec.run();
		
		if (!RunASAP) {
			window.dispose();
		}
	}

}
