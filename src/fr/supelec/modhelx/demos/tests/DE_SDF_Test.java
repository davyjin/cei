/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.tests;

import javax.swing.JFrame;

import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.DEMoC;
import fr.supelec.modhelx.core.mocs.sdf.SDFMoC;
import fr.supelec.modhelx.core.simulation.PeriodicRTClock;
import fr.supelec.modhelx.core.simulation.SwingButtonClock;
import fr.supelec.modhelx.demos.atomicblocks.DERamp;
import fr.supelec.modhelx.demos.atomicblocks.DETextDisplay;
import fr.supelec.modhelx.demos.atomicblocks.SDFAdder;
import fr.supelec.modhelx.demos.semantic_adaptation.DE_SDF_InterfaceBlock;
import fr.supelec.tesl.core.Unit;

/*@
 * Test of the semantic adaptation between DE and SDF.
 * Two ramps in DE produce increasing values starting from 0 and increasing by 1 with a period of 1 time unit.
 * The first ramp starts producing values at time 2.0, the second one at time 1.0.
 * These ramps feed an SDF model which contains an adder.
 * The output of the SDF model is connected to a DE text display.
 * The SDF model is updated periodically every 2.0 unit of DE time, starting at time 2.0.
 * Its inputs are the most recent value of the DE inputs, newer values replacing former ones.
 * 
 * The SDF model is therefore updated at times 2.0, 4.0, 6.0 ... with inputs 0 and 1, 2 and 3, 4 and 9 ...
 * 
 * The execution of the model is driven by two clocks: a periodic clock of period 1.5 seconds, 
 * and a clock which ticks each time the user presses a button in a window.
 *  
 * Time     0.0  1.0  2.0  3.0  4.0  5.0  6.0  7.0  8.0  9.0  10.0
 * ramp1  ---+----+----+----+----+----+----+----+----+----+----+
 *                     0    1    2    3    4    5    6    7    8
 *     
 * ramp2  ---+----+----+----+----+----+----+----+----+----+----+
 *                0    1    2    3    4    5    6    7    8    9
 *               
 * output ---+----+----+----+----+----+----+----+----+----+----+
 *                     1         5         9         13        17
 *      
 * The expected output of this model is :
 * 
 * input = 1.0 @ time 2.0
 * input = 5.0 @ time 4.0
 * input = 9.0 @ time 6.0
 * input = 13.0 @ time 8.0
 * input = 17.0 @ time 10.0
 * 
 */
public class DE_SDF_Test {

	public static void main(String[] args) {
		Model<Double> root = new Model<Double>("ConcurrentDETest", new DEMoC());

		// DE sources
		DERamp c1 = root.add(new DERamp("source1",
				                        "init", 0, "increment", 1,
				                        "inittime", 2.0, "period", 1.0));
		DERamp c2 = root.add(new DERamp("source2",
                                        "init", 0, "increment", 1,
                                        "inittime", 1.0, "period", 1.0));

		// DE sink
		DETextDisplay d1 = root.add(new DETextDisplay("sink1"));

		// SDF Model and interface block
		Model<Unit> sdf_model = new Model<Unit>("SDF internal model", new SDFMoC("SDF MoC"));
		
		Pin sdfin1 = sdf_model.addPin(new Pin("SDF in 1"));
		Pin sdfin2 = sdf_model.addPin(new Pin("SDF in 2"));
		Pin sdfout = sdf_model.addPin(new Pin("SDF out"));
				
		SDFAdder adder = sdf_model.add(new SDFAdder("Adder"));

		sdfin1.connectTo(adder.input(1));
		sdfin2.connectTo(adder.input(2));
		adder.output().connectTo(sdfout);
		
		DE_SDF_InterfaceBlock intfBlock = root.add(
				new DE_SDF_InterfaceBlock("DE-SDF interface block", sdf_model,
						                  "initial_update", 2.0,
						                  "period", 2.0)
		);
		Pin dein1 = intfBlock.addPin(new Pin("DE in 1"));
		Pin dein2 = intfBlock.addPin(new Pin("DE in 2"));
		Pin deout = intfBlock.addPin(new Pin("DE out"));

		// Adaptation relations
		dein1.connectTo(sdfin1);
		dein2.connectTo(sdfin2);
		sdfout.connectTo(deout);
		
		// Connections
		c1.output().connectTo(dein1);
		c2.output().connectTo(dein2);
		deout.connectTo(d1.input());
		
		Engine exec = new Engine("ModHelX", root);
		
		// Run a snapshot each time a button is clicked
		SwingButtonClock btnClock = new SwingButtonClock("SwingButton", "Next");
		exec.getSolver().addClock(btnClock);
		// Create a JFrame to host the button of the SwingButton clock
		JFrame window = new JFrame("ModHel'X simulation control");
		window.getContentPane().add(btnClock.getButton());
		window.setVisible(true);
		window.pack();
		// Or run a snapshot each every 1.5 seconds, whichever comes first
		exec.getSolver().addClock(new PeriodicRTClock("Second", 1500, 0));
		
		// Stop after 10 snapshots
		exec.stopAfter(10);

		exec.getSolver().getLogger().setLoggingAllKinds(false);
		exec.getLogger().setLoggingAllKinds(false);
		
		exec.run();
		
		window.dispose();
	}
}
