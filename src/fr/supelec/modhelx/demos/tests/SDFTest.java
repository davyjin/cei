/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.tests;

import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.mocs.sdf.SDFMoC;
import fr.supelec.modhelx.core.simulation.ASAPClock;
import fr.supelec.modhelx.core.simulation.PeriodicRTClock;
import fr.supelec.modhelx.demos.atomicblocks.SDFAdder;
import fr.supelec.modhelx.demos.atomicblocks.SDFRamp;
import fr.supelec.modhelx.demos.atomicblocks.SDFSampleDelay;
import fr.supelec.modhelx.demos.atomicblocks.SDFTextDisplay;
import fr.supelec.tesl.core.Unit;

/**
 * Test a simple model with SDF.
 * The first test exercises a multi-rate model, running as fast as possible.
 * 
 * The second uses a loop and a delay to feed an adder with its output.
 * If called with one argument, the pin rate of the output of the adder is set 
 * so that the model is not valid and an exception should be thrown.
 * This second test is run at the rate of one snapshot every second.
 * 
 * The expected output of the first test (loop free, multirate) is:
 * 
 * input = 1.0 ; 4.0 ; 7.0 ; 10.0 ; 13.0 ; 16.0
 * input = 19.0 ; 22.0 ; 25.0 ; 28.0 ; 31.0 ; 34.0
 * input = 37.0 ; 40.0 ; 43.0 ; 46.0 ; 49.0 ; 52.0
 * input = 55.0 ; 58.0 ; 61.0 ; 64.0 ; 67.0 ; 70.0
 * input = 73.0 ; 76.0 ; 79.0 ; 82.0 ; 85.0 ; 88.0
 * input = 91.0 ; 94.0 ; 97.0 ; 100.0 ; 103.0 ; 106.0
 * input = 109.0 ; 112.0 ; 115.0 ; 118.0 ; 121.0 ; 124.0
 * input = 127.0 ; 130.0 ; 133.0 ; 136.0 ; 139.0 ; 142.0
 * input = 145.0 ; 148.0 ; 151.0 ; 154.0 ; 157.0 ; 160.0
 * input = 163.0 ; 166.0 ; 169.0 ; 172.0 ; 175.0 ; 178.0
 * 
 * Since the first ramp produces 2 token at each update, and the display consumes 3 tokens 
 * at each update, the minimal activation sequence of the blocks is ramp1 x3, ramp2 x6, adder x6, display x2
 * therefore, the display has 6 data samples to display at the end of each snapshot.
 * 
 * 
 * The expected output of the second test (loop with a delay) is:
 * 
 * input = 1.0
 * input = 2.0
 * input = 4.0
 * input = 7.0
 * input = 11.0
 * input = 16.0
 * input = 22.0
 * input = 29.0
 * input = 37.0
 * input = 46.0
 * 
 */
public class SDFTest {
	public static void main(String[] args) {
		test_loopfree(args);
		System.out.println();
		test_loopdelay(args);
	}
	
	/**
	 * This model contains two ramps connected to an adder whose
	 * output is connected to a display.
	 * 
	 * @param args
	 */
	public static void test_loopfree(String[] args) {
		// Build a model with a Ramp connected to a TextDisplay
		Model<Unit> root = new Model<Unit>("SDFTest", new SDFMoC("SDF"));

		SDFRamp r1 = root.add(new SDFRamp("source 1", "init", 0));
		SDFRamp r2 = root.add(new SDFRamp("source 2", "init", 1, "increment", 2));
		SDFAdder a = root.add(new SDFAdder("adder"));
		SDFTextDisplay d = root.add(new SDFTextDisplay("sink"));

		r1.output().connectTo(a.input(1));
		r2.output().connectTo(a.input(2));
		a.output().connectTo(d.input());
		
		// Test multi-rate
		SDFMoC.setPinRate(d.input(), 3);
		SDFMoC.setPinRate(r1.output(), 2);
		
		Engine exec = new Engine("ModHelX", root);
		
		// Run as fast as possible
		exec.getSolver().addClock(new ASAPClock("ASAP"));
		
		
		// Stop after 10 snapshots
		exec.stopAfter(10);

		exec.getSolver().getLogger().setLoggingAllKinds(false);
		exec.getLogger().setLoggingAllKinds(false);
		
		exec.run();
	}
	
	public static void test_loopdelay(String[] args) {
		// Build a model with a Ramp connected to a TextDisplay
		Model<Unit> root = new Model<Unit>("SDFTest", new SDFMoC("SDF"));

		SDFRamp ramp = root.add(new SDFRamp("source 1", "init", 0, "increment", 1));
		SDFAdder adder = root.add(new SDFAdder("adder"));
		SDFSampleDelay delay = root.add(new SDFSampleDelay("delay", "init", 1));
		SDFTextDisplay disp = root.add(new SDFTextDisplay("sink"));

		ramp.output().connectTo(adder.input(1));
		adder.output().connectTo(delay.input());
		delay.output().connectTo(adder.input(2));
		adder.output().connectTo(disp.input());

		// If called with at least one argument, set the pin rate of the adder output to 2.
		// This should throw an exception because the balance equations have no solution
		if (args.length > 0) {
			SDFMoC.setPinRate(adder.output(), 2);
		}
		
		Engine exec = new Engine("ModHelX", root);
		
		// Run a snapshot per second
		exec.getSolver().addClock(new PeriodicRTClock("Second", 1000, 0));
		
		// Stop after 10 snapshots
		exec.stopAfter(10);

		exec.getSolver().getLogger().setLoggingAllKinds(false);
		exec.getLogger().setLoggingAllKinds(false);
		
		exec.run();
	}

}
