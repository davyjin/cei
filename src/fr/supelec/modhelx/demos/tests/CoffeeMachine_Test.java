/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.tests;

import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.DEMoC;
import fr.supelec.modhelx.core.mocs.tfsm.FSMState;
import fr.supelec.modhelx.core.mocs.tfsm.TFSMMoC;
import fr.supelec.modhelx.core.simulation.ASAPClock;
import fr.supelec.modhelx.demos.atomicblocks.DEScenario;
import fr.supelec.modhelx.demos.atomicblocks.DETextDisplay;
import fr.supelec.modhelx.demos.semantic_adaptation.DE_TFSM_InterfaceBlock;

/**
 * A DE model of a user and a TFSM coffee machine.
 * The user is modeled by a DEScenario. He inserts a coin at time 1.0 and pushes the coffee button at time 3.0.
 * The machine becomes ready when the user inserts a coin. It goes into preparing mode when he presses the coffee button.
 * 4 TFSM units of time later, the machine delivers the coffee.
 * 
 * Because of the time scale relation between DE and TFSM (time in TFSM is 2 times the time in DE plus 1), the
 * coin is inserted at 2*1+1 = 3 in TFSM, the coffee button is pressed at 2*3+1 = 7 in TFSM time.
 * 4 TFSM time units later, the coffee is served at 11 in TFSM time, which is 5 in DE time.
 * 
 * By setting the "verbose" property of the TFSM model to true, you can display messages when states are entered.
 * 
 * The expected output when running this model is:
 * 
 * coin = true @ time 1.0
 * preparing = true @ time 3.0
 * coffee = true @ time 3.0
 * served = true @ time 5.0
 *
 * @author frederic.boulanger@supelec.fr
 *
 */
public class CoffeeMachine_Test {
	public static void main(String[] args) {
		// ///////////////DE MODEL///////////////////
		Model<Double> deModel = new Model<Double>("DeModel", new DEMoC());

		DEScenario user = deModel.add(new DEScenario("user", "scenario", "coin=(Boolean)true@1.0;coffee=(Boolean)true@3.0;"));
		Pin coin = user.addPin(new Pin("coin"));
		Pin coffee = user.addPin(new Pin("coffee"));

		DETextDisplay display = deModel.add(new DETextDisplay("display"));
		Pin displayCoin = display.addPin(new Pin("coin"));
		Pin displayCoffee = display.addPin(new Pin("coffee"));
		Pin receiveCoffee = display.addPin(new Pin("served"));
		Pin receivePrep = display.addPin(new Pin("preparing"));

		// ///////////////TFSM MODEL////////////////////////
		Model<Double> tfsmModel = new Model<Double>("TFSM", new TFSMMoC("tfsmMoc"), "verbose", false);

		FSMState idle = tfsmModel.add(new FSMState("Idle"));
		((TFSMMoC)tfsmModel.getMoC()).setInitialState(idle);
		FSMState ready = tfsmModel.add(new FSMState("Ready"));
		FSMState prep = tfsmModel.add(new FSMState("Prep"));

		Pin inMcoin = tfsmModel.addPin(new Pin("evtCoin"));
		Pin inMcoffee = tfsmModel.addPin(new Pin("evtCoffee"));
		Pin outMprep = tfsmModel.addPin(new Pin("evtPrep"));
		Pin outMserved = tfsmModel.addPin(new Pin("evtServed"));

		// Necessary with the current implementation of TFSM (automatic input detection does not work)
		(tfsmModel.getInputPins()).add(inMcoin);
		(tfsmModel.getInputPins()).add(inMcoffee);
		(tfsmModel.getOutputPins()).add(outMprep);
		(tfsmModel.getOutputPins()).add(outMserved);

		// ///TFSM RELATIONS///////
		idle.transitionTo(ready, "evtCoin", null);
		ready.transitionTo(prep, "evtCoffee", "evtPrep");
		prep.transitionTo(idle, "(D 4.0)", "evtServed");

		// /////////////////////DE TFSM IB///////////////////////////////////

		DE_TFSM_InterfaceBlock ib = deModel.add(new DE_TFSM_InterfaceBlock("de_tfsm_ib", tfsmModel, "a", 2.0, "b", 1.0));

		Pin inIBcoin = ib.addPin(new Pin("inIBcoin"));
		Pin inIBcoffee = ib.addPin(new Pin("inIBcoffee"));
		Pin outIBprep = ib.addPin(new Pin("outIBprep"));
		Pin outIBserved = ib.addPin(new Pin("outIBserved"));

		// /////////////// IB RELATIONS ///////////////////
		inIBcoin.connectTo(inMcoin);
		inIBcoffee.connectTo(inMcoffee);
		outMprep.connectTo(outIBprep);
		outMprep.setProperty("match", true);
		outMserved.connectTo(outIBserved);
		outMserved.setProperty("match", true);

		// ////////////////DE RELATIONS////////////////////////////////////
		coin.connectTo(inIBcoin);
		coin.connectTo(displayCoin);
		coffee.connectTo(inIBcoffee);
		coffee.connectTo(displayCoffee);
		outIBserved.connectTo(receiveCoffee);
		outIBprep.connectTo(receivePrep);

		// //////////////////////////////////////////////////////////////////

		Engine exec = new Engine("ModHelX", deModel);
		// Run as fast as possible
		exec.getSolver().addClock(new ASAPClock("ASAP"));
		// Stop after 20 snapshots
		exec.stopAfter(100);

		exec.getSolver().getLogger().setLoggingAllKinds(false);
		exec.getLogger().setLoggingAllKinds(false);

		exec.run();
	}
}


