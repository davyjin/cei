/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.tests;

//import modhelxs.semantic_adaptation.DE_SDF_LinInterpolator;
import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.DEMoC;
import fr.supelec.modhelx.core.mocs.sdf.SDFMoC;
import fr.supelec.modhelx.core.simulation.ASAPClock;
import fr.supelec.modhelx.core.simulation.PeriodicRTClock;
import fr.supelec.modhelx.demos.atomicblocks.DEConst;
import fr.supelec.modhelx.demos.atomicblocks.DETextDisplay;
import fr.supelec.modhelx.demos.atomicblocks.SDFIntegrator;
import fr.supelec.modhelx.demos.semantic_adaptation.DE_SDF_LinInterpolator;
import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.TagRelation;
import fr.supelec.tesl.core.Unit;

/**
 * This model uses a special interface block (IB) between DE and SDF that can react
 * to threshold crossing on the outputs of its internal SDF model.
 * For each output with a "threshold" property, the IB compares the current and previous
 * values produced by the internal model to the threshold. If the threshold was crossed
 * from below to above, an event with value +1 is produced. If the threshold was crossed
 * from above to below, an event with value -1 is produced.
 * If the output also has a "precision" property, it gives the precision required on the 
 * date of the event. So, if the threshold was crossed, and the time elapsed between the 
 * previous update (at t) and the current one (at t') is greater than the precision, the 
 * snapshot is not validated, and a tick is added to the control clock of the IB to make 
 * the next update occur at a time which is linearly interpolated. Since the snapshot has 
 * not been validated, this next update will occur during the same snapshot. 
 * This means that the DE time will change. Since we cannot update the SDF model at any time, 
 * the IB computes the current value of the outputs by linear interpolation between the 
 * previous and current values.
 * 
 * The expected output of this model is:
 * 
 * ## Update at 2.0
 * disp in 2 = 2.0 @ time 2.0
 * ## Update at 3.0
 * disp in 2 = 4.0 @ time 3.0
 * ## Update at 4.0
 * # Interpolation at 3.5
 * disp in 1 = 1.0 @ time 3.5
 * disp in 2 = 5.0 @ time 3.5
 * ## Update at 4.0
 * disp in 2 = 6.0 @ time 4.0
 * ## Update at 5.0
 * disp in 2 = 8.0 @ time 5.0
 * ## Update at 6.0
 * disp in 2 = 10.0 @ time 6.0
 * ## Update at 7.0
 * disp in 2 = 12.0 @ time 7.0
 * ## Update at 8.0
 * disp in 2 = 14.0 @ time 8.0
 * ## Update at 9.0
 * disp in 2 = 16.0 @ time 9.0
 *  
 * @author frederic.boulanger@supelec.fr
 */
public class TimedValidationTest {
	// If called with at least one argument, the simulation is synchronized with real time
	// and this raises an exception when the DE-SDF interface block interpolates during 
	// the snapshot at 4.0 and sets the time back to 3.5
	// If called without argument, the simulation runs as fast as possible
	// and there is no tag relation between the DE clock and real-time.
	// Therefore, the DE-SDF interface block can backtrack and reset the time without
	// any constraint.
	
	public static void main(String[] args) {
		// Root DE model
		Model<Double> root = new Model<Double>("TimedValidationTest", new DEMoC());
		
		// DE blocks
		DEConst constant = root.add(new DEConst("constant", "value", 2.0, "inittime", 1.0, "period", 1.0));
		Pin src = constant.addPin(new Pin("const_out"));
		
		DETextDisplay display = root.add(new DETextDisplay("display"));
		Pin dst1 = display.addPin(new Pin("disp in 1"));
		Pin dst2 = display.addPin(new Pin("disp in 2"));
		
		// SDF submodel with interface block
		Model<Unit> sdf_model = new Model<Unit>("SDF internal model", new SDFMoC("SDF MoC"));
		SDFIntegrator integrator = sdf_model.add(new SDFIntegrator("Integrator"));
		Pin integrator_in = integrator.addPin(new Pin("Integrator input"));
		Pin integrator_out = integrator.addPin(new Pin("Integrator output"));
		
		Pin sdfin = sdf_model.addPin(new Pin("SDF in"));
		Pin sdfout = sdf_model.addPin(new Pin("SDF out"));
				
		sdfin.connectTo(integrator_in);
		integrator_out.connectTo(sdfout);
		
		// Interface Block
		DE_SDF_LinInterpolator intfBlock = root.add(
				new DE_SDF_LinInterpolator("DE-SDF interface block", sdf_model,
						                   "initial_update", 2.0,
						                   "update_period", 1.0)
		);
		Pin dein = intfBlock.addPin(new Pin("DE in"));
		Pin dethres = intfBlock.addPin(new Pin("DE thres out",
				              "threshold", 5,   // Produce an event when the output crosses this value
				              "precision", 0.1  // Time precision for the event
		              )
		);
		Pin deout = intfBlock.addPin(new Pin("DE out"));
		
		// Adaptation relations
		dein.connectTo(sdfin);
		sdfout.connectTo(dethres);
		sdfout.connectTo(deout);
				
		// Connections
		src.connectTo(dein);
		dethres.connectTo(dst1);
		deout.connectTo(dst2);
		
		Engine exec = new Engine("ModHelX", root);
		
		// If called with at least one argument, synchronize with real time
		if (args.length > 0) {
			// This should raise an exception when the DE-SDF interface block backtracks 
			// (it sets time in the past, which is now impossible because time is set by the driving real-time clock) 

			// Synchronize the simulation with a RealTime clock of period 1s starting at date 0
			Clock<Long> rtclock = new PeriodicRTClock("Second", 1000, 0);
			exec.getSolver().addClock(rtclock);
			// Bind DE time to advance by 1.0 when the system RT clock advances by 1000 (1.0s for 1000 milliseconds)
			exec.getSolver().addTagRelation(new TagRelation<Long, Double>(rtclock, root.getMoC().getClock()) {
				@Override
				public Double directConversion(Long tag) {
					return tag.doubleValue()/1000;
				}

				@Override
				public Long reverseConversion(Double tag) {
					return 1000*tag.longValue();
				}
			});
		} else {
			// Let the simulation run as fast as possible, freely with regard to real time
			exec.getSolver().addClock(new ASAPClock("ASAP"));
		}
		
		// Stop after 10 snapshots
		exec.stopAfter(10);

		exec.getSolver().getLogger().setLoggingAllKinds(false);
		exec.getLogger().setLoggingAllKinds(false);
		
		exec.run();
	}
}
