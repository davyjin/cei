/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.tests;

import fr.supelec.modhelx.core.abstractsemantics.Engine;
import fr.supelec.modhelx.core.abstractsemantics.Model;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.tfsm.FSMState;
import fr.supelec.modhelx.core.mocs.tfsm.TFSMMoC;
import fr.supelec.modhelx.core.simulation.ASAPClock;

/**
 * A simple TFSM model with no inputs (to be run as a root model).
 * This state machine has an epsilon transition from its initial state "init"
 * to state "A", a timed transition with delay 1.0 from "A" to "B", 
 * a timed transition with delay 2.0 from "B" to "C", and a timed transition
 * with delay 3.0 from "C" back to "A". We run it for only 10 snapshots.
 * 
 * The expected output of this model (thanks to the "verbose" property) is:
 * 
 * TFSM entering state init @0.0
 * TFSM entering state A @0.0
 * TFSM entering state B @1.0
 * TFSM entering state C @3.0
 * TFSM entering state A @6.0
 * TFSM entering state B @7.0
 * TFSM entering state C @9.0
 * TFSM entering state A @12.0
 * TFSM entering state B @13.0
 * TFSM entering state C @15.0
 * TFSM entering state A @18.0
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class TFSM_Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		////////////////TFSM MODEL/////////////////////////
		Model<Double> tfsmModel = new Model<Double>("TFSM", new TFSMMoC("tfsmMoc"));

		FSMState init = tfsmModel.add(new FSMState("init"));
		((TFSMMoC)tfsmModel.getMoC()).setInitialState(init);
		FSMState A = tfsmModel.add(new FSMState("A"));
		FSMState B = tfsmModel.add(new FSMState("B"));		
		FSMState C = tfsmModel.add(new FSMState("C"));
		
		Pin inMx = tfsmModel.addPin(new Pin("inMx"));
		Pin inMy = tfsmModel.addPin(new Pin("inMy"));
	
		Pin outMz = tfsmModel.addPin(new Pin("outMz"));
		Pin outMw = tfsmModel.addPin(new Pin("outMw"));

		init.transitionTo(A, "", null);  // initial epsilon transition
		tfsmModel.setProperty("initTime", 0.0);  // Set date of this initial transition
		A.transitionTo(B, "D 1.0", "outMz");
		B.transitionTo(C, "D 2.0", "outMw");
		C.transitionTo(A, "D 3.0", null);
				
		// This is necessary with the current version of TFSM (auto detection of inputs and outputs does not work)
		(tfsmModel.getInputPins()).add(inMx);
		(tfsmModel.getInputPins()).add(inMy);
		(tfsmModel.getOutputPins()).add(outMz);
		(tfsmModel.getOutputPins()).add(outMw);
		
		tfsmModel.setProperty("verbose", true); // Call the update method of state blocks to display state entry
				
		////////////////////////////////////////////////////////////////////
		Engine exec = new Engine("ModHelX", tfsmModel);
		// Run as fast as possible
		exec.getSolver().addClock(new ASAPClock("ASAP"));
		// Stop after 10 snapshots
		exec.stopAfter(10);

		exec.getSolver().getLogger().setLoggingAllKinds(false);
		exec.getLogger().setLoggingAllKinds(false);

		exec.run();
	}
	
}


