/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.AbstractDEMoC;
import fr.supelec.modhelx.core.state_management.Attribute;
import fr.supelec.modhelx.core.state_management.Parameter;
import fr.supelec.modhelx.core.state_management.TimedManagedBlock;
import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.RoundDouble;

/**
 * A DERamp is a Block which produces an increasing value 
 * on all its pins at each snapshot. The initial value is set by the "init" property (default 0),
 * the increment is set by the "increment" property (default 1). If the "period" property 
 * (default 1.0) is not null, the DERamp will produce outputs periodically, starting at the
 * time set by the "inittime" property (default 0.0). If the "period" property is null, the
 * ramp will only produce its initial value at time "inittime". This block has an output
 * pin, which is returned by its output() method.
 * 
 * @author frederic.boulanger@supelec.fr
 */
public class DERamp extends TimedManagedBlock<Double> {
	// Increment of the value at each snapshot
	private Parameter<Integer> increment;
	// Period of observation
	private Parameter<Double> period;
	// Value produced at this snapshot
	private Attribute<Integer> value;

	// Default is to have one output
	private final Pin output_;

	/** Create a DERamp with a name and properties. */
	public DERamp(String name, Object ... properties) {
		// Default values: init=0, increment=1, period=1.0, inittime=0.0
		super(Double.class, new RoundDouble(), name,
				"init", 0, "increment", 1, "period", 1.0, "inittime", 0.0);
		// Set specific values for properties
		setProperties(properties);
		// Create my output pin
		this.output_ = addPin(new Pin("output"));
	}

	/** Get the default output pin of the ramp. */
	public Pin output() {
		return this.output_;
	}


	@SuppressWarnings("unchecked")
	@Override
	public void doSetup() {
		// Create the Parameters of the ramp
		this.increment = new Parameter<Integer>(Integer.class);
		this.period = new Parameter<Double>(Double.class);
		// Create the Attributes of the ramp
		this.value = new Attribute<Integer>(Integer.class);

		// Initialize the Attributes and Parameter from the value of the properties
		this.initFromProperties("value=init", nextObsTimeAttribute()+"=inittime");

		// This clock shares the same time scale as the DE moc
		observationClock().sameTags((Clock<Double>) getMoC().getClock());
	}

	@Override
	public void doUpdate() {
		// Add a token with the current value to each pin
		for (Pin p: getInterface()) {
			p.putValue(this.value);
		}
	}

	//	@Override
	//	public void doEndOfSnapshot() {
	//		if (this.period.getValue() == 0.0) {
	//			// If I am not periodic, I should not be observed any more
	//			setNextObservationTime(null);
	//		} else {
	//			// If I was updated during this snapshot, schedule my next observation time according to my period
	//			if (wasUpdated()) {
	//				setNextObservationTime(tagSum(((AbstractDEMoC)getMoC()).getCurrentTime(), this.period.getValue()));
	//			}
	//		}
	//		if (this.wasUpdated()) {
	//			// If the ramp was updated, compute the next observation date by adding the period to the current time
	//			double cur_time = ((AbstractDEMoC)getMoC()).getCurrentTime();
	//			this.setNextObservationTime(tagSum(cur_time, this.period.getValue()));
	//			this.value.setValue(this.value.getValue() + this.increment.getValue());
	//		}
	//	}

	@Override
	public void doEndOfSnapshot() {
		if (this.wasUpdated()) {
			this.value.setValue(this.value.getValue() + this.increment.getValue());
			if (this.period.getValue() == 0.0) {
				// If I am not periodic, I should not be observed any more
				setNextObservationTime(null);
			} else {
				// If the ramp was updated and is periodic, compute the next observation date by adding the period to the current time
				double cur_time = ((AbstractDEMoC)getMoC()).getCurrentTime();
				this.setNextObservationTime(tagSum(cur_time, this.period.getValue()));
			}
		}
	}
}
