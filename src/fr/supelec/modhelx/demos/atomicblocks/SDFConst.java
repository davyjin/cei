/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.sdf.SDFMoC;
import fr.supelec.modhelx.core.state_management.Attribute;
import fr.supelec.modhelx.core.state_management.PlainManagedBlock;

/**
 * An SDFConst is an SDF Block which produces the same value on all
 * its Pins at each update. This value is set by the "value" property (default 0).
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class SDFConst extends PlainManagedBlock {
	private Attribute<Integer> value_;
	
	public SDFConst(String name, Object ... properties) {
		super(name, "value", 0);
		setProperties(properties);
	}

	@Override
	public void doSetup() {
		this.value_ = new Attribute<Integer>(Integer.class);
		initFromProperties("value_=value");
	}

	@Override
	public void doUpdate() {
		for (Pin p: getInterface()) {
			int rate = SDFMoC.getPinRate(p);
			for (int i = 0; i < rate; i++) {
				p.putValue(this.value_);
			}
		}
	}
}
