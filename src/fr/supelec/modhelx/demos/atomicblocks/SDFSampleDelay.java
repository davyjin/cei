/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import java.util.Collection;

import fr.supelec.modhelx.core.abstractsemantics.AtomicBlock;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.abstractsemantics.Token;
import fr.supelec.modhelx.core.mocs.sdf.SDFMoC;

/**
 * An SDFSampleDelay is an SDF block which delays its input by one sample.
 * At the first update, its produces an initial value on its output.
 * This initial value is set by the "init" property (default value 0).
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class SDFSampleDelay extends AtomicBlock {
	private Object next_value_;
	private Object current_value_;
	private final Pin input_;
	private final Pin output_;
	
	public SDFSampleDelay(String name, Object ... properties) {
		super(name, "init", 0);
		setProperties(properties);
		this.input_ = addPin(new Pin("input"));
		this.output_ = addPin(new Pin("output"));
	}
	
	public Pin input() {
		return this.input_;
	}
	
	public Pin output() {
		return this.output_;
	}
	
	@Override
	public void setup() {
		this.next_value_ = getProperty("init", Object.class);
		for (Pin p : getOutputPins()) {
			SDFMoC.setNumberOfInitialTokens(p, 1);
			p.addToken(new Token(getMoC()).setValue(this.next_value_));
		}
	}

	@Override
	public void reset() {
		this.current_value_ = this.next_value_;
	}
	
	@Override
	public void update() {
		// Only one input pin is supported
		Collection<Pin> inputs = getInputPins();
		if (inputs.size() != 1) {
			throw new RuntimeException("Error: " + getClass()+getName() + " \"" + this.getName() + "\" does not have exactly one input pin");
		}
		Pin in = inputs.iterator().next();
		// Only one token is supported
		Collection<Token> tokens = in.getTokens();
		if (tokens.size() != 1) {
			throw new RuntimeException("Error: " + getClass()+getName() + " \"" + this.getName() + "." + in.getName() + "\" does not have exactly one token");
		}
		this.current_value_ = in.getToken(true).getValue();
		for (Pin p : getOutputPins()) {
			p.addToken(new Token(getMoC()).setValue(this.current_value_));
		}
	}
	
	@Override
	public void endOfSnapshot() {
		this.next_value_ = this.current_value_;
	}
}
