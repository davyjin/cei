/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import fr.supelec.modhelx.core.abstractsemantics.ObservableEntity;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.state_management.PlainManagedBlock;

/**
 * A Probe is a block that notifies a PinListener each time
 * one of its input pins has tokens at the end of the snapshot.
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class Probe extends PlainManagedBlock {
	public static interface PinListener {
		public void setSource(ObservableEntity src);
		public void setup();
		public void pinChanged(Pin p);
		public void wrapup();
	}
	private final PinListener listener_;
	
	public Probe(String name, PinListener listener, Object... properties) {
		super(name, properties);
		this.listener_ = listener;
	}

	@Override
	public void doSetup() {
		for (Pin p : getInputPins()) {
			p.clearTokens();
		}
		this.listener_.setup();
	}

	@Override
	public void doUpdate() {
		// Nothing special to do
	}
	
	@Override
	public void doEndOfSnapshot() {
		for (Pin p : getInputPins()) {
			if (p.hasToken()) {
				this.listener_.pinChanged(p);
			}
		}
	}

	@Override
	public void wrapup() {
		this.listener_.wrapup();
	}
}
