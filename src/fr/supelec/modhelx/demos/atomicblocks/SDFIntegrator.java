/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.sdf.SDFMoC;
import fr.supelec.modhelx.core.state_management.Attribute;
import fr.supelec.modhelx.core.state_management.PlainManagedBlock;

/**
 * An SDFIntegrator sums adds all the values received on its inputs
 * to its internal state and produced this state on all its outputs.
 * The initial state of the integrator is set by the "initState" 
 * property (default 0.0).
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class SDFIntegrator extends PlainManagedBlock {
	protected Attribute<Double> state_;

	public SDFIntegrator(String name, Object ... properties) {
		super(name, "initState", 0.0);
		setProperties(properties);
	}
	
	@Override
	public void doSetup() {
		this.state_ = new Attribute<Double>(Double.class);
		initFromProperties("state_=initState");
	}
	
	@Override
	public void doUpdate() {
		for (Pin p : getInputPins()) {
			int rate = SDFMoC.getPinRate(p);
			for (int i = 0; i < rate; i++) {
				double in = p.readValue(Number.class, true).doubleValue();
				this.state_.setValue(this.state_.getValue() + in);
			}
		}
		for (Pin p : getOutputPins()) {
			int rate = SDFMoC.getPinRate(p);
			for (int i = 0; i < rate; i++) {
				p.putValue(this.state_);
			}
		}

	}
}
