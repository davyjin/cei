/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.AbstractDEMoC;
import fr.supelec.modhelx.core.state_management.ListAttribute;
import fr.supelec.modhelx.core.state_management.Parameter;
import fr.supelec.modhelx.core.state_management.TimedManagedBlock;
import fr.supelec.tesl.core.RoundDouble;

/**
 * A DEDelay is a block for use with the Dicrete Events MoC, which introduces
 * a time delay between occurrences of events on its input and on its output.
 * Each event received on the input at time T is produced on the output at
 * time T+delay. Delay is set through the "delay" property (default value 1.0).
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class DEDelay extends TimedManagedBlock<Double> {
	/** A delayed event to produce on output. */
	private class DelayedOutput {
		public Object value;
		public double time;
		public DelayedOutput(Object value, double time) {
			this.value = value;
			this.time = time;
		}
	}
	/** The time delay. */
	private Parameter<Double> delay_;
	/** The list of delayed events to produce. */
	private ListAttribute<DelayedOutput> delayed_outputs_;
	/** The input and output pins of the Delay. */
	private final Pin input_;
	private final Pin output_;

	public DEDelay(String name, Object ... properties) {
		super(Double.class, new RoundDouble(), name, "delay", 1.0);
		this.input_ = addPin(new Pin(getName() + "_input"));
		this.output_ = addPin(new Pin(getName() + "_output"));
		this.setProperties(properties);
	}
	
	public Pin input() {
		return this.input_;
	}

	public Pin output() {
		return this.output_;
	}
	
	@Override
	public void doSetup() {
		// Use the same time scale as the DE model we are in.
		observationClock().sameTags(((AbstractDEMoC)(getMoC())).getClock());
		this.delay_ = new Parameter<Double>(Double.class);
		this.delayed_outputs_ = new ListAttribute<DelayedOutput>();
		initFromProperties("delay_=delay");
		// The behavior of a delay does not depend instantaneously on the value of its input
		AbstractDEMoC.setNotInstantaneouslyDependent(this.input_);
	}

	@Override
	public void doUpdate() {
		if (this.input_.hasToken()) {
			// If we received an event, store it for delayed production on the output
			this.delayed_outputs_.add(new DelayedOutput(
					                      this.input_.readValue(Object.class, true),
					                      tagSum(((AbstractDEMoC)getMoC()).getCurrentTime(), this.delay_.getValue())
					                 ));
		}
		if (this.delayed_outputs_.size() > 0) {
			// If we have events to produce now, produce them
			DelayedOutput next = this.delayed_outputs_.get(0);
			if (next.time == ((AbstractDEMoC)getMoC()).getCurrentTime()) {
				this.output_.putValue(next.value);
				this.delayed_outputs_.remove(0);
			}
		}
	}

	@Override
	public void doEndOfSnapshot() {
		if (this.delayed_outputs_.size() > 0) {
			// If we have events to produce in the future, set our next 
			// observation time to the date of the first one.
			// Notice that events are necessarily in increasing date order
			// because they are added to the list in the order they are received,
			// and the delay cannot be changed.
			DelayedOutput next = this.delayed_outputs_.get(0);
			setNextObservationTime(next.time);
		} else {
			// If we don't have anything to do in the future, say so.
			setNextObservationTime(null);
		}
	}
}
