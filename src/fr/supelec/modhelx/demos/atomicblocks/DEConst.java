/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.AbstractDEMoC;
import fr.supelec.modhelx.core.state_management.Attribute;
import fr.supelec.modhelx.core.state_management.Parameter;
import fr.supelec.modhelx.core.state_management.TimedManagedBlock;
import fr.supelec.tesl.core.RoundDouble;

/**
 * A DEConst is a Block for the Discrete Events MoC, which produces the same value on all
 * its Pins at each update. The produced value is set by the "value" property (default 0).
 * If the "period" property (default 0.0) is not null, the DEConst produces its value
 * periodically, starting at the time given by the "inittime" property (default 0.0).
 * If the "period" property is null, the value is produced only once at the time given
 * by the "inittime" property. 
 * 
 * @author frederic.boulanger@supelec.fr
 */
public class DEConst extends TimedManagedBlock<Double> {
	/** The value produced by this DEConst. */
	private Attribute<Double> value_;
	/** The period with which the value is produced. */
	private Parameter<Double> period_;

	public DEConst(String name, Object ... properties) {
		super(Double.class, new RoundDouble(), name,
				"value", 0, "period", 0.0, "inittime", 0.0);
		setProperties(properties);
	}

	@Override
	public void doSetup() {
		// Create the attributes and parameters
		this.value_ = new Attribute<Double>(Double.class);
		this.period_ = new Parameter<Double>(Double.class);
		// Initialize attributes and parameter from the properties, with eventual name mappings
		initFromProperties("value_=value", "period_=period", nextObsTimeAttribute()+"=inittime");
		// Use the same time scale for my observation clock and the clock of my MoC
		observationClock().sameTags(((AbstractDEMoC)getMoC()).getClock());
	}
	
	@Override
	public void doUpdate() {
		// Put my value on each of my pins.
		for (Pin p: getInterface()) {
			p.putValue(this.value_);
		}
	}

	@Override
	public void doEndOfSnapshot() {
		if (this.period_.getValue() == 0.0) {
			// If I am not periodic, I should not be observed any more
			setNextObservationTime(null);
		} else {
			// If I was updated during this snapshot, schedule my next observation time according to my period
			if (wasUpdated()) {
				setNextObservationTime(tagSum(((AbstractDEMoC)getMoC()).getCurrentTime(), this.period_.getValue()));
			}
		}
	}
}
