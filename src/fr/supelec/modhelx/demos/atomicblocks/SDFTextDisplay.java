/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import java.util.LinkedList;
import java.util.List;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.state_management.PlainManagedBlock;
import fr.supelec.modhelx.core.state_management.TransientMap;

/**
 * A TextDisplay is a Block which displays the value of
 * all the tokens on each of its pins at each update.
 * 
 * @author frederic.boulanger@supelec.fr
 */
public class SDFTextDisplay extends PlainManagedBlock {
	// Map storing which data is on which pin
	private TransientMap<Pin, List<Object>> data_;
	private final Pin input_;
	
	public SDFTextDisplay(String name) {
		super(name);
		this.input_ = addPin(new Pin("input"));
	}

	public Pin input() {
		return this.input_;
	}
	
	@Override
	public void doSetup() {
		this.data_ = new TransientMap<Pin, List<Object>>();
	}
	
	@Override
	public void doUpdate() {
		for (Pin p: getInterface()) {
			List<Object> cur_list = this.data_.get(p);
			if (cur_list == null) {
				cur_list = new LinkedList<Object>();
			}
			while (p.hasToken()) {
				cur_list.add(p.getToken(true).getValue());
			}
			this.data_.put(p, cur_list);
		}
	}
	
	@Override
	public void doEndOfSnapshot() {
		for (Pin p: this.data_.keySet()) {
			System.out.print(p.getName() + " = ");
			boolean skipsep = true;
			for (Object v : this.data_.get(p)) {
				if (skipsep) {
					skipsep = false;
				} else {
					System.out.print(" ; ");
				}
				System.out.print(v);
			}
			System.out.println();
		}
	}
}
