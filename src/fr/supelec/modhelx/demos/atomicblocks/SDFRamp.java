/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.sdf.SDFMoC;
import fr.supelec.modhelx.core.state_management.Attribute;
import fr.supelec.modhelx.core.state_management.Parameter;
import fr.supelec.modhelx.core.state_management.PlainManagedBlock;

/**
 * A Ramp is a Block which produces an increasing value 
 * on all its Pins at each update. The value is incremented after producing each output 
 * token, so it may be incremented several times during a snapshot in multi-rate models.
 * 
 * @author frederic.boulanger@supelec.fr
 */
public class SDFRamp extends PlainManagedBlock {
	// Current value of the output
	private Attribute<Integer> value_;
	// Increment of the value at each snapshot
	private Parameter<Integer> increment_;
	// The Ramp output pin
	private final Pin output_;
	
	public SDFRamp(String name, Object ... properties) {
		super(name, "init", 0, "increment", 1);
		setProperties(properties);
		this.output_ = addPin(new Pin("output"));
	}

	public Pin output() {
		return this.output_;
	}
	
	@Override
	public void doSetup() {
		this.value_ = new Attribute<Integer>(Integer.class);
		this.increment_ = new Parameter<Integer>(Integer.class);
		initFromProperties("value_=init", "increment_=increment");
	}
	
	@Override
	public void doUpdate() {
		for (Pin p: getInterface()) {
			int rate = SDFMoC.getPinRate(p);
			for (int i = 0; i < rate; i++) {
				p.putValue(this.value_);
				this.value_.setValue(this.value_.getValue() + this.increment_.getValue());
			}
		}
	}
}
