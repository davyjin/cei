/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import java.util.HashMap;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.simulation.EventFeederClock;
import fr.supelec.modhelx.core.state_management.NotManagedAttribute;
import fr.supelec.modhelx.core.state_management.TimedManagedBlock;
import fr.supelec.tesl.core.Clock;
import fr.supelec.tesl.core.RoundDouble;

/**
 * An EventFeeder can be used to feed events into a simulation
 * from external sources like a GUI.
 * To feed an event into the simulation, call the feed(pin, value)
 * method of the feeder.
 * An EventFeeder has a driving clock that will trigger a snapshot 
 * when an event is fed.
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class EventFeeder extends TimedManagedBlock<Double> {
	@NotManagedAttribute
	private HashMap<Pin, Object> values_;
	@NotManagedAttribute
	private HashMap<Pin, Object> snap_values_;
	@NotManagedAttribute
	private EventFeederClock clock_;
	
	public EventFeeder(String name, Object ... properties) {
		super(Double.class, new RoundDouble(), name, properties);
	}

	@Override
	public synchronized void doStartOfSnapshot() {
		// Events that will be processed during this snapshot
		this.snap_values_ = new HashMap<Pin, Object>(this.values_);
		this.values_.clear();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void doSetup() {
		this.values_ = new HashMap<Pin, Object>();
		this.clock_ = new EventFeederClock(getName()+" clock");
		getSolver().addClock(this.clock_);
		this.clock_.implies(observationClock());
		observationClock().sameTags((Clock<Double>) getMoC().getClock());
	}

	@Override
	public void doUpdate() {
		for (Pin p : getOutputPins()) {
			if (this.snap_values_.containsKey(p)) {
				p.putValue(this.snap_values_.get(p));
			}
		}
	}

	@Override
	public void doEndOfSnapshot() {
		// These events have been processed
		this.snap_values_.clear();
	}
	
	public synchronized void feed(Pin p, Object v) {
		this.values_.put(p, v);
		this.clock_.eventOccurred();
	}

}
