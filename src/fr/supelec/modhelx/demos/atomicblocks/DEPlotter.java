/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.supelec.modhelx.core.abstractsemantics.NamedEntity;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.AbstractDEMoC;
import fr.supelec.modhelx.core.state_management.Attribute;
import fr.supelec.modhelx.core.state_management.PlainManagedBlock;
import fr.supelec.modhelx.core.state_management.TransientMap;
/**
 * A DEPlotter is a Block for the DE MoC which displays the value
 * received on each of its Pins in a graphical window.
 * The properties used by the DEPlotter are:
 *   - "xscale": the horizontal scale, in pixels per time unit
 *   - "yscale": the vertical scale, in pixels per unit
 *   - "xmin" and "xmax": the horizontal range
 *   - "ymin" and "ymax": the vertical range
 *   - "xticks" and "yticks": interval between horizontal/vertical ticks
 *   - "dotw" and "doth": width and height of the oval dots for each drawn value
 *   - "connect": true for connecting points, false for just drawing points
 *   - "manhattan": when connecting point, true for Manhattan style connection, false for straight connection
 * 
 * @author frederic.boulanger@supelec.fr
 */
public class DEPlotter extends PlainManagedBlock {
	@SuppressWarnings("serial")
	private static class PlotterWindow extends JFrame {
		private static class Plotter extends JComponent {
			static final Color colors[] = {
				Color.RED,
				Color.BLUE,
				Color.MAGENTA,
				Color.ORANGE,
				Color.PINK,
				Color.YELLOW,
				Color.GREEN
			};
			private Map<Pin, List<PlotPoint>> plots_;
			HashMap<Pin, Color> sigColors;
			final int xoffset = 40;
			final int yoffset = 30;
			int dot_width = 3;
			int dot_height = 3;
			final int arrow_length = 6;
			final int arrow_width = 3;
			double xscale = 20.0;
			double yscale = 20.0;
			double x_ticks = 1.0;
			double y_ticks = 1.0;
			double xmin = -10.0;
			double xmax = 10;
			double ymin = -10.0;
			double ymax = 10;
			boolean connect = true;
			boolean manhattan = true;

			class PlotPoint {
				public double time;
				public double value;
				public PlotPoint(double time, double value) {
					this.time = time;
					this.value = value;
				}
			}

			public Plotter() {
				setPreferredSize(new Dimension(300,200));
				this.plots_ = new HashMap<Pin, List<PlotPoint>>();
				this.sigColors = new HashMap<Pin, Color>();
			}

			public void setup(NamedEntity propertiesHolder, Collection<Pin> signals) {
				this.xscale = propertiesHolder.getProperty("xscale", Double.class);
				this.yscale = propertiesHolder.getProperty("yscale", Double.class);
				this.xmin = propertiesHolder.getProperty("xmin", Double.class);
				this.ymin = propertiesHolder.getProperty("ymin", Double.class);
				this.xmax = propertiesHolder.getProperty("xmax", Double.class);
				this.ymax = propertiesHolder.getProperty("ymax", Double.class);
				this.dot_width = propertiesHolder.getProperty("dotw", Integer.class);
				this.dot_height = propertiesHolder.getProperty("doth", Integer.class);
				this.x_ticks = propertiesHolder.getProperty("xticks", Double.class);
				this.y_ticks = propertiesHolder.getProperty("yticks", Double.class);
				this.connect = propertiesHolder.getProperty("connect", Boolean.class);
				this.manhattan = propertiesHolder.getProperty("manhattan", Boolean.class);
				int col_idx = 0;
				for (Pin p : signals) {
					this.sigColors.put(p, colors[col_idx]);
					col_idx = (col_idx + 1) % colors.length;
				}
				setPreferredSize(new Dimension((int) (2*this.xoffset + (this.xmax - this.xmin)*this.xscale),
						(int) (2*this.yoffset + (this.ymax - this.ymin)*this.yscale)));
				getParent().invalidate();
			}

			public synchronized void addPlotPoint(Pin p, double time, double value) {
				List<PlotPoint> l = this.plots_.get(p);
				if (l == null) {
					l = new LinkedList<PlotPoint>();
					this.plots_.put(p, l);
				}
				l.add(new PlotPoint(time, value));
			}

			@Override
			public synchronized void paintComponent(Graphics g) {
				super.paintComponent(g);
				Graphics tmp = g.create();
				Dimension size = getSize();
				// Draw the axes
				tmp.setColor(Color.BLACK);
				Font font = Font.decode("SANS-PLAIN-9");
				tmp.setFont(font);
				FontMetrics metrics = tmp.getFontMetrics();

				// Ticks on x axis + vertical lines
				int x = this.xoffset;
				double v = this.xmin;
				while (x < size.width - this.xoffset - this.dot_width) {
					tmp.drawLine(x, size.height - this.yoffset, x, size.height - this.yoffset + this.dot_height);
					tmp.setColor(Color.LIGHT_GRAY);
					tmp.drawLine(x, size.height - this.yoffset, x, this.yoffset + this.dot_height);
					tmp.setColor(Color.BLACK);
					String label = Double.toString(v);
					Rectangle2D bounds = metrics.getStringBounds(label, tmp);
					tmp.drawString(label, x, (int) (size.height - this.yoffset + this.dot_height + bounds.getHeight()));
					v += this.x_ticks;
					x = (int) Math.round(this.xoffset + (v - this.xmin) * this.xscale);
				}
				// Ticks on y axis + horizontal lines
				int y = size.height - this.yoffset;
				v = this.ymin;
				while (y > this.yoffset + this.dot_height) {
					tmp.drawLine(this.xoffset, y, this.xoffset - this.dot_width, y);
					tmp.setColor(Color.LIGHT_GRAY);
					tmp.drawLine(this.xoffset, y, size.width - this.xoffset - this.dot_width, y);
					tmp.setColor(Color.BLACK);
					String label = Double.toString(v);
					Rectangle2D bounds = metrics.getStringBounds(label, tmp);
					tmp.drawString(label, (int) (this.xoffset - this.dot_width - bounds.getWidth()), y);
					v += this.y_ticks;
					y = size.height - (int) Math.round(this.yoffset + (v - this.ymin) * this.yscale);
				}

				// x axis
				if ((this.ymax >= 0.0) && (this.ymin <= 0.0)) {
					y = size.height - this.yoffset + (int)(this.ymin * this.yscale);
					tmp.drawLine(this.xoffset, y, size.width - this.xoffset, y);
					tmp.drawLine(size.width - this.xoffset, y, size.width - this.xoffset - this.arrow_length, y - this.arrow_width);
					tmp.drawLine(size.width - this.xoffset, y, size.width - this.xoffset - this.arrow_length, y + this.arrow_width);
				}
				// y axis
				if ((this.xmax >= 0.0) && (this.xmin <= 0.0)) {
					x = this.xoffset - (int)(this.xmin * this.xscale);
					tmp.drawLine(x, size.height - this.yoffset, x, this.yoffset);
					tmp.drawLine(x, this.yoffset, x - this.arrow_width, this.yoffset + this.arrow_length);
					tmp.drawLine(x, this.yoffset, x + this.arrow_width, this.yoffset + this.arrow_length);
				}

				// Draw data from each input in a different color 
				int col_idx = 0;
				for (Pin p : this.plots_.keySet()) {
					List<PlotPoint> l = this.plots_.get(p);
					tmp.setColor(this.sigColors.get(p));
					int last_x = 0;
					int last_y = 0;
					boolean first = true;
					for (PlotPoint pp : l) {
						x = (int) Math.round(this.xoffset + this.xscale * (pp.time - this.xmin));
						y = size.height - ((int) Math.round(this.yoffset + this.yscale * (pp.value - this.ymin)));
						if (connect) {
							if (first) {
								first = false;
							} else {
								if (manhattan) {
									tmp.drawLine(last_x, last_y, x, last_y);
									tmp.drawLine(x, last_y, x, y);
								} else {
									tmp.drawLine(last_x, last_y, x, y);
								}
							}
						}
						last_x = x;
						last_y = y;
						tmp.fillOval(x - this.dot_width, y - this.dot_height, 2*this.dot_width, 2*this.dot_height);
					}
					col_idx = (col_idx + 1) % colors.length;
				}
			}
		}

		private static class PlotInfo extends JPanel {
			private Plotter myPlotter_;
			private JPanel legend;
			private JPanel info;
			private JLabel xmin;
			private JLabel xmax;
			private JLabel ymin;
			private JLabel ymax;
			private JLabel xscale;
			private JLabel yscale;

			public PlotInfo(Plotter plotter) {
				super(new BorderLayout());
				this.myPlotter_ = plotter;
				Font font = Font.decode("SANS-PLAIN-9");
				this.legend = new JPanel();
				this.legend.setLayout(new BoxLayout(this.legend, BoxLayout.Y_AXIS));
				add(this.legend, BorderLayout.NORTH);

				this.info = new JPanel();
				this.info.setLayout(new BoxLayout(this.info, BoxLayout.Y_AXIS));
				add(this.info, BorderLayout.SOUTH);
				this.xmin = new JLabel();
				this.xmin.setFont(font);
				this.info.add(this.xmin);
				this.xmax = new JLabel();
				this.xmax.setFont(font);
				this.info.add(this.xmax);
				this.ymin = new JLabel();
				this.ymin.setFont(font);
				this.info.add(this.ymin);
				this.ymax = new JLabel();
				this.ymax.setFont(font);
				this.info.add(this.ymax);
				this.xscale = new JLabel();
				this.xscale.setFont(font);
				this.info.add(this.xscale);
				this.yscale = new JLabel();
				this.yscale.setFont(font);
				this.info.add(this.yscale);
				this.setBorder(BorderFactory.createEmptyBorder(10, 2, 20, 15));
			}

			public void setup() {
				this.xmin.setText("xmin: " + this.myPlotter_.xmin);
				this.xmax.setText("xmax: " + this.myPlotter_.xmax);
				this.ymin.setText("ymin: " + this.myPlotter_.ymin);
				this.ymax.setText("ymax: " + this.myPlotter_.ymax);
				this.xscale.setText("xscale: " + this.myPlotter_.xscale);
				this.yscale.setText("yscale: " + this.myPlotter_.yscale);

				Font font = Font.decode("SANS-PLAIN-9");
				for (Pin p : this.myPlotter_.sigColors.keySet()) {
					JLabel sigLab = new JLabel(p.getName());
					sigLab.setFont(font);
					sigLab.setForeground(this.myPlotter_.sigColors.get(p));
					this.legend.add(sigLab);
				}
			}
		}

		private Plotter plotter_;
		private PlotInfo plotinfo_;

		public PlotterWindow(String name) {
			super(name);
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			this.plotter_ = new Plotter();
			getContentPane().add(this.plotter_, BorderLayout.CENTER);
			this.plotinfo_ = new PlotInfo(this.plotter_);
			getContentPane().add(this.plotinfo_, BorderLayout.EAST);
			pack();
			setVisible(true);
		}

		public void addPlotPoint(Pin p, double time, double value) {
			this.plotter_.addPlotPoint(p, time, value);
		}

		public void setup(NamedEntity propertiesHolder, Collection<Pin> signals) {
			this.plotter_.setup(propertiesHolder, signals);
			this.plotinfo_.setup();
			setSize(getPreferredSize());
		}
	}
	// Map storing which data is on which pin
	private TransientMap<Pin, Number> data_;
	private Attribute<PlotterWindow> window_;

	public DEPlotter(String name, Object ... properties) {
		super(name,
				"title", "DE plot",
				"connect", true,
				"manhattan", true,
				"xmin", -10.0, "xmax", 10.0,
				"ymin", -10.0, "ymax", 10.0,
				"xticks", 1.0, "yticks", 1.0,
				"xscale", 20.0, "yscale", 20.0,
				"dotw", 3, "doth", 3);
		setProperties(properties);
	}

	@Override
	public void doSetup() {
		// Create the transient map for storing input data
		this.data_ = new TransientMap<Pin, Number>();
		this.window_ = new Attribute<PlotterWindow>(PlotterWindow.class);
		this.window_.init(new PlotterWindow(getProperty("title", String.class)));
		this.window_.getValue().setup(this, getInputPins());
	}

	@Override
	public void doUpdate() {
		for (Pin p: getInterface()) {
			if (p.hasToken()) {
				// The plotter works only with numbers.
				Number n = p.readValue(Number.class, true);
				this.data_.put(p, n);
			}
		}
	}

	@Override
	public void doEndOfSnapshot() {
		// Display the data received on each pin
		for (Pin p: this.data_.keySet()) {
			//			System.out.println(p.getName() + " = " + this.data_.get(p) + " @ time " + ((AbstractDEMoC)getMoC()).getCurrentTime());
			this.window_.getValue().addPlotPoint(p, ((AbstractDEMoC)getMoC()).getCurrentTime(), this.data_.get(p).doubleValue());
		}
		this.window_.getValue().repaint();
	}
}
