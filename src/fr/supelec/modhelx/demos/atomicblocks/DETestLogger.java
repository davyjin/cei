/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import java.util.Collection;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.abstractsemantics.Token;
import fr.supelec.modhelx.core.state_management.PlainManagedBlock;
import fr.supelec.modhelx.core.state_management.TransientMap;

/**
 * A DETestLogger is a Block which logs the value of
 * each of its Pins at each snapshot. The log is obtained
 * with the getLog() method. 
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class DETestLogger extends PlainManagedBlock {
	// Map storing which data is on which pin
	private TransientMap<Pin, Object> data_;
	// By default we have one pin named "input"
	private final Pin input_;
	private StringBuilder log_;
	
	public DETestLogger(String name) {
		super(name);
		this.input_ = addPin(new Pin("input"));
	}

	public Pin input() {
		return this.input_;
	}
	
	@Override
	public void doSetup() {
		// Create the transient map for storing input data
		this.data_ = new TransientMap<Pin, Object>();
		// Get a fresh StringBuilder
		this.log_ = new StringBuilder();
	}

	@Override
	public void doUpdate() {
		for (Pin p: getInterface()) {
			// For each pin, get the available tokens
			Collection<Token> toks = p.getTokens();
			if (toks.size() > 0) {
				if (toks.size() > 1) {
					// If there is more than one token, create an array for the data
					Object[] data = toks.toArray();
					int i = 0;
					for (Token tok : toks) {
						// and fill it with the tokens values
						data[i++] = tok.getValue();
					}
					this.data_.put(p, data);
				} else {
					// If there is only one token, get its value
					this.data_.put(p, toks.iterator().next().getValue());
				}
			}
			// Consume the tokens
			p.clearTokens();
		}
	}
	
	
	@Override
	public void doEndOfSnapshot() {
		// Display the data received on each pin
		for (Pin p: this.data_.keySet()) {
			this.log_.append(p.getName() + " = " + this.data_.get(p) + " @ time " + getMoC().getCurrentTime());
			this.log_.append(System.getProperty("line.separator"));
		}
	}
	
	public String getLog() {
		return this.log_.toString();
	}
}
