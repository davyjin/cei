/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.state_management.Parameter;
import fr.supelec.modhelx.core.state_management.PlainManagedBlock;

/**
 * An SDFScale is an SDF block which scales its input.
 * Each value read on the input is multiplied by the scale
 * and put on the output. The input and output pins must
 * have the same rate. The scale is set by the "scale" property
 * (default value 1.0).
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class SDFScale extends PlainManagedBlock {
	private Parameter<Double> scale;
	private final Pin input;
	private final Pin output;

	public SDFScale(String name, Object ... properties) {
		super(name, "scale", 1.0);
		setProperties(properties);
		this.input = addPin(new Pin(getName()+".input"));
		this.output = addPin(new Pin(getName()+".output"));
	}

	public Pin input() {
		return this.input;
	}

	public Pin output() {
		return this.output;
	}

	@Override
	public void doSetup() {
		this.scale = new Parameter<Double>(Double.class);
		initFromProperties();
	}

	@Override
	public void doUpdate() {
		int rate = this.input.getTokens().size();
		for (int i = 0; i < rate; i++) {
			double val = this.input.readValue(Number.class, true).doubleValue();
			this.output.putValue(val * this.scale.getValue());
		}
	}
}
