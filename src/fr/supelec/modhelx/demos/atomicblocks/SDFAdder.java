/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.sdf.SDFMoC;
import fr.supelec.modhelx.core.state_management.PlainManagedBlock;
import fr.supelec.modhelx.core.state_management.TransientValue;

/**
 * An SDFAdder is an SDF block which computes the sum of the tokens on all its input
 * pins and puts the result on all its output pins.
 * This block creates two inputs and one output, respectively obtained using the
 * input(<1 or 2>) and output() methods.
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class SDFAdder extends PlainManagedBlock {
	private TransientValue<Double> sum_;
	private final Pin[] input_ = new Pin[2];
	private final Pin output_;

	public SDFAdder(String name) {
		super(name);
		this.input_[0] = addPin(new Pin("input1"));
		this.input_[1] = addPin(new Pin("input2"));
		this.output_ = addPin(new Pin("output"));
	}
	
	@Override
	public void doSetup() {
		this.sum_ = new TransientValue<Double>(Double.class);
		this.sum_.init(0.0);
	}
	
	public Pin input(int num) {
		return this.input_[num-1];
	}
	
	public Pin output() {
		return this.output_;
	}
	
	@Override
	public void doUpdate() {
		double sum = 0.0;
		for (Pin p : getInputPins()) {
			int rate = SDFMoC.getPinRate(p);
			for (int i = 0; i < rate; i++) {
				double in = p.readValue(Number.class, true).doubleValue();
				sum += in;
			}
		}
		for (Pin p : getOutputPins()) {
			int rate = SDFMoC.getPinRate(p);
			for (int i = 0; i < rate; i++) {
				p.putValue(sum);
			}
		}
	}
}
