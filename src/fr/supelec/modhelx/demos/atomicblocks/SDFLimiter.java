/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.state_management.Parameter;
import fr.supelec.modhelx.core.state_management.PlainManagedBlock;

/**
 * An SDFLimitedIntegrator is an SDF block which produces -1, 0 or 1
 * on its output when its input is below or equal to the low_limit, 
 * between the low_limit and the high_limit, and above or equal to the high_limit. 
 * The low and high limits are set by the "low" and "high" properties (Double).
 * 
 * @author frederic.boulanger@supelec.fr
 *
 */
public class SDFLimiter extends PlainManagedBlock {
	private Parameter<Double> high_limit;
	private Parameter<Double> low_limit;
	private final Pin input;
	private final Pin output;

	public SDFLimiter(String name, Object ... properties) {
		super(name, "low", 0, "high", 10);
		setProperties(properties);
		this.input = addPin(new Pin(getName()+".input"));
		this.output = addPin(new Pin(getName()+".output"));
	}

	public Pin input() {
		return this.input;
	}

	public Pin output() {
		return this.output;
	}

	@Override
	public void doSetup() {
		this.high_limit = new Parameter<Double>(Double.class);
		this.low_limit = new Parameter<Double>(Double.class);
		initFromProperties("high_limit=high", "low_limit=low");
	}

	@Override
	public void doUpdate() {
		int rate = this.input.getTokens().size();
		for (int i = 0; i < rate; i++) {
			double val = this.input.readValue(Number.class, true).doubleValue();
			if (val <= this.low_limit.getValue()) {
				this.output.putValue(-1);
			} else if (val >= this.high_limit.getValue()) {
				this.output.putValue(1);
			} else {
				this.output.putValue(0);
			}
		}
	}
}
