/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.demos.atomicblocks;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.mocs.de.AbstractDEMoC;
import fr.supelec.modhelx.core.state_management.Attribute;
import fr.supelec.modhelx.core.state_management.Parameter;
import fr.supelec.modhelx.core.state_management.TimedManagedBlock;
import fr.supelec.tesl.core.RoundDouble;

/**
 * A DEScenario plays a scenario on its output pins.
 * The scenario is given in the "scenario" property of the block.
 * The format of the scenario is a string compose of successive
 * scenario steps delimited by semicolumns ';'
 * Each scenario step is formed of the name of the output pin, the equal
 * sign, the type of the data between parenthesis, the value to put on the pin,
 * the @ sign and the date at which the step should be played.
 * For instance "output=(Double)3.14@1.5;" is a scenario step for
 * putting the Double value 3.14 on pin "output" at time 1.5.
 *  
 * @author frederic.boulanger@supelec.fr
 *
 */
public class DEScenario extends TimedManagedBlock<Double> {
	private class Step {
		public Step() {
			this.pin = null;
			this.value = null;
			this.time = 0;
			this.next = null;
		}
		Pin pin;
		Object value;
		double time;
		Step next;
		
		@Override
		public String toString() {
			return "put " + this.value + " on " + this.pin.getName() + " @" + this.time;
		}
	}
	private Parameter<Boolean> verbose;
	private Parameter<Step> scenario_;
	private Parameter<Double> repeatAfter;
	private Attribute<Step> next_step_;
	private Attribute<Double> base_time_;
	private static final Pattern step_pattern = Pattern.compile("(.*)=\\((.*)\\)(.*)@(.*)");

	public DEScenario(String name, Object ... properties) {
		super(Double.class, new RoundDouble(), name, "repeatAfter", -1.0, "verbose", false);
		this.setProperties(properties);
	}

	@Override
	public void doSetup() {
		observationClock().sameTags(((AbstractDEMoC)(getParent().getModel().getMoC())).getClock());
		this.verbose = new Parameter<Boolean>(Boolean.class);
		this.repeatAfter = new Parameter<Double>(Double.class);
		this.scenario_ = new Parameter<Step>(Step.class, false);
		this.next_step_ = new Attribute<Step>(Step.class, false);
		this.base_time_ = new Attribute<Double>(Double.class);
		this.base_time_.init(0.0);
		initFromProperties();
		String scenario = getProperty("scenario", String.class);
		if (scenario == null) {
			this.scenario_.init(null);
			this.next_step_.init(null);
		} else {
			// Parse the scenario string and build a list of scenario steps
			String steps[] = scenario.split(";");
			Step current_step = null;
			for (String step : steps) {
				Matcher m = step_pattern.matcher(step);
				if (m.matches()) {
					String name = m.group(1);
					String type = m.group(2);
					String value = m.group(3);
					String time = m.group(4);
					Step scenario_step = new Step();
					for (Pin p : getOutputPins()) {
						if (p.getName().equals(name)) {
							scenario_step.pin = p;
							break;
						}
					}
					if (scenario_step.pin == null) {
						throw new Error("## Error: unknown output pin name \"" + name + "\"");
					}
					
					if (type.equals("Integer")) {
						scenario_step.value = Integer.parseInt(value);
					} else if (type.equals("Double")) {
						scenario_step.value = Double.parseDouble(value);
					} else if (type.equals("Boolean")) {
						scenario_step.value = Boolean.parseBoolean(value);
					} else if (type.equals("String")) {
						scenario_step.value = value;
					} else {
						throw new Error("## Error: unsupported data type in scenario \""+type+"\"");
					}
					scenario_step.time = Double.parseDouble(time);
					if (current_step == null) {
						this.scenario_.init(scenario_step);
						this.next_step_.init(scenario_step);
					} else {
						current_step.next = scenario_step;
					}
					current_step = scenario_step;
				} else {
					throw new Error("## Error: malformed scenario step \"" + step + "\"");
				}
			}
		}
		if (this.next_step_.getValue() != null) {
			this.initNextObservationTime(tagSum(this.base_time_.getValue(), this.next_step_.getValue().time));
		}
	}

	@Override
	public void doUpdate() {
		Step step = this.next_step_.getValue();
		if (step != null) {
			if (this.verbose.getValue()) {
				System.out.println(getName() + " playing step: " + step);
			}
			step.pin.putValue(step.value);
		}
	}

	@Override
	public void doEndOfSnapshot() {
		if (wasUpdated()) {
			// If we were updated (and so played a step), prepare for the next step
			Step step = this.next_step_.getValue();
			if (step == null) {
				return;
			}
			if ((step.next == null) && (this.repeatAfter.getValue() >= 0)) {
				this.next_step_.setValue(this.scenario_.getValue());
				this.base_time_.setValue(tagSum(getNextObservationTime(), this.repeatAfter.getValue()));
			} else {
				this.next_step_.setValue(step.next);
			}
			step = this.next_step_.getValue();
			if (step != null) {
				double next_time = tagSum(this.base_time_.getValue(), step.time);
				setNextObservationTime(next_time);
			} else {
				setNextObservationTime(null);
			}
		}
	}

//	public static void main(String args[]) {
//		Model root = new Model("root", new ConcurrentDEMoC("DE MoC"));
//		DEScenario player = root.add(new DEScenario("DE scenario player", "scenario", "out=(Double)3.14@1.0;out=(Double)-1.6@3.0", "repeatAfter", 2.0));
//		Pin scenario = player.addPin(new Pin("out"));
//		DETextDisplay disp = root.add(new DETextDisplay("Display"));
//		Pin sink = disp.addPin(new Pin("sink"));
//		scenario.connectTo(sink);
//		
//		Engine exec = new Engine("ModHelX", root);
//		exec.getSolver().addClock(new ASAPClock("ASAP"));
//		exec.stopAfter(10);
//		Logging.setLoggingLevel(ClockSet.ClockLogger, Level.OFF);
//		Logging.setLoggingLevel(Engine.EngineLogger, Level.OFF);
//
//		exec.run();
//	}
}
