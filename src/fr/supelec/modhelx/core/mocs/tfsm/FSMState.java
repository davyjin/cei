/*
 * This file is part of ModHel'X.
 *
 * ModHel'X is free software: you can redistribute it and/or modify
 * it under the terms of the Eclipse Public License 1.0
 *
 * ModHel'X is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * Eclipse Public License for more details.
 *
 * You should have received a copy of the Eclipse Public License
 * along with ModHel'X. If not, see <http://opensource.org/licenses/EPL-1.0>.
 * 
 * Copyright Supélec, Department of Computer Science, 2013
 * http://wwwdi.supelec.fr/software
 * 
 */

package fr.supelec.modhelx.core.mocs.tfsm;

import fr.supelec.modhelx.core.abstractsemantics.AtomicBlock;
import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.core.abstractsemantics.Relation;

public class FSMState extends AtomicBlock {
	private final Pin in_;
	private final Pin out_;
	
	public FSMState(String name) {
		super(name);
		this.in_ = addPin(new Pin(getName()+".in"));
		this.out_ = addPin(new Pin(getName()+".out"));
	}

	public Relation transitionTo(FSMState to, String guard, String action) {
		Relation r = this.in_.connectTo(to.out_);
		r.setProperty("garde", guard);
		if (action != null) {
			r.setProperty("action", action);
		}
		return r;
	}
	
	@Override
	public void update() {
		Boolean verbose = this.getParent().getModel().getProperty("verbose", Boolean.class); 
		if ((verbose != null) && verbose) {
			System.out.println(getParent().getModel().getName() + " entering state " + getName() + " @" + ((TFSMMoC)getMoC()).getCurrentTime());
		}
	}

}
