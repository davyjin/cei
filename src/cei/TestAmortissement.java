package cei;
import org.apache.commons.math3.ode.SecondOrderDifferentialEquations;


public class TestAmortissement implements SecondOrderDifferentialEquations {
	private double a = -0.25;
	private double b = 1;
	private double m = 1;
	private double k = (a*a) + (b*b);
	private double c = -2 * a;
	
	@Override
	public void computeSecondDerivatives(double t, double[] y,
			double[] yDot, double[] yDDot) {
		yDDot[0] = ((-c/m) * yDot[0]) - ((k/m) * y[0]);
	}

	@Override
	public int getDimension() {
		return 1;
	}

}
