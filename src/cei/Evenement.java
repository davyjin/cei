package cei;
import java.util.ArrayList;

import org.antlr.runtime.tree.Tree;
import org.apache.commons.math3.ode.events.EventHandler;

import com.graphbuilder.math.*;

public class Evenement implements EventHandler {
	private Action action;
	private Tree rst_state;
	private Expression rst_op;
	private Expression condition;
	private ArrayList<String> vectEtat;
	private VarMap variables;
	private FuncMap fm;
	private boolean eventOnIncreasing;
	private double eventSignal;
	
	
	public Evenement(Tree rs, Tree cd, String act, ArrayList<String> vectEtat,
			VarMap varmap, FuncMap funcmap, boolean eventOnIncreasing) {
		this.variables = varmap;
		this.fm = funcmap;
		this.rst_state = rs;
		String expr = EquationDiff.removeQuote(rs.getChild(1).getText());		
		String expr2 = EquationDiff.removeQuote(cd.getText());
		this.rst_op = ExpressionTree.parse(expr);
		this.condition = ExpressionTree.parse(expr2);;		
		this.vectEtat = vectEtat;
		this.eventOnIncreasing = eventOnIncreasing;
		this.eventSignal = 0.0;
		if (act.equalsIgnoreCase("reset_state")) {
			this.action = EventHandler.Action.RESET_STATE;
		}
		else if (act.equalsIgnoreCase("stop")) {
			this.action = EventHandler.Action.STOP;
		}
		else {
			this.action = EventHandler.Action.RESET_DERIVATIVES;
		}
	}
	
	@Override
	public Action eventOccurred(double t, double[] y, boolean increasing) {
		if (increasing) {
			this.eventSignal = 1.0;
		}
		else {
			this.eventSignal = -1.0;
		}
		if (increasing == this.eventOnIncreasing) {
			return this.action;
		}
		else
			return EventHandler.Action.CONTINUE;
	}

	@Override
	public double g(double t, double[] y) {		
		this.eventSignal = 0.0;
		this.miseAjourTableVar(y);
		return this.condition.eval(variables, fm);
	}
	
	private void miseAjourTableVar(double[] y) {
		int sizeVector = y.length;		
		for (String varEtat : this.vectEtat) {			
			String der1 = "der1::" + varEtat + "::";
			String der2 = "der2::" + varEtat + "::";			
			String[] tabVar = {varEtat, der1, der2};
			for (int i = 0; i < sizeVector; i++) {
				this.variables.setValue(tabVar[i], y[i]);
			}
		}
	}
	
	@Override
	public void init(double t0, double[] y0, double t) {
		// Unused
	}

	@Override
	public void resetState(double t, double[] y) {
		Tree leftMember = this.rst_state.getChild(0);
		this.miseAjourTableVar(y);
		switch (leftMember.getType()) {
		case EquaDiffParser.ID:
			y[0] = this.rst_op.eval(this.variables, this.fm);
			break;
		case EquaDiffParser.DER1:
			y[1] = this.rst_op.eval(this.variables, this.fm);
			break;
		case EquaDiffParser.DER2:
			break;
		default:
			break;
		}			
	}	
	
	public double getEventSignal() {
		return this.eventSignal;
	}
}
