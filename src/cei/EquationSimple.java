package cei;

import java.util.HashMap;
import java.util.concurrent.Semaphore;

import org.apache.commons.math3.analysis.solvers.BrentSolver;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.ode.FirstOrderConverter;
import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.SecondOrderDifferentialEquations;
import org.apache.commons.math3.ode.events.EventHandler;
import org.apache.commons.math3.ode.nonstiff.ClassicalRungeKuttaIntegrator;
import org.apache.commons.math3.ode.sampling.StepHandler;
import org.apache.commons.math3.ode.sampling.StepInterpolator;

import fr.supelec.modhelx.core.abstractsemantics.Pin;
import fr.supelec.modhelx.demos.ctsolver.DEBouncingBall;

/**
 * Classe simplifiée permettant l'intégration dans ModHelX à partir de blocs.
 * @author jin
 *
 */
public class EquationSimple extends Thread {
	
	public static final String Y = "y"; 
	public static final String YDOT = "yDot"; 
	public static final String YDDOT = "yDDot";	
	public static final String[] VECTEUR = {Y, YDOT, YDDOT};
	
	private FirstOrderDifferentialEquations equaDiff;
	private HashMap<String, Pin> pinTable;
	
	private double t0;
	private double tFinal;	
	private double h;
	
	private double[] y;
	
	private ClassicalRungeKuttaIntegrator rg; 
	private StepSimple stepHandler;
	private EvenementSimple eventHandler;
	
	private boolean isOver;
			
	public EquationSimple(double t0, double tFinal, double h,
			HashMap<String, Pin> t, boolean eventOnIncreasing) {
		this.isOver = false;
		this.t0 = t0;
		this.tFinal = tFinal;
		this.h = h;
		this.pinTable = t;
		this.equaDiff = new FirstOrderConverter(new EquationDiffSimple(this.pinTable));	
		this.y = new double[2];
		
		this.rg = new ClassicalRungeKuttaIntegrator(this.h);
		this.stepHandler = new StepSimple(this.pinTable);
		this.rg.addStepHandler(this.stepHandler);
		this.eventHandler = new EvenementSimple("RESET_STATE", eventOnIncreasing, this.pinTable);	
		this.rg.addEventHandler(this.eventHandler, 0.02, 1e-9, 50000, new BrentSolver(1e-9));		
	}
	
	public void integrate() {
		this.start();
	}
	
	public void run() {
		this.rg.integrate(this.equaDiff, this.t0, this.y, this.tFinal, this.y);
		this.isOver = true;
	}
	
	public StepSimple getStepHandler() {
		return this.stepHandler;
	}
	
	public boolean isOver() {
		return this.isOver;
	}
	
	public double getCurrentTime() {
		return this.stepHandler.getCurrentTime();
	}
	
	public double[] getCurrentState() {
		return this.stepHandler.getCurrentState();
	}
	
	public double getCurrentEvent() {
		if (this.eventHandler != null)
			return this.eventHandler.getEventSignal();
		else
			return 0.0;
	}
	
	public void update() throws InterruptedException {
		this.stepHandler.stepStart();
		this.stepHandler.stepEnd();		
	}
	
				
	private class EquationDiffSimple implements SecondOrderDifferentialEquations {
		
		private HashMap<String, Pin> pinTable;
		
		public EquationDiffSimple(HashMap<String, Pin> t) {			
			this.pinTable = t;
		}
		
		@Override
		public int getDimension() {
			return 1;
		}
		
		@Override
		public void computeSecondDerivatives(double t, double[] y,
				double[] yDot, double[] yDDot) {
			Pin p = this.pinTable.get(DEBouncingBall.PIN_YDDOT_IN);
			if (p.hasToken()) 
				yDDot[0] = p.readValue(Double.class, true);			
		}		
	}
	
	private class StepSimple implements StepHandler {
		@SuppressWarnings("unused")
		private HashMap<String, Pin> pinTable;
		private Semaphore stepStart;
		private Semaphore stepEnd;
		private double currentTime;
		private double[] currentState;
		private boolean isLast;
		
		public StepSimple(HashMap<String, Pin> t) {
			this.pinTable = t;
			this.initSemaphore();
		}
		
		private void initSemaphore() {
			this.stepStart = new Semaphore(0);
			this.stepEnd = new Semaphore(0);
		}
		
		@Override
		public void handleStep(StepInterpolator interpolator, boolean isLast)
				throws MaxCountExceededException {
			this.isLast = isLast;
			try {
				this.stepStart.acquire();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			this.currentTime = interpolator.getCurrentTime();
	        this.currentState = interpolator.getInterpolatedState();
	        System.out.println("t = " + this.currentTime + "\n" + "y = " + this.currentState[0]);
	        this.stepEnd.release();
		}
		
		@Override
		public void init(double t0, double[] y0, double t) {
			
		}	
		
		public void stepStart() {		
			if (this.isLast == false) {
				this.stepStart.release();
			}
		}
		
		public void stepEnd() throws InterruptedException {
			if (this.isLast == false) {
				this.stepEnd.acquire();
			}
		}
		
		public double getCurrentTime() {
			return this.currentTime;
		}
		
		public double[] getCurrentState() {
			return this.currentState.clone();
		}
	}
	
	private class EvenementSimple implements EventHandler {
		private Action action;
		private boolean eventOnIncreasing;
		private double eventSignal;
		private HashMap<String, Pin> pinTable;
		private double precValue = 1.0;
		
		public EvenementSimple(String act, boolean eventOnIncreasing,
				HashMap<String, Pin> t) {
			this.eventOnIncreasing = eventOnIncreasing;
			this.eventSignal = 0.0;
			if (act.equalsIgnoreCase("reset_state")) {
				this.action = EventHandler.Action.RESET_STATE;
			}
			else if (act.equalsIgnoreCase("stop")) {
				this.action = EventHandler.Action.STOP;
			}
			else {
				this.action = EventHandler.Action.RESET_DERIVATIVES;
			}
			this.pinTable = t;
		}
		
		@Override
		public Action eventOccurred(double t, double[] y, boolean increasing) {
			if (increasing) {
				this.eventSignal = 1.0;
			}
			else {
				this.eventSignal = -1.0;
			}
			if (increasing == this.eventOnIncreasing) {
				return this.action;
			}
			else
				return EventHandler.Action.CONTINUE;
		}

		@Override
		public double g(double t, double[] y) {		
			this.eventSignal = 0.0;
			Pin p = this.pinTable.get(DEBouncingBall.PIN_EVENT);
			if(p.hasToken())				
				this.precValue =  p.readValue(Double.class, true);
			return this.precValue;				
		}
		
		@Override
		public void init(double t0, double[] y0, double t) {
			// Unused
		}

		@Override
		public void resetState(double t, double[] y) {
			Pin p = this.pinTable.get(DEBouncingBall.PIN_RESET);
			if (p.hasToken())
				y[1] = - p.readValue(Double.class, true);			
		}	
		
		public double getEventSignal() {
			return this.eventSignal;
		}
	}

}
