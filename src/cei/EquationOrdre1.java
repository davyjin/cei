package cei;
import org.antlr.runtime.tree.Tree;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;


public class EquationOrdre1 extends EquationDiff implements
		FirstOrderDifferentialEquations {

	public EquationOrdre1(Tree arbre) {
		super(arbre);
	}
	
	@Override
	public void computeDerivatives(double t, double[] y, double[] yDot)
			throws MaxCountExceededException, DimensionMismatchException {
		Tree racineDerivees = this.getTree().getChild(IndiceBloc.INDICEDERIVEES.ordinal());
		for (int i = 0; i < racineDerivees.getChildCount(); i++) {
			this.parcoursBrancheDerivees(racineDerivees.getChild(i), y, yDot);
		}
	}

	@Override
	public int getDimension() {
		return 1;
	}
	
	public void parcoursBrancheDerivees(Tree racineAffect, double[] y, double[] yDot) {
		Tree racineDerivees = racineAffect.getChild(0);
		Tree racineOp = racineAffect.getChild(1);		
		switch(racineDerivees.getType()) {
		case EquaDiffParser.DER1 :
			yDot[0] = this.calculDerivee(racineOp, y, yDot); 
			break;
		case EquaDiffParser.ID :
			y[0] = this.calculDerivee(racineOp, y, yDot); 
			break;
		default :
			break;				
		}
	}
	
	public double calculDerivee(Tree racineOp, double[] y, double[] yDot) {
		double value;
		switch(racineOp.getType()) {
		case EquaDiffParser.INT :
			value = Double.parseDouble(racineOp.getText());
			break;
		case EquaDiffParser.FLOAT :
			value = Double.parseDouble(racineOp.getText());
			break;
		case EquaDiffParser.ID :
			String nomConstante = racineOp.getText();
			if (this.isCste(nomConstante)) {
				value = this.getCste(nomConstante);
			}
			else if (racineOp.getParent().getType() == EquaDiffParser.DER1) {
				value = yDot[0];
			}
			else {
				value = y[0];
			}
			break;
		case EquaDiffParser.DER1 : 
			value = this.calculDerivee(racineOp.getChild(0), y, yDot);
			break;
		case EquaDiffParser.ADDOP :
			if (racineOp.getChildCount() == 2) {
				if (racineOp.getText().equals("+"))
					value = this.calculDerivee(racineOp.getChild(0), y, yDot) 
					+ this.calculDerivee(racineOp.getChild(1), y, yDot);
				else 
					value = this.calculDerivee(racineOp.getChild(0), y, yDot) 
					- this.calculDerivee(racineOp.getChild(1), y, yDot);
			}
			else {
				value = - this.calculDerivee(racineOp.getChild(0), y, yDot);
			}
			break;
		case EquaDiffParser.MULTOP :
			if (racineOp.getText().equals("*"))
				value = this.calculDerivee(racineOp.getChild(0), y, yDot) 
						* this.calculDerivee(racineOp.getChild(1), y, yDot);
			else 
				value = this.calculDerivee(racineOp.getChild(0), y, yDot) 
						/ this.calculDerivee(racineOp.getChild(1), y, yDot);
			break;
		default:
			value = 0.0;
			break;
		}
		return value;
	}		

}
