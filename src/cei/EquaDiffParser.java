package cei;
// $ANTLR 3.5.1 /home/jin/eclipse/EquaDiff/EquaDiff.g 2014-02-07 13:34:13

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class EquaDiffParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ACTION_EVT", "ACT_RST_ST", "ADDOP", 
		"AFFECT", "AS", "CHAR", "COMMA", "COMMENT", "CONDINI", "CSTES", "DER1", 
		"DER2", "DERIV", "DOUBLE_U", "ESC_SEQ", "EVT", "EXPONENT", "FLOAT", "HEX_DIGIT", 
		"ID", "INT", "LACC", "LPAREN", "MULTOP", "OCTAL_ESC", "ORDRE", "RACC", 
		"RPAREN", "SEP", "STRING", "THEN", "UNICODE_ESC", "VECTETAT", "WHEN", 
		"WS"
	};
	public static final int EOF=-1;
	public static final int ACTION_EVT=4;
	public static final int ACT_RST_ST=5;
	public static final int ADDOP=6;
	public static final int AFFECT=7;
	public static final int AS=8;
	public static final int CHAR=9;
	public static final int COMMA=10;
	public static final int COMMENT=11;
	public static final int CONDINI=12;
	public static final int CSTES=13;
	public static final int DER1=14;
	public static final int DER2=15;
	public static final int DERIV=16;
	public static final int DOUBLE_U=17;
	public static final int ESC_SEQ=18;
	public static final int EVT=19;
	public static final int EXPONENT=20;
	public static final int FLOAT=21;
	public static final int HEX_DIGIT=22;
	public static final int ID=23;
	public static final int INT=24;
	public static final int LACC=25;
	public static final int LPAREN=26;
	public static final int MULTOP=27;
	public static final int OCTAL_ESC=28;
	public static final int ORDRE=29;
	public static final int RACC=30;
	public static final int RPAREN=31;
	public static final int SEP=32;
	public static final int STRING=33;
	public static final int THEN=34;
	public static final int UNICODE_ESC=35;
	public static final int VECTETAT=36;
	public static final int WHEN=37;
	public static final int WS=38;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public EquaDiffParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public EquaDiffParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return EquaDiffParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/jin/eclipse/EquaDiff/EquaDiff.g"; }


	public static class equation_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "equation"
	// /home/jin/eclipse/EquaDiff/EquaDiff.g:15:1: equation : ordre vecteuretat condini cste derivees ( evt )? ;
	public final EquaDiffParser.equation_return equation() throws RecognitionException {
		EquaDiffParser.equation_return retval = new EquaDiffParser.equation_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope ordre1 =null;
		ParserRuleReturnScope vecteuretat2 =null;
		ParserRuleReturnScope condini3 =null;
		ParserRuleReturnScope cste4 =null;
		ParserRuleReturnScope derivees5 =null;
		ParserRuleReturnScope evt6 =null;


		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:16:2: ( ordre vecteuretat condini cste derivees ( evt )? )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:16:4: ordre vecteuretat condini cste derivees ( evt )?
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_ordre_in_equation47);
			ordre1=ordre();
			state._fsp--;

			adaptor.addChild(root_0, ordre1.getTree());

			pushFollow(FOLLOW_vecteuretat_in_equation49);
			vecteuretat2=vecteuretat();
			state._fsp--;

			adaptor.addChild(root_0, vecteuretat2.getTree());

			pushFollow(FOLLOW_condini_in_equation51);
			condini3=condini();
			state._fsp--;

			adaptor.addChild(root_0, condini3.getTree());

			pushFollow(FOLLOW_cste_in_equation53);
			cste4=cste();
			state._fsp--;

			adaptor.addChild(root_0, cste4.getTree());

			pushFollow(FOLLOW_derivees_in_equation55);
			derivees5=derivees();
			state._fsp--;

			adaptor.addChild(root_0, derivees5.getTree());

			// /home/jin/eclipse/EquaDiff/EquaDiff.g:16:44: ( evt )?
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==EVT) ) {
				alt1=1;
			}
			switch (alt1) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:16:44: evt
					{
					pushFollow(FOLLOW_evt_in_equation57);
					evt6=evt();
					state._fsp--;

					adaptor.addChild(root_0, evt6.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		   catch (RecognitionException e) {
		    throw e;
		   }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "equation"


	public static class ordre_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "ordre"
	// /home/jin/eclipse/EquaDiff/EquaDiff.g:19:1: ordre : ORDRE ^ AFFECT ! INT SEP !;
	public final EquaDiffParser.ordre_return ordre() throws RecognitionException {
		EquaDiffParser.ordre_return retval = new EquaDiffParser.ordre_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token ORDRE7=null;
		Token AFFECT8=null;
		Token INT9=null;
		Token SEP10=null;

		CommonTree ORDRE7_tree=null;
		CommonTree AFFECT8_tree=null;
		CommonTree INT9_tree=null;
		CommonTree SEP10_tree=null;

		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:20:2: ( ORDRE ^ AFFECT ! INT SEP !)
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:20:5: ORDRE ^ AFFECT ! INT SEP !
			{
			root_0 = (CommonTree)adaptor.nil();


			ORDRE7=(Token)match(input,ORDRE,FOLLOW_ORDRE_in_ordre70); 
			ORDRE7_tree = (CommonTree)adaptor.create(ORDRE7);
			root_0 = (CommonTree)adaptor.becomeRoot(ORDRE7_tree, root_0);

			AFFECT8=(Token)match(input,AFFECT,FOLLOW_AFFECT_in_ordre73); 
			INT9=(Token)match(input,INT,FOLLOW_INT_in_ordre76); 
			INT9_tree = (CommonTree)adaptor.create(INT9);
			adaptor.addChild(root_0, INT9_tree);

			SEP10=(Token)match(input,SEP,FOLLOW_SEP_in_ordre78); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		   catch (RecognitionException e) {
		    throw e;
		   }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "ordre"


	public static class vecteuretat_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "vecteuretat"
	// /home/jin/eclipse/EquaDiff/EquaDiff.g:22:1: vecteuretat : VECTETAT ^ LACC ! LPAREN ! ( ID )+ RPAREN ! SEP ! RACC !;
	public final EquaDiffParser.vecteuretat_return vecteuretat() throws RecognitionException {
		EquaDiffParser.vecteuretat_return retval = new EquaDiffParser.vecteuretat_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token VECTETAT11=null;
		Token LACC12=null;
		Token LPAREN13=null;
		Token ID14=null;
		Token RPAREN15=null;
		Token SEP16=null;
		Token RACC17=null;

		CommonTree VECTETAT11_tree=null;
		CommonTree LACC12_tree=null;
		CommonTree LPAREN13_tree=null;
		CommonTree ID14_tree=null;
		CommonTree RPAREN15_tree=null;
		CommonTree SEP16_tree=null;
		CommonTree RACC17_tree=null;

		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:23:2: ( VECTETAT ^ LACC ! LPAREN ! ( ID )+ RPAREN ! SEP ! RACC !)
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:23:5: VECTETAT ^ LACC ! LPAREN ! ( ID )+ RPAREN ! SEP ! RACC !
			{
			root_0 = (CommonTree)adaptor.nil();


			VECTETAT11=(Token)match(input,VECTETAT,FOLLOW_VECTETAT_in_vecteuretat90); 
			VECTETAT11_tree = (CommonTree)adaptor.create(VECTETAT11);
			root_0 = (CommonTree)adaptor.becomeRoot(VECTETAT11_tree, root_0);

			LACC12=(Token)match(input,LACC,FOLLOW_LACC_in_vecteuretat93); 
			LPAREN13=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_vecteuretat96); 
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:23:29: ( ID )+
			int cnt2=0;
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( (LA2_0==ID) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:23:30: ID
					{
					ID14=(Token)match(input,ID,FOLLOW_ID_in_vecteuretat100); 
					ID14_tree = (CommonTree)adaptor.create(ID14);
					adaptor.addChild(root_0, ID14_tree);

					}
					break;

				default :
					if ( cnt2 >= 1 ) break loop2;
					EarlyExitException eee = new EarlyExitException(2, input);
					throw eee;
				}
				cnt2++;
			}

			RPAREN15=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_vecteuretat104); 
			SEP16=(Token)match(input,SEP,FOLLOW_SEP_in_vecteuretat107); 
			RACC17=(Token)match(input,RACC,FOLLOW_RACC_in_vecteuretat110); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		   catch (RecognitionException e) {
		    throw e;
		   }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "vecteuretat"


	public static class condini_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "condini"
	// /home/jin/eclipse/EquaDiff/EquaDiff.g:26:1: condini : CONDINI ^ LACC ! ( affectation )+ RACC !;
	public final EquaDiffParser.condini_return condini() throws RecognitionException {
		EquaDiffParser.condini_return retval = new EquaDiffParser.condini_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token CONDINI18=null;
		Token LACC19=null;
		Token RACC21=null;
		ParserRuleReturnScope affectation20 =null;

		CommonTree CONDINI18_tree=null;
		CommonTree LACC19_tree=null;
		CommonTree RACC21_tree=null;

		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:27:2: ( CONDINI ^ LACC ! ( affectation )+ RACC !)
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:27:4: CONDINI ^ LACC ! ( affectation )+ RACC !
			{
			root_0 = (CommonTree)adaptor.nil();


			CONDINI18=(Token)match(input,CONDINI,FOLLOW_CONDINI_in_condini123); 
			CONDINI18_tree = (CommonTree)adaptor.create(CONDINI18);
			root_0 = (CommonTree)adaptor.becomeRoot(CONDINI18_tree, root_0);

			LACC19=(Token)match(input,LACC,FOLLOW_LACC_in_condini126); 
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:27:19: ( affectation )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= DER1 && LA3_0 <= DER2)||LA3_0==ID) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:27:19: affectation
					{
					pushFollow(FOLLOW_affectation_in_condini129);
					affectation20=affectation();
					state._fsp--;

					adaptor.addChild(root_0, affectation20.getTree());

					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			RACC21=(Token)match(input,RACC,FOLLOW_RACC_in_condini132); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		   catch (RecognitionException e) {
		    throw e;
		   }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "condini"


	public static class cste_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "cste"
	// /home/jin/eclipse/EquaDiff/EquaDiff.g:30:1: cste : CSTES ^ LACC ! ( affectation )+ RACC !;
	public final EquaDiffParser.cste_return cste() throws RecognitionException {
		EquaDiffParser.cste_return retval = new EquaDiffParser.cste_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token CSTES22=null;
		Token LACC23=null;
		Token RACC25=null;
		ParserRuleReturnScope affectation24 =null;

		CommonTree CSTES22_tree=null;
		CommonTree LACC23_tree=null;
		CommonTree RACC25_tree=null;

		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:31:2: ( CSTES ^ LACC ! ( affectation )+ RACC !)
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:31:4: CSTES ^ LACC ! ( affectation )+ RACC !
			{
			root_0 = (CommonTree)adaptor.nil();


			CSTES22=(Token)match(input,CSTES,FOLLOW_CSTES_in_cste146); 
			CSTES22_tree = (CommonTree)adaptor.create(CSTES22);
			root_0 = (CommonTree)adaptor.becomeRoot(CSTES22_tree, root_0);

			LACC23=(Token)match(input,LACC,FOLLOW_LACC_in_cste149); 
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:31:17: ( affectation )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( ((LA4_0 >= DER1 && LA4_0 <= DER2)||LA4_0==ID) ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:31:17: affectation
					{
					pushFollow(FOLLOW_affectation_in_cste152);
					affectation24=affectation();
					state._fsp--;

					adaptor.addChild(root_0, affectation24.getTree());

					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			RACC25=(Token)match(input,RACC,FOLLOW_RACC_in_cste155); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		   catch (RecognitionException e) {
		    throw e;
		   }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "cste"


	public static class evt_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "evt"
	// /home/jin/eclipse/EquaDiff/EquaDiff.g:34:1: evt : EVT ^ LACC ! exprevt RACC !;
	public final EquaDiffParser.evt_return evt() throws RecognitionException {
		EquaDiffParser.evt_return retval = new EquaDiffParser.evt_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token EVT26=null;
		Token LACC27=null;
		Token RACC29=null;
		ParserRuleReturnScope exprevt28 =null;

		CommonTree EVT26_tree=null;
		CommonTree LACC27_tree=null;
		CommonTree RACC29_tree=null;

		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:35:2: ( EVT ^ LACC ! exprevt RACC !)
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:35:4: EVT ^ LACC ! exprevt RACC !
			{
			root_0 = (CommonTree)adaptor.nil();


			EVT26=(Token)match(input,EVT,FOLLOW_EVT_in_evt167); 
			EVT26_tree = (CommonTree)adaptor.create(EVT26);
			root_0 = (CommonTree)adaptor.becomeRoot(EVT26_tree, root_0);

			LACC27=(Token)match(input,LACC,FOLLOW_LACC_in_evt170); 
			pushFollow(FOLLOW_exprevt_in_evt173);
			exprevt28=exprevt();
			state._fsp--;

			adaptor.addChild(root_0, exprevt28.getTree());

			RACC29=(Token)match(input,RACC,FOLLOW_RACC_in_evt175); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		   catch (RecognitionException e) {
		    throw e;
		   }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "evt"


	public static class exprevt_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "exprevt"
	// /home/jin/eclipse/EquaDiff/EquaDiff.g:38:1: exprevt : ( ACTION_EVT ^ WHEN ! expr SEP !| ACT_RST_ST ^ AS ! ( affectation )+ WHEN ! expr SEP !);
	public final EquaDiffParser.exprevt_return exprevt() throws RecognitionException {
		EquaDiffParser.exprevt_return retval = new EquaDiffParser.exprevt_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token ACTION_EVT30=null;
		Token WHEN31=null;
		Token SEP33=null;
		Token ACT_RST_ST34=null;
		Token AS35=null;
		Token WHEN37=null;
		Token SEP39=null;
		ParserRuleReturnScope expr32 =null;
		ParserRuleReturnScope affectation36 =null;
		ParserRuleReturnScope expr38 =null;

		CommonTree ACTION_EVT30_tree=null;
		CommonTree WHEN31_tree=null;
		CommonTree SEP33_tree=null;
		CommonTree ACT_RST_ST34_tree=null;
		CommonTree AS35_tree=null;
		CommonTree WHEN37_tree=null;
		CommonTree SEP39_tree=null;

		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:39:2: ( ACTION_EVT ^ WHEN ! expr SEP !| ACT_RST_ST ^ AS ! ( affectation )+ WHEN ! expr SEP !)
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( (LA6_0==ACTION_EVT) ) {
				alt6=1;
			}
			else if ( (LA6_0==ACT_RST_ST) ) {
				alt6=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}

			switch (alt6) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:39:4: ACTION_EVT ^ WHEN ! expr SEP !
					{
					root_0 = (CommonTree)adaptor.nil();


					ACTION_EVT30=(Token)match(input,ACTION_EVT,FOLLOW_ACTION_EVT_in_exprevt187); 
					ACTION_EVT30_tree = (CommonTree)adaptor.create(ACTION_EVT30);
					root_0 = (CommonTree)adaptor.becomeRoot(ACTION_EVT30_tree, root_0);

					WHEN31=(Token)match(input,WHEN,FOLLOW_WHEN_in_exprevt190); 
					pushFollow(FOLLOW_expr_in_exprevt193);
					expr32=expr();
					state._fsp--;

					adaptor.addChild(root_0, expr32.getTree());

					SEP33=(Token)match(input,SEP,FOLLOW_SEP_in_exprevt195); 
					}
					break;
				case 2 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:40:4: ACT_RST_ST ^ AS ! ( affectation )+ WHEN ! expr SEP !
					{
					root_0 = (CommonTree)adaptor.nil();


					ACT_RST_ST34=(Token)match(input,ACT_RST_ST,FOLLOW_ACT_RST_ST_in_exprevt201); 
					ACT_RST_ST34_tree = (CommonTree)adaptor.create(ACT_RST_ST34);
					root_0 = (CommonTree)adaptor.becomeRoot(ACT_RST_ST34_tree, root_0);

					AS35=(Token)match(input,AS,FOLLOW_AS_in_exprevt204); 
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:40:20: ( affectation )+
					int cnt5=0;
					loop5:
					while (true) {
						int alt5=2;
						int LA5_0 = input.LA(1);
						if ( ((LA5_0 >= DER1 && LA5_0 <= DER2)||LA5_0==ID) ) {
							alt5=1;
						}

						switch (alt5) {
						case 1 :
							// /home/jin/eclipse/EquaDiff/EquaDiff.g:40:20: affectation
							{
							pushFollow(FOLLOW_affectation_in_exprevt207);
							affectation36=affectation();
							state._fsp--;

							adaptor.addChild(root_0, affectation36.getTree());

							}
							break;

						default :
							if ( cnt5 >= 1 ) break loop5;
							EarlyExitException eee = new EarlyExitException(5, input);
							throw eee;
						}
						cnt5++;
					}

					WHEN37=(Token)match(input,WHEN,FOLLOW_WHEN_in_exprevt210); 
					pushFollow(FOLLOW_expr_in_exprevt213);
					expr38=expr();
					state._fsp--;

					adaptor.addChild(root_0, expr38.getTree());

					SEP39=(Token)match(input,SEP,FOLLOW_SEP_in_exprevt215); 
					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		   catch (RecognitionException e) {
		    throw e;
		   }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprevt"


	public static class affectation_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "affectation"
	// /home/jin/eclipse/EquaDiff/EquaDiff.g:43:1: affectation : ( ID AFFECT ^ STRING SEP !| der AFFECT ^ STRING SEP !);
	public final EquaDiffParser.affectation_return affectation() throws RecognitionException {
		EquaDiffParser.affectation_return retval = new EquaDiffParser.affectation_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token ID40=null;
		Token AFFECT41=null;
		Token STRING42=null;
		Token SEP43=null;
		Token AFFECT45=null;
		Token STRING46=null;
		Token SEP47=null;
		ParserRuleReturnScope der44 =null;

		CommonTree ID40_tree=null;
		CommonTree AFFECT41_tree=null;
		CommonTree STRING42_tree=null;
		CommonTree SEP43_tree=null;
		CommonTree AFFECT45_tree=null;
		CommonTree STRING46_tree=null;
		CommonTree SEP47_tree=null;

		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:44:2: ( ID AFFECT ^ STRING SEP !| der AFFECT ^ STRING SEP !)
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0==ID) ) {
				alt7=1;
			}
			else if ( ((LA7_0 >= DER1 && LA7_0 <= DER2)) ) {
				alt7=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}

			switch (alt7) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:44:4: ID AFFECT ^ STRING SEP !
					{
					root_0 = (CommonTree)adaptor.nil();


					ID40=(Token)match(input,ID,FOLLOW_ID_in_affectation228); 
					ID40_tree = (CommonTree)adaptor.create(ID40);
					adaptor.addChild(root_0, ID40_tree);

					AFFECT41=(Token)match(input,AFFECT,FOLLOW_AFFECT_in_affectation230); 
					AFFECT41_tree = (CommonTree)adaptor.create(AFFECT41);
					root_0 = (CommonTree)adaptor.becomeRoot(AFFECT41_tree, root_0);

					STRING42=(Token)match(input,STRING,FOLLOW_STRING_in_affectation233); 
					STRING42_tree = (CommonTree)adaptor.create(STRING42);
					adaptor.addChild(root_0, STRING42_tree);

					SEP43=(Token)match(input,SEP,FOLLOW_SEP_in_affectation235); 
					}
					break;
				case 2 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:45:4: der AFFECT ^ STRING SEP !
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_der_in_affectation242);
					der44=der();
					state._fsp--;

					adaptor.addChild(root_0, der44.getTree());

					AFFECT45=(Token)match(input,AFFECT,FOLLOW_AFFECT_in_affectation244); 
					AFFECT45_tree = (CommonTree)adaptor.create(AFFECT45);
					root_0 = (CommonTree)adaptor.becomeRoot(AFFECT45_tree, root_0);

					STRING46=(Token)match(input,STRING,FOLLOW_STRING_in_affectation247); 
					STRING46_tree = (CommonTree)adaptor.create(STRING46);
					adaptor.addChild(root_0, STRING46_tree);

					SEP47=(Token)match(input,SEP,FOLLOW_SEP_in_affectation249); 
					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		   catch (RecognitionException e) {
		    throw e;
		   }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "affectation"


	public static class derivees_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "derivees"
	// /home/jin/eclipse/EquaDiff/EquaDiff.g:48:1: derivees : DERIV ^ LACC ! ( affectation )+ RACC !;
	public final EquaDiffParser.derivees_return derivees() throws RecognitionException {
		EquaDiffParser.derivees_return retval = new EquaDiffParser.derivees_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token DERIV48=null;
		Token LACC49=null;
		Token RACC51=null;
		ParserRuleReturnScope affectation50 =null;

		CommonTree DERIV48_tree=null;
		CommonTree LACC49_tree=null;
		CommonTree RACC51_tree=null;

		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:49:2: ( DERIV ^ LACC ! ( affectation )+ RACC !)
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:49:4: DERIV ^ LACC ! ( affectation )+ RACC !
			{
			root_0 = (CommonTree)adaptor.nil();


			DERIV48=(Token)match(input,DERIV,FOLLOW_DERIV_in_derivees261); 
			DERIV48_tree = (CommonTree)adaptor.create(DERIV48);
			root_0 = (CommonTree)adaptor.becomeRoot(DERIV48_tree, root_0);

			LACC49=(Token)match(input,LACC,FOLLOW_LACC_in_derivees264); 
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:49:17: ( affectation )+
			int cnt8=0;
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( ((LA8_0 >= DER1 && LA8_0 <= DER2)||LA8_0==ID) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:49:18: affectation
					{
					pushFollow(FOLLOW_affectation_in_derivees268);
					affectation50=affectation();
					state._fsp--;

					adaptor.addChild(root_0, affectation50.getTree());

					}
					break;

				default :
					if ( cnt8 >= 1 ) break loop8;
					EarlyExitException eee = new EarlyExitException(8, input);
					throw eee;
				}
				cnt8++;
			}

			RACC51=(Token)match(input,RACC,FOLLOW_RACC_in_derivees272); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		   catch (RecognitionException e) {
		    throw e;
		   }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "derivees"


	public static class der_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "der"
	// /home/jin/eclipse/EquaDiff/EquaDiff.g:52:1: der : ( DER1 ^ DOUBLE_U ! ID DOUBLE_U !| DER2 ^ DOUBLE_U ! ID DOUBLE_U !);
	public final EquaDiffParser.der_return der() throws RecognitionException {
		EquaDiffParser.der_return retval = new EquaDiffParser.der_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token DER152=null;
		Token DOUBLE_U53=null;
		Token ID54=null;
		Token DOUBLE_U55=null;
		Token DER256=null;
		Token DOUBLE_U57=null;
		Token ID58=null;
		Token DOUBLE_U59=null;

		CommonTree DER152_tree=null;
		CommonTree DOUBLE_U53_tree=null;
		CommonTree ID54_tree=null;
		CommonTree DOUBLE_U55_tree=null;
		CommonTree DER256_tree=null;
		CommonTree DOUBLE_U57_tree=null;
		CommonTree ID58_tree=null;
		CommonTree DOUBLE_U59_tree=null;

		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:53:2: ( DER1 ^ DOUBLE_U ! ID DOUBLE_U !| DER2 ^ DOUBLE_U ! ID DOUBLE_U !)
			int alt9=2;
			int LA9_0 = input.LA(1);
			if ( (LA9_0==DER1) ) {
				alt9=1;
			}
			else if ( (LA9_0==DER2) ) {
				alt9=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 9, 0, input);
				throw nvae;
			}

			switch (alt9) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:53:4: DER1 ^ DOUBLE_U ! ID DOUBLE_U !
					{
					root_0 = (CommonTree)adaptor.nil();


					DER152=(Token)match(input,DER1,FOLLOW_DER1_in_der285); 
					DER152_tree = (CommonTree)adaptor.create(DER152);
					root_0 = (CommonTree)adaptor.becomeRoot(DER152_tree, root_0);

					DOUBLE_U53=(Token)match(input,DOUBLE_U,FOLLOW_DOUBLE_U_in_der288); 
					ID54=(Token)match(input,ID,FOLLOW_ID_in_der291); 
					ID54_tree = (CommonTree)adaptor.create(ID54);
					adaptor.addChild(root_0, ID54_tree);

					DOUBLE_U55=(Token)match(input,DOUBLE_U,FOLLOW_DOUBLE_U_in_der293); 
					}
					break;
				case 2 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:54:4: DER2 ^ DOUBLE_U ! ID DOUBLE_U !
					{
					root_0 = (CommonTree)adaptor.nil();


					DER256=(Token)match(input,DER2,FOLLOW_DER2_in_der299); 
					DER256_tree = (CommonTree)adaptor.create(DER256);
					root_0 = (CommonTree)adaptor.becomeRoot(DER256_tree, root_0);

					DOUBLE_U57=(Token)match(input,DOUBLE_U,FOLLOW_DOUBLE_U_in_der302); 
					ID58=(Token)match(input,ID,FOLLOW_ID_in_der305); 
					ID58_tree = (CommonTree)adaptor.create(ID58);
					adaptor.addChild(root_0, ID58_tree);

					DOUBLE_U59=(Token)match(input,DOUBLE_U,FOLLOW_DOUBLE_U_in_der307); 
					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		   catch (RecognitionException e) {
		    throw e;
		   }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "der"


	public static class expr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr"
	// /home/jin/eclipse/EquaDiff/EquaDiff.g:57:1: expr : ( term ( ADDOP ^ term )* | ADDOP ^ expr );
	public final EquaDiffParser.expr_return expr() throws RecognitionException {
		EquaDiffParser.expr_return retval = new EquaDiffParser.expr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token ADDOP61=null;
		Token ADDOP63=null;
		ParserRuleReturnScope term60 =null;
		ParserRuleReturnScope term62 =null;
		ParserRuleReturnScope expr64 =null;

		CommonTree ADDOP61_tree=null;
		CommonTree ADDOP63_tree=null;

		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:58:2: ( term ( ADDOP ^ term )* | ADDOP ^ expr )
			int alt11=2;
			int LA11_0 = input.LA(1);
			if ( ((LA11_0 >= DER1 && LA11_0 <= DER2)||LA11_0==FLOAT||(LA11_0 >= ID && LA11_0 <= INT)||LA11_0==LPAREN) ) {
				alt11=1;
			}
			else if ( (LA11_0==ADDOP) ) {
				alt11=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 11, 0, input);
				throw nvae;
			}

			switch (alt11) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:58:4: term ( ADDOP ^ term )*
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_term_in_expr320);
					term60=term();
					state._fsp--;

					adaptor.addChild(root_0, term60.getTree());

					// /home/jin/eclipse/EquaDiff/EquaDiff.g:58:9: ( ADDOP ^ term )*
					loop10:
					while (true) {
						int alt10=2;
						int LA10_0 = input.LA(1);
						if ( (LA10_0==ADDOP) ) {
							alt10=1;
						}

						switch (alt10) {
						case 1 :
							// /home/jin/eclipse/EquaDiff/EquaDiff.g:58:10: ADDOP ^ term
							{
							ADDOP61=(Token)match(input,ADDOP,FOLLOW_ADDOP_in_expr323); 
							ADDOP61_tree = (CommonTree)adaptor.create(ADDOP61);
							root_0 = (CommonTree)adaptor.becomeRoot(ADDOP61_tree, root_0);

							pushFollow(FOLLOW_term_in_expr326);
							term62=term();
							state._fsp--;

							adaptor.addChild(root_0, term62.getTree());

							}
							break;

						default :
							break loop10;
						}
					}

					}
					break;
				case 2 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:59:4: ADDOP ^ expr
					{
					root_0 = (CommonTree)adaptor.nil();


					ADDOP63=(Token)match(input,ADDOP,FOLLOW_ADDOP_in_expr333); 
					ADDOP63_tree = (CommonTree)adaptor.create(ADDOP63);
					root_0 = (CommonTree)adaptor.becomeRoot(ADDOP63_tree, root_0);

					pushFollow(FOLLOW_expr_in_expr336);
					expr64=expr();
					state._fsp--;

					adaptor.addChild(root_0, expr64.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		   catch (RecognitionException e) {
		    throw e;
		   }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expr"


	public static class term_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "term"
	// /home/jin/eclipse/EquaDiff/EquaDiff.g:62:1: term : factor ( MULTOP ^ factor )* ;
	public final EquaDiffParser.term_return term() throws RecognitionException {
		EquaDiffParser.term_return retval = new EquaDiffParser.term_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token MULTOP66=null;
		ParserRuleReturnScope factor65 =null;
		ParserRuleReturnScope factor67 =null;

		CommonTree MULTOP66_tree=null;

		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:62:6: ( factor ( MULTOP ^ factor )* )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:62:8: factor ( MULTOP ^ factor )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_factor_in_term346);
			factor65=factor();
			state._fsp--;

			adaptor.addChild(root_0, factor65.getTree());

			// /home/jin/eclipse/EquaDiff/EquaDiff.g:62:15: ( MULTOP ^ factor )*
			loop12:
			while (true) {
				int alt12=2;
				int LA12_0 = input.LA(1);
				if ( (LA12_0==MULTOP) ) {
					alt12=1;
				}

				switch (alt12) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:62:16: MULTOP ^ factor
					{
					MULTOP66=(Token)match(input,MULTOP,FOLLOW_MULTOP_in_term349); 
					MULTOP66_tree = (CommonTree)adaptor.create(MULTOP66);
					root_0 = (CommonTree)adaptor.becomeRoot(MULTOP66_tree, root_0);

					pushFollow(FOLLOW_factor_in_term352);
					factor67=factor();
					state._fsp--;

					adaptor.addChild(root_0, factor67.getTree());

					}
					break;

				default :
					break loop12;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		   catch (RecognitionException e) {
		    throw e;
		   }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "term"


	public static class factor_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "factor"
	// /home/jin/eclipse/EquaDiff/EquaDiff.g:64:1: factor : ( ID | FLOAT | INT | ( LPAREN ! expr RPAREN !) | der );
	public final EquaDiffParser.factor_return factor() throws RecognitionException {
		EquaDiffParser.factor_return retval = new EquaDiffParser.factor_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token ID68=null;
		Token FLOAT69=null;
		Token INT70=null;
		Token LPAREN71=null;
		Token RPAREN73=null;
		ParserRuleReturnScope expr72 =null;
		ParserRuleReturnScope der74 =null;

		CommonTree ID68_tree=null;
		CommonTree FLOAT69_tree=null;
		CommonTree INT70_tree=null;
		CommonTree LPAREN71_tree=null;
		CommonTree RPAREN73_tree=null;

		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:64:8: ( ID | FLOAT | INT | ( LPAREN ! expr RPAREN !) | der )
			int alt13=5;
			switch ( input.LA(1) ) {
			case ID:
				{
				alt13=1;
				}
				break;
			case FLOAT:
				{
				alt13=2;
				}
				break;
			case INT:
				{
				alt13=3;
				}
				break;
			case LPAREN:
				{
				alt13=4;
				}
				break;
			case DER1:
			case DER2:
				{
				alt13=5;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 13, 0, input);
				throw nvae;
			}
			switch (alt13) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:64:10: ID
					{
					root_0 = (CommonTree)adaptor.nil();


					ID68=(Token)match(input,ID,FOLLOW_ID_in_factor362); 
					ID68_tree = (CommonTree)adaptor.create(ID68);
					adaptor.addChild(root_0, ID68_tree);

					}
					break;
				case 2 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:64:15: FLOAT
					{
					root_0 = (CommonTree)adaptor.nil();


					FLOAT69=(Token)match(input,FLOAT,FOLLOW_FLOAT_in_factor366); 
					FLOAT69_tree = (CommonTree)adaptor.create(FLOAT69);
					adaptor.addChild(root_0, FLOAT69_tree);

					}
					break;
				case 3 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:64:23: INT
					{
					root_0 = (CommonTree)adaptor.nil();


					INT70=(Token)match(input,INT,FOLLOW_INT_in_factor370); 
					INT70_tree = (CommonTree)adaptor.create(INT70);
					adaptor.addChild(root_0, INT70_tree);

					}
					break;
				case 4 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:64:29: ( LPAREN ! expr RPAREN !)
					{
					root_0 = (CommonTree)adaptor.nil();


					// /home/jin/eclipse/EquaDiff/EquaDiff.g:64:29: ( LPAREN ! expr RPAREN !)
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:64:31: LPAREN ! expr RPAREN !
					{
					LPAREN71=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_factor376); 
					pushFollow(FOLLOW_expr_in_factor379);
					expr72=expr();
					state._fsp--;

					adaptor.addChild(root_0, expr72.getTree());

					RPAREN73=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_factor381); 
					}

					}
					break;
				case 5 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:64:56: der
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_der_in_factor388);
					der74=der();
					state._fsp--;

					adaptor.addChild(root_0, der74.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}

		   catch (RecognitionException e) {
		    throw e;
		   }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "factor"

	// Delegated rules



	public static final BitSet FOLLOW_ordre_in_equation47 = new BitSet(new long[]{0x0000001000000000L});
	public static final BitSet FOLLOW_vecteuretat_in_equation49 = new BitSet(new long[]{0x0000000000001000L});
	public static final BitSet FOLLOW_condini_in_equation51 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_cste_in_equation53 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_derivees_in_equation55 = new BitSet(new long[]{0x0000000000080002L});
	public static final BitSet FOLLOW_evt_in_equation57 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ORDRE_in_ordre70 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_AFFECT_in_ordre73 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_INT_in_ordre76 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_SEP_in_ordre78 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VECTETAT_in_vecteuretat90 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_LACC_in_vecteuretat93 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_LPAREN_in_vecteuretat96 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_vecteuretat100 = new BitSet(new long[]{0x0000000080800000L});
	public static final BitSet FOLLOW_RPAREN_in_vecteuretat104 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_SEP_in_vecteuretat107 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_RACC_in_vecteuretat110 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CONDINI_in_condini123 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_LACC_in_condini126 = new BitSet(new long[]{0x000000000080C000L});
	public static final BitSet FOLLOW_affectation_in_condini129 = new BitSet(new long[]{0x000000004080C000L});
	public static final BitSet FOLLOW_RACC_in_condini132 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CSTES_in_cste146 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_LACC_in_cste149 = new BitSet(new long[]{0x000000000080C000L});
	public static final BitSet FOLLOW_affectation_in_cste152 = new BitSet(new long[]{0x000000004080C000L});
	public static final BitSet FOLLOW_RACC_in_cste155 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_EVT_in_evt167 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_LACC_in_evt170 = new BitSet(new long[]{0x0000000000000030L});
	public static final BitSet FOLLOW_exprevt_in_evt173 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_RACC_in_evt175 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ACTION_EVT_in_exprevt187 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_WHEN_in_exprevt190 = new BitSet(new long[]{0x0000000005A0C040L});
	public static final BitSet FOLLOW_expr_in_exprevt193 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_SEP_in_exprevt195 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ACT_RST_ST_in_exprevt201 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_AS_in_exprevt204 = new BitSet(new long[]{0x000000000080C000L});
	public static final BitSet FOLLOW_affectation_in_exprevt207 = new BitSet(new long[]{0x000000200080C000L});
	public static final BitSet FOLLOW_WHEN_in_exprevt210 = new BitSet(new long[]{0x0000000005A0C040L});
	public static final BitSet FOLLOW_expr_in_exprevt213 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_SEP_in_exprevt215 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_affectation228 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_AFFECT_in_affectation230 = new BitSet(new long[]{0x0000000200000000L});
	public static final BitSet FOLLOW_STRING_in_affectation233 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_SEP_in_affectation235 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_der_in_affectation242 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_AFFECT_in_affectation244 = new BitSet(new long[]{0x0000000200000000L});
	public static final BitSet FOLLOW_STRING_in_affectation247 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_SEP_in_affectation249 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DERIV_in_derivees261 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_LACC_in_derivees264 = new BitSet(new long[]{0x000000000080C000L});
	public static final BitSet FOLLOW_affectation_in_derivees268 = new BitSet(new long[]{0x000000004080C000L});
	public static final BitSet FOLLOW_RACC_in_derivees272 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DER1_in_der285 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_DOUBLE_U_in_der288 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_der291 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_DOUBLE_U_in_der293 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DER2_in_der299 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_DOUBLE_U_in_der302 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_der305 = new BitSet(new long[]{0x0000000000020000L});
	public static final BitSet FOLLOW_DOUBLE_U_in_der307 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_term_in_expr320 = new BitSet(new long[]{0x0000000000000042L});
	public static final BitSet FOLLOW_ADDOP_in_expr323 = new BitSet(new long[]{0x0000000005A0C000L});
	public static final BitSet FOLLOW_term_in_expr326 = new BitSet(new long[]{0x0000000000000042L});
	public static final BitSet FOLLOW_ADDOP_in_expr333 = new BitSet(new long[]{0x0000000005A0C040L});
	public static final BitSet FOLLOW_expr_in_expr336 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_factor_in_term346 = new BitSet(new long[]{0x0000000008000002L});
	public static final BitSet FOLLOW_MULTOP_in_term349 = new BitSet(new long[]{0x0000000005A0C000L});
	public static final BitSet FOLLOW_factor_in_term352 = new BitSet(new long[]{0x0000000008000002L});
	public static final BitSet FOLLOW_ID_in_factor362 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FLOAT_in_factor366 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INT_in_factor370 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_factor376 = new BitSet(new long[]{0x0000000005A0C040L});
	public static final BitSet FOLLOW_expr_in_factor379 = new BitSet(new long[]{0x0000000080000000L});
	public static final BitSet FOLLOW_RPAREN_in_factor381 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_der_in_factor388 = new BitSet(new long[]{0x0000000000000002L});
}
