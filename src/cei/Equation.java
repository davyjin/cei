package cei;
import java.io.IOException;
import java.util.ArrayList;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import org.apache.commons.math3.analysis.solvers.BrentSolver;
import org.apache.commons.math3.ode.FirstOrderConverter;
import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;
import org.apache.commons.math3.ode.nonstiff.ClassicalRungeKuttaIntegrator;

import com.graphbuilder.math.*;

public class Equation extends Thread {
	
	private FirstOrderDifferentialEquations equaDiff;
	private ArrayList<String> vectEtat;
	
	private VarMap variables;
	private FuncMap fm;
	
	private double t0;
	private double tFinal;	
	private double h;
	
	private double[] y;
	
	private int ordreEquation;
	
	private ClassicalRungeKuttaIntegrator rg; 
	private Step stepHandler;
	private Evenement eventHandler;
	
	private boolean isOver;
	
	public Equation(String nomFichier, double t0, double tFinal, 
			double h, boolean eventOnIncreasing) throws IOException, RecognitionException {
		// Booléen de contrôle 
		this.isOver = false;
		// Paramètres solveur
		this.t0 = t0;
		this.tFinal = tFinal;
		this.h = h;
		// Création d'un objet lexeur
		EquaDiffLexer lexer = new EquaDiffLexer(new ANTLRFileStream(nomFichier));
		// Création d'un objet parseur
		EquaDiffParser parser = new EquaDiffParser(new CommonTokenStream(lexer));
		// Obtention de l'AST
		Tree arbre = (Tree)parser.equation().getTree();
		// Obtention de l'ordre de l'équation différentielle
		Tree ordre = arbre.getChild(IndiceBloc.INDICEORDRE.ordinal());
		this.ordreEquation = Integer.parseInt(ordre.getChild(0).getText());
		// Création de l'équation selon l'ordre
		if (this.ordreEquation == 2) {
			EquationOrdre2 eq2 = new EquationOrdre2(arbre);
			this.y = eq2.getVect();
			this.vectEtat = eq2.getVectEtat();
			this.variables = eq2.getVarMap();
			this.fm = eq2.getFuncMap();
			this.equaDiff = new FirstOrderConverter(eq2);			
		}
		else {
			EquationOrdre1 eq1 = new EquationOrdre1(arbre);
			this.y = eq1.getVect();
			this.vectEtat = eq1.getVectEtat();
			this.equaDiff = eq1;
		}
		this.rg = new ClassicalRungeKuttaIntegrator(this.h);		
		this.stepHandler = new Step(this.t0, this.vectEtat, this.variables);
		this.rg.addStepHandler(this.stepHandler);
		this.eventHandler = null;
		// Test si événement à gérer
		if (arbre.getChildCount() > 5) {
			Tree action = arbre.getChild(IndiceBloc.INDICEEVT.ordinal()).getChild(0);
			Tree rst_state = action.getChild(0);
			Tree condition = action.getChild(1);
			this.eventHandler = new Evenement(rst_state, condition, action.getText(), 
					this.vectEtat, this.variables, this.fm, eventOnIncreasing);
			this.rg.addEventHandler(this.eventHandler, 0.02, 1e-9, 50000, new BrentSolver(1e-9));
		}
	}
	
	public void integrate() {
		this.start();
	}
	
	public void run() {
		this.rg.integrate(this.equaDiff, this.t0, this.y, this.tFinal, this.y);
		this.isOver = true;
	}
	
	public Step getStepHandler() {
		return this.stepHandler;
	}
	
	public boolean isOver() {
		return this.isOver;
	}
	
	public double getCurrentTime() {
		return this.stepHandler.getCurrentTime();
	}
	
	public double[] getCurrentState() {
		return this.stepHandler.getCurrentState();
	}
	
	public double getCurrentEvent() {
		if (this.eventHandler != null)
			return this.eventHandler.getEventSignal();
		else
			return 0.0;
	}
	
	public void update() throws InterruptedException {
		this.stepHandler.stepStart();
		this.stepHandler.stepEnd();		
	}
	
	public boolean doVariableExists(String name) {
		String[] tab = this.variables.getVariableNames();
		for (String s : tab) {
			if (s.equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	public void updateVariable(String name, double value) {
		if (!doVariableExists(name))
			return;
		else
			this.variables.setValue(name, value);
	}	
 }
