package cei;
import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.ode.sampling.StepHandler;
import org.apache.commons.math3.ode.sampling.StepInterpolator;

import com.graphbuilder.math.VarMap;


public class Step implements StepHandler {
	private FileWriter outputFile;
	private VarMap variables;
	private ArrayList<String> vectEtat;
	private String nomFichier = null;
	private Semaphore stepStart;
	private Semaphore stepEnd;
	private double currentTime;
	private double[] currentState;
	private boolean isLast;
	
	// Deprecated
	public Step(double t0) {
		this.currentTime = t0;
		this.initSemaphore();
	}
	
	public Step(double t0, ArrayList<String> vectEtat, VarMap varMap) {
		this.currentTime = t0;
		this.vectEtat = vectEtat;
		this.variables = varMap;
		this.initSemaphore();
	}
	
	public Step(String nomFichier, double t0) {
		this.currentTime = t0;
		this.nomFichier = nomFichier;
		this.initSemaphore();		
	}
	
	private void initSemaphore() {
		this.stepStart = new Semaphore(0);
		this.stepEnd = new Semaphore(0);
	}
	
	@Override
	public void handleStep(StepInterpolator interpolator, boolean isLast)
			throws MaxCountExceededException {
		this.isLast = isLast;
		try {
			this.stepStart.acquire();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		this.currentTime = interpolator.getCurrentTime();
        this.currentState = interpolator.getInterpolatedState();
        this.miseAjourTableVar(this.currentState);
        try {
        	// System.out.println("t = " + this.currentTime + " " + "y = " + this.currentState[0]);
			this.outputFile.write(this.currentTime + " " + this.currentState[0] + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
        if (isLast) {        	
        	try {
				this.closeFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
        this.stepEnd.release();
	}
	
	private void closeFile() throws IOException {
		this.outputFile.close();
	}

	private void miseAjourTableVar(double[] y) {
		int sizeVector = y.length;		
		for (String varEtat : this.vectEtat) {			
			String der1 = "der1::" + varEtat + "::";
			String der2 = "der2::" + varEtat + "::";			
			String[] tabVar = {varEtat, der1, der2};
			for (int i = 0; i < sizeVector; i++) {
				this.variables.setValue(tabVar[i], y[i]);
			}
		}
	}
	
	@Override
	public void init(double t0, double[] y0, double t) {
		try {
			if (this.nomFichier != null) {
				this.outputFile = new FileWriter(new File(this.nomFichier));
			}
			else {
				this.outputFile = new FileWriter(new File("resultats.txt"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	
	public void stepStart() {		
		if (this.isLast == false) {
			// System.out.println("Step Start");
			this.stepStart.release();
		}
	}
	
	public void stepEnd() throws InterruptedException {
		if (this.isLast == false) {
			this.stepEnd.acquire();
			// System.out.println("Step End");
		}
	}
	
	public double getCurrentTime() {
		return this.currentTime;
	}
	
	public double[] getCurrentState() {
		return this.currentState.clone();
	}
}
