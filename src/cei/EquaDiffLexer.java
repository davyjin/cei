package cei;
// $ANTLR 3.5.1 /home/jin/eclipse/EquaDiff/EquaDiff.g 2014-02-07 13:34:13

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class EquaDiffLexer extends Lexer {
	public static final int EOF=-1;
	public static final int ACTION_EVT=4;
	public static final int ACT_RST_ST=5;
	public static final int ADDOP=6;
	public static final int AFFECT=7;
	public static final int AS=8;
	public static final int CHAR=9;
	public static final int COMMA=10;
	public static final int COMMENT=11;
	public static final int CONDINI=12;
	public static final int CSTES=13;
	public static final int DER1=14;
	public static final int DER2=15;
	public static final int DERIV=16;
	public static final int DOUBLE_U=17;
	public static final int ESC_SEQ=18;
	public static final int EVT=19;
	public static final int EXPONENT=20;
	public static final int FLOAT=21;
	public static final int HEX_DIGIT=22;
	public static final int ID=23;
	public static final int INT=24;
	public static final int LACC=25;
	public static final int LPAREN=26;
	public static final int MULTOP=27;
	public static final int OCTAL_ESC=28;
	public static final int ORDRE=29;
	public static final int RACC=30;
	public static final int RPAREN=31;
	public static final int SEP=32;
	public static final int STRING=33;
	public static final int THEN=34;
	public static final int UNICODE_ESC=35;
	public static final int VECTETAT=36;
	public static final int WHEN=37;
	public static final int WS=38;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public EquaDiffLexer() {} 
	public EquaDiffLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public EquaDiffLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/home/jin/eclipse/EquaDiff/EquaDiff.g"; }

	// $ANTLR start "DER1"
	public final void mDER1() throws RecognitionException {
		try {
			int _type = DER1;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:67:2: ( 'der1' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:67:4: 'der1'
			{
			match("der1"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DER1"

	// $ANTLR start "DER2"
	public final void mDER2() throws RecognitionException {
		try {
			int _type = DER2;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:71:2: ( 'der2' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:71:4: 'der2'
			{
			match("der2"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DER2"

	// $ANTLR start "ORDRE"
	public final void mORDRE() throws RecognitionException {
		try {
			int _type = ORDRE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:75:2: ( 'Ordre' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:75:4: 'Ordre'
			{
			match("Ordre"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ORDRE"

	// $ANTLR start "LPAREN"
	public final void mLPAREN() throws RecognitionException {
		try {
			int _type = LPAREN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:79:2: ( '(' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:79:5: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LPAREN"

	// $ANTLR start "RPAREN"
	public final void mRPAREN() throws RecognitionException {
		try {
			int _type = RPAREN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:83:2: ( ')' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:83:5: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RPAREN"

	// $ANTLR start "DOUBLE_U"
	public final void mDOUBLE_U() throws RecognitionException {
		try {
			int _type = DOUBLE_U;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:87:2: ( '::' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:87:4: '::'
			{
			match("::"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DOUBLE_U"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:91:2: ( ',' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:91:5: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "DERIV"
	public final void mDERIV() throws RecognitionException {
		try {
			int _type = DERIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:95:2: ( 'Derivees' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:95:4: 'Derivees'
			{
			match("Derivees"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DERIV"

	// $ANTLR start "VECTETAT"
	public final void mVECTETAT() throws RecognitionException {
		try {
			int _type = VECTETAT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:99:2: ( 'VectEtat' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:99:4: 'VectEtat'
			{
			match("VectEtat"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VECTETAT"

	// $ANTLR start "CSTES"
	public final void mCSTES() throws RecognitionException {
		try {
			int _type = CSTES;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:103:2: ( 'Cstes' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:103:4: 'Cstes'
			{
			match("Cstes"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CSTES"

	// $ANTLR start "CONDINI"
	public final void mCONDINI() throws RecognitionException {
		try {
			int _type = CONDINI;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:107:2: ( 'CondIni' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:107:4: 'CondIni'
			{
			match("CondIni"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CONDINI"

	// $ANTLR start "EVT"
	public final void mEVT() throws RecognitionException {
		try {
			int _type = EVT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:111:2: ( 'Evenement' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:111:4: 'Evenement'
			{
			match("Evenement"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EVT"

	// $ANTLR start "WHEN"
	public final void mWHEN() throws RecognitionException {
		try {
			int _type = WHEN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:115:2: ( 'when' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:115:4: 'when'
			{
			match("when"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHEN"

	// $ANTLR start "THEN"
	public final void mTHEN() throws RecognitionException {
		try {
			int _type = THEN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:119:2: ( 'then' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:119:4: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "THEN"

	// $ANTLR start "AS"
	public final void mAS() throws RecognitionException {
		try {
			int _type = AS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:123:2: ( 'as' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:123:4: 'as'
			{
			match("as"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AS"

	// $ANTLR start "ACTION_EVT"
	public final void mACTION_EVT() throws RecognitionException {
		try {
			int _type = ACTION_EVT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:127:2: ( 'RESET_DER' | 'STOP' )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0=='R') ) {
				alt1=1;
			}
			else if ( (LA1_0=='S') ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:127:4: 'RESET_DER'
					{
					match("RESET_DER"); 

					}
					break;
				case 2 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:128:4: 'STOP'
					{
					match("STOP"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ACTION_EVT"

	// $ANTLR start "ACT_RST_ST"
	public final void mACT_RST_ST() throws RecognitionException {
		try {
			int _type = ACT_RST_ST;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:132:2: ( 'RESET_STATE' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:132:4: 'RESET_STATE'
			{
			match("RESET_STATE"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ACT_RST_ST"

	// $ANTLR start "LACC"
	public final void mLACC() throws RecognitionException {
		try {
			int _type = LACC;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:136:2: ( '{' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:136:4: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LACC"

	// $ANTLR start "RACC"
	public final void mRACC() throws RecognitionException {
		try {
			int _type = RACC;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:140:2: ( '}' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:140:4: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RACC"

	// $ANTLR start "AFFECT"
	public final void mAFFECT() throws RecognitionException {
		try {
			int _type = AFFECT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:144:2: ( '=' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:144:5: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AFFECT"

	// $ANTLR start "SEP"
	public final void mSEP() throws RecognitionException {
		try {
			int _type = SEP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:147:5: ( ';' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:147:7: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEP"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:149:5: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:149:7: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:149:31: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '0' && LA2_0 <= '9')||(LA2_0 >= 'A' && LA2_0 <= 'Z')||LA2_0=='_'||(LA2_0 >= 'a' && LA2_0 <= 'z')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop2;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:152:5: ( ( '0' .. '9' )+ )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:152:7: ( '0' .. '9' )+
			{
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:152:7: ( '0' .. '9' )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "FLOAT"
	public final void mFLOAT() throws RecognitionException {
		try {
			int _type = FLOAT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:156:5: ( ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( EXPONENT )? | '.' ( '0' .. '9' )+ ( EXPONENT )? | ( '0' .. '9' )+ EXPONENT )
			int alt10=3;
			alt10 = dfa10.predict(input);
			switch (alt10) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:156:9: ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( EXPONENT )?
					{
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:156:9: ( '0' .. '9' )+
					int cnt4=0;
					loop4:
					while (true) {
						int alt4=2;
						int LA4_0 = input.LA(1);
						if ( ((LA4_0 >= '0' && LA4_0 <= '9')) ) {
							alt4=1;
						}

						switch (alt4) {
						case 1 :
							// /home/jin/eclipse/EquaDiff/EquaDiff.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt4 >= 1 ) break loop4;
							EarlyExitException eee = new EarlyExitException(4, input);
							throw eee;
						}
						cnt4++;
					}

					match('.'); 
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:156:25: ( '0' .. '9' )*
					loop5:
					while (true) {
						int alt5=2;
						int LA5_0 = input.LA(1);
						if ( ((LA5_0 >= '0' && LA5_0 <= '9')) ) {
							alt5=1;
						}

						switch (alt5) {
						case 1 :
							// /home/jin/eclipse/EquaDiff/EquaDiff.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop5;
						}
					}

					// /home/jin/eclipse/EquaDiff/EquaDiff.g:156:37: ( EXPONENT )?
					int alt6=2;
					int LA6_0 = input.LA(1);
					if ( (LA6_0=='E'||LA6_0=='e') ) {
						alt6=1;
					}
					switch (alt6) {
						case 1 :
							// /home/jin/eclipse/EquaDiff/EquaDiff.g:156:37: EXPONENT
							{
							mEXPONENT(); 

							}
							break;

					}

					}
					break;
				case 2 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:157:9: '.' ( '0' .. '9' )+ ( EXPONENT )?
					{
					match('.'); 
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:157:13: ( '0' .. '9' )+
					int cnt7=0;
					loop7:
					while (true) {
						int alt7=2;
						int LA7_0 = input.LA(1);
						if ( ((LA7_0 >= '0' && LA7_0 <= '9')) ) {
							alt7=1;
						}

						switch (alt7) {
						case 1 :
							// /home/jin/eclipse/EquaDiff/EquaDiff.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt7 >= 1 ) break loop7;
							EarlyExitException eee = new EarlyExitException(7, input);
							throw eee;
						}
						cnt7++;
					}

					// /home/jin/eclipse/EquaDiff/EquaDiff.g:157:25: ( EXPONENT )?
					int alt8=2;
					int LA8_0 = input.LA(1);
					if ( (LA8_0=='E'||LA8_0=='e') ) {
						alt8=1;
					}
					switch (alt8) {
						case 1 :
							// /home/jin/eclipse/EquaDiff/EquaDiff.g:157:25: EXPONENT
							{
							mEXPONENT(); 

							}
							break;

					}

					}
					break;
				case 3 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:158:9: ( '0' .. '9' )+ EXPONENT
					{
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:158:9: ( '0' .. '9' )+
					int cnt9=0;
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( ((LA9_0 >= '0' && LA9_0 <= '9')) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// /home/jin/eclipse/EquaDiff/EquaDiff.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt9 >= 1 ) break loop9;
							EarlyExitException eee = new EarlyExitException(9, input);
							throw eee;
						}
						cnt9++;
					}

					mEXPONENT(); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FLOAT"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:162:5: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' | '/*' ( options {greedy=false; } : . )* '*/' )
			int alt14=2;
			int LA14_0 = input.LA(1);
			if ( (LA14_0=='/') ) {
				int LA14_1 = input.LA(2);
				if ( (LA14_1=='/') ) {
					alt14=1;
				}
				else if ( (LA14_1=='*') ) {
					alt14=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 14, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 14, 0, input);
				throw nvae;
			}

			switch (alt14) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:162:9: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
					{
					match("//"); 

					// /home/jin/eclipse/EquaDiff/EquaDiff.g:162:14: (~ ( '\\n' | '\\r' ) )*
					loop11:
					while (true) {
						int alt11=2;
						int LA11_0 = input.LA(1);
						if ( ((LA11_0 >= '\u0000' && LA11_0 <= '\t')||(LA11_0 >= '\u000B' && LA11_0 <= '\f')||(LA11_0 >= '\u000E' && LA11_0 <= '\uFFFF')) ) {
							alt11=1;
						}

						switch (alt11) {
						case 1 :
							// /home/jin/eclipse/EquaDiff/EquaDiff.g:
							{
							if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop11;
						}
					}

					// /home/jin/eclipse/EquaDiff/EquaDiff.g:162:28: ( '\\r' )?
					int alt12=2;
					int LA12_0 = input.LA(1);
					if ( (LA12_0=='\r') ) {
						alt12=1;
					}
					switch (alt12) {
						case 1 :
							// /home/jin/eclipse/EquaDiff/EquaDiff.g:162:28: '\\r'
							{
							match('\r'); 
							}
							break;

					}

					match('\n'); 
					_channel=HIDDEN;
					}
					break;
				case 2 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:163:9: '/*' ( options {greedy=false; } : . )* '*/'
					{
					match("/*"); 

					// /home/jin/eclipse/EquaDiff/EquaDiff.g:163:14: ( options {greedy=false; } : . )*
					loop13:
					while (true) {
						int alt13=2;
						int LA13_0 = input.LA(1);
						if ( (LA13_0=='*') ) {
							int LA13_1 = input.LA(2);
							if ( (LA13_1=='/') ) {
								alt13=2;
							}
							else if ( ((LA13_1 >= '\u0000' && LA13_1 <= '.')||(LA13_1 >= '0' && LA13_1 <= '\uFFFF')) ) {
								alt13=1;
							}

						}
						else if ( ((LA13_0 >= '\u0000' && LA13_0 <= ')')||(LA13_0 >= '+' && LA13_0 <= '\uFFFF')) ) {
							alt13=1;
						}

						switch (alt13) {
						case 1 :
							// /home/jin/eclipse/EquaDiff/EquaDiff.g:163:42: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop13;
						}
					}

					match("*/"); 

					_channel=HIDDEN;
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:166:5: ( ( ' ' | '\\t' | '\\r' | '\\n' ) )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:166:9: ( ' ' | '\\t' | '\\r' | '\\n' )
			{
			if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "ADDOP"
	public final void mADDOP() throws RecognitionException {
		try {
			int _type = ADDOP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:173:7: ( '+' | '-' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:
			{
			if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ADDOP"

	// $ANTLR start "MULTOP"
	public final void mMULTOP() throws RecognitionException {
		try {
			int _type = MULTOP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:175:8: ( '*' | '/' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:
			{
			if ( input.LA(1)=='*'||input.LA(1)=='/' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MULTOP"

	// $ANTLR start "STRING"
	public final void mSTRING() throws RecognitionException {
		try {
			int _type = STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:178:5: ( '\"' ( ESC_SEQ |~ ( '\\\\' | '\"' ) )* '\"' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:178:8: '\"' ( ESC_SEQ |~ ( '\\\\' | '\"' ) )* '\"'
			{
			match('\"'); 
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:178:12: ( ESC_SEQ |~ ( '\\\\' | '\"' ) )*
			loop15:
			while (true) {
				int alt15=3;
				int LA15_0 = input.LA(1);
				if ( (LA15_0=='\\') ) {
					alt15=1;
				}
				else if ( ((LA15_0 >= '\u0000' && LA15_0 <= '!')||(LA15_0 >= '#' && LA15_0 <= '[')||(LA15_0 >= ']' && LA15_0 <= '\uFFFF')) ) {
					alt15=2;
				}

				switch (alt15) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:178:14: ESC_SEQ
					{
					mESC_SEQ(); 

					}
					break;
				case 2 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:178:24: ~ ( '\\\\' | '\"' )
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop15;
				}
			}

			match('\"'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRING"

	// $ANTLR start "CHAR"
	public final void mCHAR() throws RecognitionException {
		try {
			int _type = CHAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:181:5: ( '\\'' ( ESC_SEQ |~ ( '\\'' | '\\\\' ) ) '\\'' )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:181:8: '\\'' ( ESC_SEQ |~ ( '\\'' | '\\\\' ) ) '\\''
			{
			match('\''); 
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:181:13: ( ESC_SEQ |~ ( '\\'' | '\\\\' ) )
			int alt16=2;
			int LA16_0 = input.LA(1);
			if ( (LA16_0=='\\') ) {
				alt16=1;
			}
			else if ( ((LA16_0 >= '\u0000' && LA16_0 <= '&')||(LA16_0 >= '(' && LA16_0 <= '[')||(LA16_0 >= ']' && LA16_0 <= '\uFFFF')) ) {
				alt16=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 16, 0, input);
				throw nvae;
			}

			switch (alt16) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:181:15: ESC_SEQ
					{
					mESC_SEQ(); 

					}
					break;
				case 2 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:181:25: ~ ( '\\'' | '\\\\' )
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			match('\''); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CHAR"

	// $ANTLR start "EXPONENT"
	public final void mEXPONENT() throws RecognitionException {
		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:186:10: ( ( 'e' | 'E' ) ( '+' | '-' )? ( '0' .. '9' )+ )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:186:12: ( 'e' | 'E' ) ( '+' | '-' )? ( '0' .. '9' )+
			{
			if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:186:22: ( '+' | '-' )?
			int alt17=2;
			int LA17_0 = input.LA(1);
			if ( (LA17_0=='+'||LA17_0=='-') ) {
				alt17=1;
			}
			switch (alt17) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:
					{
					if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			// /home/jin/eclipse/EquaDiff/EquaDiff.g:186:33: ( '0' .. '9' )+
			int cnt18=0;
			loop18:
			while (true) {
				int alt18=2;
				int LA18_0 = input.LA(1);
				if ( ((LA18_0 >= '0' && LA18_0 <= '9')) ) {
					alt18=1;
				}

				switch (alt18) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt18 >= 1 ) break loop18;
					EarlyExitException eee = new EarlyExitException(18, input);
					throw eee;
				}
				cnt18++;
			}

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXPONENT"

	// $ANTLR start "HEX_DIGIT"
	public final void mHEX_DIGIT() throws RecognitionException {
		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:189:11: ( ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' ) )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HEX_DIGIT"

	// $ANTLR start "ESC_SEQ"
	public final void mESC_SEQ() throws RecognitionException {
		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:193:5: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' ) | UNICODE_ESC | OCTAL_ESC )
			int alt19=3;
			int LA19_0 = input.LA(1);
			if ( (LA19_0=='\\') ) {
				switch ( input.LA(2) ) {
				case '\"':
				case '\'':
				case '\\':
				case 'b':
				case 'f':
				case 'n':
				case 'r':
				case 't':
					{
					alt19=1;
					}
					break;
				case 'u':
					{
					alt19=2;
					}
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
					{
					alt19=3;
					}
					break;
				default:
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 19, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 19, 0, input);
				throw nvae;
			}

			switch (alt19) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:193:9: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' )
					{
					match('\\'); 
					if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||input.LA(1)=='t' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 2 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:194:9: UNICODE_ESC
					{
					mUNICODE_ESC(); 

					}
					break;
				case 3 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:195:9: OCTAL_ESC
					{
					mOCTAL_ESC(); 

					}
					break;

			}
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ESC_SEQ"

	// $ANTLR start "OCTAL_ESC"
	public final void mOCTAL_ESC() throws RecognitionException {
		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:200:5: ( '\\\\' ( '0' .. '3' ) ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\' ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\' ( '0' .. '7' ) )
			int alt20=3;
			int LA20_0 = input.LA(1);
			if ( (LA20_0=='\\') ) {
				int LA20_1 = input.LA(2);
				if ( ((LA20_1 >= '0' && LA20_1 <= '3')) ) {
					int LA20_2 = input.LA(3);
					if ( ((LA20_2 >= '0' && LA20_2 <= '7')) ) {
						int LA20_4 = input.LA(4);
						if ( ((LA20_4 >= '0' && LA20_4 <= '7')) ) {
							alt20=1;
						}

						else {
							alt20=2;
						}

					}

					else {
						alt20=3;
					}

				}
				else if ( ((LA20_1 >= '4' && LA20_1 <= '7')) ) {
					int LA20_3 = input.LA(3);
					if ( ((LA20_3 >= '0' && LA20_3 <= '7')) ) {
						alt20=2;
					}

					else {
						alt20=3;
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 20, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 20, 0, input);
				throw nvae;
			}

			switch (alt20) {
				case 1 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:200:9: '\\\\' ( '0' .. '3' ) ( '0' .. '7' ) ( '0' .. '7' )
					{
					match('\\'); 
					if ( (input.LA(1) >= '0' && input.LA(1) <= '3') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 2 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:201:9: '\\\\' ( '0' .. '7' ) ( '0' .. '7' )
					{
					match('\\'); 
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 3 :
					// /home/jin/eclipse/EquaDiff/EquaDiff.g:202:9: '\\\\' ( '0' .. '7' )
					{
					match('\\'); 
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OCTAL_ESC"

	// $ANTLR start "UNICODE_ESC"
	public final void mUNICODE_ESC() throws RecognitionException {
		try {
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:207:5: ( '\\\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT )
			// /home/jin/eclipse/EquaDiff/EquaDiff.g:207:9: '\\\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
			{
			match('\\'); 
			match('u'); 
			mHEX_DIGIT(); 

			mHEX_DIGIT(); 

			mHEX_DIGIT(); 

			mHEX_DIGIT(); 

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "UNICODE_ESC"

	@Override
	public void mTokens() throws RecognitionException {
		// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:8: ( DER1 | DER2 | ORDRE | LPAREN | RPAREN | DOUBLE_U | COMMA | DERIV | VECTETAT | CSTES | CONDINI | EVT | WHEN | THEN | AS | ACTION_EVT | ACT_RST_ST | LACC | RACC | AFFECT | SEP | ID | INT | FLOAT | COMMENT | WS | ADDOP | MULTOP | STRING | CHAR )
		int alt21=30;
		alt21 = dfa21.predict(input);
		switch (alt21) {
			case 1 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:10: DER1
				{
				mDER1(); 

				}
				break;
			case 2 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:15: DER2
				{
				mDER2(); 

				}
				break;
			case 3 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:20: ORDRE
				{
				mORDRE(); 

				}
				break;
			case 4 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:26: LPAREN
				{
				mLPAREN(); 

				}
				break;
			case 5 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:33: RPAREN
				{
				mRPAREN(); 

				}
				break;
			case 6 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:40: DOUBLE_U
				{
				mDOUBLE_U(); 

				}
				break;
			case 7 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:49: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 8 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:55: DERIV
				{
				mDERIV(); 

				}
				break;
			case 9 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:61: VECTETAT
				{
				mVECTETAT(); 

				}
				break;
			case 10 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:70: CSTES
				{
				mCSTES(); 

				}
				break;
			case 11 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:76: CONDINI
				{
				mCONDINI(); 

				}
				break;
			case 12 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:84: EVT
				{
				mEVT(); 

				}
				break;
			case 13 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:88: WHEN
				{
				mWHEN(); 

				}
				break;
			case 14 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:93: THEN
				{
				mTHEN(); 

				}
				break;
			case 15 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:98: AS
				{
				mAS(); 

				}
				break;
			case 16 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:101: ACTION_EVT
				{
				mACTION_EVT(); 

				}
				break;
			case 17 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:112: ACT_RST_ST
				{
				mACT_RST_ST(); 

				}
				break;
			case 18 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:123: LACC
				{
				mLACC(); 

				}
				break;
			case 19 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:128: RACC
				{
				mRACC(); 

				}
				break;
			case 20 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:133: AFFECT
				{
				mAFFECT(); 

				}
				break;
			case 21 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:140: SEP
				{
				mSEP(); 

				}
				break;
			case 22 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:144: ID
				{
				mID(); 

				}
				break;
			case 23 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:147: INT
				{
				mINT(); 

				}
				break;
			case 24 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:151: FLOAT
				{
				mFLOAT(); 

				}
				break;
			case 25 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:157: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 26 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:165: WS
				{
				mWS(); 

				}
				break;
			case 27 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:168: ADDOP
				{
				mADDOP(); 

				}
				break;
			case 28 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:174: MULTOP
				{
				mMULTOP(); 

				}
				break;
			case 29 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:181: STRING
				{
				mSTRING(); 

				}
				break;
			case 30 :
				// /home/jin/eclipse/EquaDiff/EquaDiff.g:1:188: CHAR
				{
				mCHAR(); 

				}
				break;

		}
	}


	protected DFA10 dfa10 = new DFA10(this);
	protected DFA21 dfa21 = new DFA21(this);
	static final String DFA10_eotS =
		"\5\uffff";
	static final String DFA10_eofS =
		"\5\uffff";
	static final String DFA10_minS =
		"\2\56\3\uffff";
	static final String DFA10_maxS =
		"\1\71\1\145\3\uffff";
	static final String DFA10_acceptS =
		"\2\uffff\1\2\1\1\1\3";
	static final String DFA10_specialS =
		"\5\uffff}>";
	static final String[] DFA10_transitionS = {
			"\1\2\1\uffff\12\1",
			"\1\3\1\uffff\12\1\13\uffff\1\4\37\uffff\1\4",
			"",
			"",
			""
	};

	static final short[] DFA10_eot = DFA.unpackEncodedString(DFA10_eotS);
	static final short[] DFA10_eof = DFA.unpackEncodedString(DFA10_eofS);
	static final char[] DFA10_min = DFA.unpackEncodedStringToUnsignedChars(DFA10_minS);
	static final char[] DFA10_max = DFA.unpackEncodedStringToUnsignedChars(DFA10_maxS);
	static final short[] DFA10_accept = DFA.unpackEncodedString(DFA10_acceptS);
	static final short[] DFA10_special = DFA.unpackEncodedString(DFA10_specialS);
	static final short[][] DFA10_transition;

	static {
		int numStates = DFA10_transitionS.length;
		DFA10_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA10_transition[i] = DFA.unpackEncodedString(DFA10_transitionS[i]);
		}
	}

	protected class DFA10 extends DFA {

		public DFA10(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 10;
			this.eot = DFA10_eot;
			this.eof = DFA10_eof;
			this.min = DFA10_min;
			this.max = DFA10_max;
			this.accept = DFA10_accept;
			this.special = DFA10_special;
			this.transition = DFA10_transition;
		}
		@Override
		public String getDescription() {
			return "155:1: FLOAT : ( ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( EXPONENT )? | '.' ( '0' .. '9' )+ ( EXPONENT )? | ( '0' .. '9' )+ EXPONENT );";
		}
	}

	static final String DFA21_eotS =
		"\1\uffff\2\24\4\uffff\11\24\5\uffff\1\51\1\uffff\1\32\5\uffff\11\24\1"+
		"\64\2\24\2\uffff\11\24\1\uffff\2\24\1\103\1\104\6\24\1\113\1\114\1\24"+
		"\1\116\2\uffff\1\117\2\24\1\122\2\24\2\uffff\1\24\2\uffff\2\24\1\uffff"+
		"\5\24\1\136\3\24\1\142\1\143\1\uffff\3\24\2\uffff\1\147\1\116\1\24\1\uffff"+
		"\1\24\1\152\1\uffff";
	static final String DFA21_eofS =
		"\153\uffff";
	static final String DFA21_minS =
		"\1\11\1\145\1\162\4\uffff\2\145\1\157\1\166\2\150\1\163\1\105\1\124\5"+
		"\uffff\1\56\1\uffff\1\52\5\uffff\1\162\1\144\1\162\1\143\1\164\1\156\3"+
		"\145\1\60\1\123\1\117\2\uffff\1\61\1\162\1\151\1\164\1\145\1\144\3\156"+
		"\1\uffff\1\105\1\120\2\60\1\145\1\166\1\105\1\163\1\111\1\145\2\60\1\124"+
		"\1\60\2\uffff\1\60\1\145\1\164\1\60\1\156\1\155\2\uffff\1\137\2\uffff"+
		"\1\145\1\141\1\uffff\1\151\1\145\1\104\1\163\1\164\1\60\1\156\1\105\1"+
		"\124\2\60\1\uffff\1\164\1\122\1\101\2\uffff\2\60\1\124\1\uffff\1\105\1"+
		"\60\1\uffff";
	static final String DFA21_maxS =
		"\1\175\1\145\1\162\4\uffff\2\145\1\163\1\166\2\150\1\163\1\105\1\124\5"+
		"\uffff\1\145\1\uffff\1\57\5\uffff\1\162\1\144\1\162\1\143\1\164\1\156"+
		"\3\145\1\172\1\123\1\117\2\uffff\1\62\1\162\1\151\1\164\1\145\1\144\3"+
		"\156\1\uffff\1\105\1\120\2\172\1\145\1\166\1\105\1\163\1\111\1\145\2\172"+
		"\1\124\1\172\2\uffff\1\172\1\145\1\164\1\172\1\156\1\155\2\uffff\1\137"+
		"\2\uffff\1\145\1\141\1\uffff\1\151\1\145\1\123\1\163\1\164\1\172\1\156"+
		"\1\105\1\124\2\172\1\uffff\1\164\1\122\1\101\2\uffff\2\172\1\124\1\uffff"+
		"\1\105\1\172\1\uffff";
	static final String DFA21_acceptS =
		"\3\uffff\1\4\1\5\1\6\1\7\11\uffff\1\22\1\23\1\24\1\25\1\26\1\uffff\1\30"+
		"\1\uffff\1\32\1\33\1\34\1\35\1\36\14\uffff\1\27\1\31\11\uffff\1\17\16"+
		"\uffff\1\1\1\2\6\uffff\1\15\1\16\1\uffff\1\20\1\3\2\uffff\1\12\13\uffff"+
		"\1\13\3\uffff\1\10\1\11\3\uffff\1\14\2\uffff\1\21";
	static final String DFA21_specialS =
		"\153\uffff}>";
	static final String[] DFA21_transitionS = {
			"\2\30\2\uffff\1\30\22\uffff\1\30\1\uffff\1\33\4\uffff\1\34\1\3\1\4\1"+
			"\32\1\31\1\6\1\31\1\26\1\27\12\25\1\5\1\23\1\uffff\1\22\3\uffff\2\24"+
			"\1\11\1\7\1\12\11\24\1\2\2\24\1\16\1\17\2\24\1\10\4\24\4\uffff\1\24\1"+
			"\uffff\1\15\2\24\1\1\17\24\1\14\2\24\1\13\3\24\1\20\1\uffff\1\21",
			"\1\35",
			"\1\36",
			"",
			"",
			"",
			"",
			"\1\37",
			"\1\40",
			"\1\42\3\uffff\1\41",
			"\1\43",
			"\1\44",
			"\1\45",
			"\1\46",
			"\1\47",
			"\1\50",
			"",
			"",
			"",
			"",
			"",
			"\1\26\1\uffff\12\25\13\uffff\1\26\37\uffff\1\26",
			"",
			"\1\52\4\uffff\1\52",
			"",
			"",
			"",
			"",
			"",
			"\1\53",
			"\1\54",
			"\1\55",
			"\1\56",
			"\1\57",
			"\1\60",
			"\1\61",
			"\1\62",
			"\1\63",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			"\1\65",
			"\1\66",
			"",
			"",
			"\1\67\1\70",
			"\1\71",
			"\1\72",
			"\1\73",
			"\1\74",
			"\1\75",
			"\1\76",
			"\1\77",
			"\1\100",
			"",
			"\1\101",
			"\1\102",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			"\1\105",
			"\1\106",
			"\1\107",
			"\1\110",
			"\1\111",
			"\1\112",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			"\1\115",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			"",
			"",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			"\1\120",
			"\1\121",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			"\1\123",
			"\1\124",
			"",
			"",
			"\1\125",
			"",
			"",
			"\1\126",
			"\1\127",
			"",
			"\1\130",
			"\1\131",
			"\1\132\16\uffff\1\133",
			"\1\134",
			"\1\135",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			"\1\137",
			"\1\140",
			"\1\141",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			"",
			"\1\144",
			"\1\145",
			"\1\146",
			"",
			"",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			"\1\150",
			"",
			"\1\151",
			"\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
			""
	};

	static final short[] DFA21_eot = DFA.unpackEncodedString(DFA21_eotS);
	static final short[] DFA21_eof = DFA.unpackEncodedString(DFA21_eofS);
	static final char[] DFA21_min = DFA.unpackEncodedStringToUnsignedChars(DFA21_minS);
	static final char[] DFA21_max = DFA.unpackEncodedStringToUnsignedChars(DFA21_maxS);
	static final short[] DFA21_accept = DFA.unpackEncodedString(DFA21_acceptS);
	static final short[] DFA21_special = DFA.unpackEncodedString(DFA21_specialS);
	static final short[][] DFA21_transition;

	static {
		int numStates = DFA21_transitionS.length;
		DFA21_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA21_transition[i] = DFA.unpackEncodedString(DFA21_transitionS[i]);
		}
	}

	protected class DFA21 extends DFA {

		public DFA21(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 21;
			this.eot = DFA21_eot;
			this.eof = DFA21_eof;
			this.min = DFA21_min;
			this.max = DFA21_max;
			this.accept = DFA21_accept;
			this.special = DFA21_special;
			this.transition = DFA21_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( DER1 | DER2 | ORDRE | LPAREN | RPAREN | DOUBLE_U | COMMA | DERIV | VECTETAT | CSTES | CONDINI | EVT | WHEN | THEN | AS | ACTION_EVT | ACT_RST_ST | LACC | RACC | AFFECT | SEP | ID | INT | FLOAT | COMMENT | WS | ADDOP | MULTOP | STRING | CHAR );";
		}
	}

}
