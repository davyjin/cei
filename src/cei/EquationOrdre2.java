package cei;
import org.antlr.runtime.tree.Tree;
import org.apache.commons.math3.ode.SecondOrderDifferentialEquations;

import com.graphbuilder.math.ExpressionTree;


public class EquationOrdre2 extends EquationDiff implements 
		SecondOrderDifferentialEquations {
	
	public EquationOrdre2(Tree arbre) {
		super(arbre);
	}

	@Override
	public void computeSecondDerivatives(double t, double[] y,
			double[] yDot, double[] yDDot) {
		Tree racineDerivees = this.getTree().getChild(IndiceBloc.INDICEDERIVEES.ordinal());
		for (int i = 0; i < racineDerivees.getChildCount(); i++) {
			this.parcoursBrancheDerivees(racineDerivees.getChild(i), y, yDot, yDDot);
		}		
	}

	@Override
	public int getDimension() {
		return this.vectEtat.size();
	}
	
	public void parcoursBrancheDerivees(Tree racineAffect, double[] y, 
			double[] yDot, double[] yDDot) {
		Tree racineDerivees = racineAffect.getChild(0);
		Tree racineOp = racineAffect.getChild(1);
		String expr = EquationDiff.removeQuote(racineOp.getText());
		
		switch(racineDerivees.getType()) {
		case EquaDiffParser.DER2 :			
			yDDot[0] = ExpressionTree.parse(expr).eval(this.variables, this.fm); 
			this.variables.setValue(EquationDiff.constructNomDer(racineDerivees), yDDot[0]);			
			break;
		case EquaDiffParser.DER1 :
			yDot[0] = ExpressionTree.parse(expr).eval(this.variables, this.fm);
			this.variables.setValue(EquationDiff.constructNomDer(racineDerivees), yDot[0]);
			break;
		case EquaDiffParser.ID :
			y[0] = ExpressionTree.parse(expr).eval(this.variables, this.fm);
			this.variables.setValue(EquationDiff.constructNomDer(racineDerivees), y[0]);
			break;
		default :
			break;				
		}
	}
}
