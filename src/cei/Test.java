package cei;
import java.io.IOException;

import org.antlr.runtime.RecognitionException;

public class Test {

	public static void main(String[] args) throws IOException, RecognitionException {
		testGenerique();
	}
	
	public static void testGenerique() throws IOException, RecognitionException {
		// String filePath = "test/penduleode.txt";
		// String filePath = "test/amortissement.txt";
		String filePath = "test/balleode.txt";
		double t0 = 0.0;
		double tFinal = 20.0;
		double h = 0.01;
		boolean eventOnIncreasing = false;
		BlocCT bloc = new BlocCT(t0, tFinal, h, filePath, eventOnIncreasing);
		bloc.start();		
	}
}
