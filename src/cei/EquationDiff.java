package cei;
import java.util.*;
import org.antlr.runtime.tree.Tree;
import com.graphbuilder.math.*;

public abstract class EquationDiff {
	protected Tree arbreEq;
	
	protected VarMap variables;
	protected FuncMap fm;
	protected HashMap<String, Expression> tabConstantes;
	protected HashMap<String, Expression> tabCondIni;
	protected ArrayList<String> vectEtat;
	protected double[] vect;	
		
	public EquationDiff(Tree arbre) {
		this.arbreEq = arbre;
		// Case sensitive variable map
		this.variables = new VarMap(false);
		// Function map
		this.fm = new FuncMap();
		this.fm.loadDefaultFunctions();
		
		this.tabCondIni = new HashMap<String, Expression>();
		this.tabConstantes = new HashMap<String, Expression>();
		this.vectEtat = new ArrayList<String>();
		
		this.parcoursConstantes();
		this.parcoursCondIni();
		this.parcoursVectEtat();
	}
	
	public double[] getVect() {
		return this.vect;
	}
	
	public Tree getTree() {
		return this.arbreEq;
	}
	
	public VarMap getVarMap() {
		return this.variables;
	}
	
	public FuncMap getFuncMap() {
		return this.fm;
	}
	
	public double getCste(String nomConstante) {
		return this.tabConstantes.get(nomConstante).eval(this.variables, this.fm);
	}
	
	public double getCondIni(String nomVariable) {
		return this.tabCondIni.get(nomVariable).eval(this.variables, this.fm);
	}
	
	public boolean isCste(String nomConstante) {
		return this.tabConstantes.containsKey(nomConstante);
	}
	
	public boolean isCondIni(String nomVariable) {
		return this.tabCondIni.containsKey(nomVariable);
	}
			
	public boolean isEtat(String nomEtat) {
		return this.vectEtat.contains(nomEtat);
	}
	
	public ArrayList<String> getVectEtat() {
		return this.vectEtat;
	}
	
	private void parcoursVectEtat() {
		Tree racineVectEtat = this.arbreEq.getChild(IndiceBloc.INDICEVECTETAT.ordinal());
		for (int i = 0; i < racineVectEtat.getChildCount(); i++) {
			String variable = racineVectEtat.getChild(i).getText();
			this.vectEtat.add(variable);
		}		
	}
	
	private void parcoursConstantes() {
		Tree racineConstantes = this.arbreEq.getChild(IndiceBloc.INDICECSTES.ordinal());
		for (int i = 0; i < racineConstantes.getChildCount(); i++) {
			Tree racineAffect = racineConstantes.getChild(i);
			String nomVariable = racineAffect.getChild(0).getText();
			String expr = EquationDiff.removeQuote(racineAffect.getChild(1).getText());
			Expression e = ExpressionTree.parse(expr);
			this.variables.setValue(nomVariable, e.eval(this.variables, this.fm));
			this.tabConstantes.put(nomVariable, e);
		}
	}
	
	protected static String removeQuote(String s) {
		return s.replaceAll("\"", "");
	}
	
	protected static String constructNomDer(Tree t) {
		if (t.getChildCount() != 0)
			return t.getText() + "::" + t.getChild(0).getText() + "::";
		else
			return t.getText();
	}
	
	protected static String constructNomDer(String n, int ordre) {
		if (ordre == 0)
			return n;
		else 
			return "der" + ordre + "::" + n + "::";
	}
	
	private void parcoursCondIni() {
		Tree racineCondIni = this.arbreEq.getChild(IndiceBloc.INDICECONDINI.ordinal());
		int nombreEnfants = racineCondIni.getChildCount();
		this.vect = new double[nombreEnfants];
		for (int i = 0; i < nombreEnfants; i++) {
			Tree racineAffect = racineCondIni.getChild(i);
			Tree leftMember = racineAffect.getChild(0);
			String nomVariable;
			if (leftMember.getType() == EquaDiffParser.ID) {
				nomVariable = racineAffect.getChild(0).getText();				
			}
			else {
				nomVariable = EquationDiff.constructNomDer(leftMember);
			}
			String expr = EquationDiff.removeQuote(racineAffect.getChild(1).getText());
			Expression e = ExpressionTree.parse(expr);
			this.variables.setValue(nomVariable, e.eval(this.variables, this.fm));
			this.tabCondIni.put(nomVariable, e);
			this.vect[i] = e.eval(this.variables, this.fm);
		}
	}	
}
