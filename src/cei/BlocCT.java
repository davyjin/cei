package cei;
import java.io.IOException;

import org.antlr.runtime.RecognitionException;

/**
 * Classe de test, DEPRECATED
 * @author jin
 *
 */
public class BlocCT extends Thread {
	private Equation eq;
	
	public BlocCT(double t0, double tFinal, double h, String filePath, boolean eventOnIncreasing) 
			throws IOException, RecognitionException {
		this.eq = new Equation(filePath, t0, tFinal, h, eventOnIncreasing);
	}
	
	public void run() {
		this.eq.integrate();
		while(!this.eq.isOver()) {
			try {
				this.update();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void update() throws InterruptedException {
		this.eq.getStepHandler().stepStart();
		this.eq.getStepHandler().stepEnd();		
	}
}
